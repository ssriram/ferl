
% freeway
vuct = [0.55 1.47 3.26 5.09 6.22];
muct0 = [2.65 5.65 8.36 10.93 12.33];
muct = [3.07 5.99 10.05 13.38 13.87];

evuct = [0.03 0.06 0.06 0.07 0.04];
emuct0 = [0.07 0.06 0.06 0.04 0.03];
emuct = [0.07 0.08 0.10 0.06 0.04];

% end of freeway

% invaders
vuct = [18.34 21.82 26.32 27.77 28.57];
muct0 = [14.23 20.24 25.1 27.17 28.92];
muct = [15.93 22.18 26.4 28.82 29.53];

evuct = [0.29 0.21 0.17 0.14 0.11];
emuct0 = [0.32 0.3 0.16 0.13 0.1];
emuct = [0.34 0.27 0.18 0.10 0.09];

% end of invaders

%seaquest
vuct = [1.07 1.47 1.74 1.85 1.95];
muct0 = [1.28 2.19 3.48 4.3 4.99];
muct = [1.38 2.45 4.67 6.2 6.72];

evuct = [0.02 0.03 0.04 0.04 0.04];
emuct0 = [0.04 0.04 0.03 0.03 0.03];
emuct = [0.03 0.04 0.05 0.04 0.04];

% end of seaquest

nrollouts = [20 40 80 160 320];
grouped = [vuct(1) muct0(1) muct(1);vuct(2) muct0(2) muct(2);vuct(3) muct0(3) muct(3);
vuct(4) muct0(4) muct(4);vuct(5) muct0(5) muct(5)];

egrouped = [evuct(1) emuct0(1) emuct(1);evuct(2) emuct0(2) emuct(2);evuct(3) emuct0(3) emuct(3);
evuct(4) emuct0(4) emuct(4);evuct(5) emuct0(5) emuct(5)];

h = bar(grouped,'group')
set(h,'BarWidth',1);    % The bars will now touch each other
set(gca,'fontsize',40);
lh = legend('UCT','mNN-UCT(0)','mNN-UCT');
xlabel('Number of Rollouts');
ylabel('Mean Score');
set(gca,'XtickL',nrollouts);

hold on

numgroups = size(grouped,1);
numbars = size(grouped,2);
groupwidth = 1;
for i = 1:numbars
      % Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
      x = get(get(h(i),'children'),'xdata');
      barCenters = mean(unique(x,'rows'))
      errorbar(barCenters, grouped(:,i), egrouped(:,i), 'k', 'linestyle', 'none');
end
xlim([0 6]);
hold on
plot(xlim,[16 16]);
