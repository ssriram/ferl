\documentclass[letterpaper]{article}
\usepackage{aaai}
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{verbatim}
\setlength{\pdfpagewidth}{8.5in}
\setlength{\pdfpageheight}{11in}
\pdfinfo{
/Title (Insert Your Title Here)
/Author (Put All Your Authors Here, Separated by Commas)}
\setcounter{secnumdepth}{2}

% User defined
\renewcommand\thesubfigure{(\alph{subfigure})}
\captionsetup[subfigure]{labelformat=simple}
\newcommand{\norm}[1]{\| #1 \|}
\newcommand{\argmin}{\operatornamewithlimits{argmin}}
\newcommand{\BigO}[1]{\ensuremath{\operatorname{O}\bigl(#1\bigr)}}
\newtheorem{lem}{Lemma}
\DeclareMathOperator*{\argmax}{arg\,max}

\begin{document}
%The file aaai.sty is the style file for AAAI Press 
%proceedings, working notes, and technical reports.

\title{Improving exploration in UCT using local manifolds}
\author{Anonymous
}
\maketitle

\begin{abstract}
Monte-Carlo planning has been proven successful in many sequential decision-making settings, but it suffers from poor exploration when the rewards are sparse. In this paper, we improve exploration in UCT by generalizing across similar states using a given distance metric. When the state space does not have a natural distance metric, we show how we can learn a local manifold from the transition graph of states in the near future. On domains inspired by video games, empirical evidence shows that our algorithm is more sample efficient than UCT, particularly when rewards are sparse.
\end{abstract}

\section{Introduction}
\label{sec:intro}
A useful approach to sequential decision problems involving large state spaces, is to search in the action space, by building a search tree using samples from a model \cite{browne2012survey}. One such recently developed algorithm is UCT \cite{kocsis2006bandit}. The popularity of these planning algorithms is due to the fact that the complexity of the action selection step is independent of the size of the state space.

However, UCT performs poorly when the rewards are sparse. Consider the grid world domain shown in Figure \ref{fig:grid}. The agent starts at the solid square in blue. The goal state is marked by a red X and the agent receives a reward of $+1$ on reaching the goal state, leading to episode termination. The agent has 4 actions available - move UP, DOWN, LEFT and RIGHT. In the absence of reward from rollouts, UCT essentially follows random behavior. For a reasonably distant goal state, a uniform random policy is unlikely to observe any reward. Due to its random rollout policy, UCT keeps visiting states similar to those it has already seen before. As a result, UCT needs a large number of rollouts to see reward.
\begin{figure*}
	\centering
	\begin{subfigure}{0.28\hsize}
  		\centering
  		\includegraphics[width=0.6\hsize]{Grid.eps}
  		\caption{Grid World}
  		\label{fig:grid}
	\end{subfigure}%
	\begin{subfigure}{0.28\hsize}
  		\centering
  		\includegraphics[width=0.6\hsize]{Grid_wall.eps}
  		\caption{Grid World with Wall}
  		\label{fig:grid_wall}
	\end{subfigure}
	\begin{subfigure}{0.4\hsize}
		\centering
		\includegraphics[width=0.85\hsize]{manifold.eps}
		\caption{Manifold Embedding of 20 $\times$ 20 grid with wall}
		\label{fig:mani}
	\end{subfigure}
\caption{Grid World Domains}
\label{fig:grid_world}
\end{figure*}

An alternative approach would be to systematically explore the state space, collecting returns from states that are different from the ones already visited. However, performing an exhaustive search of the state space would be impractical. We want to retain the strengths of Monte-Carlo planning in using samples, while still achieving efficient exploration. In this paper we aim to exploit similarity between states, and augment UCT with this information, to achieve efficient exploration. If the state representation does not provide a similarity metric, we learn a manifold of the state space, thus embedding states in Euclidean space where we can compute similarity using a distance metric. We first review some background material in the next section, before describing our algorithm in section \ref{sec:nnUCT}.

\section{MDPs and UCT}
\label{sec:prelim}
We consider sequential decision problems formulated as Markov Decision Processes (MDPs). In this work, we focus on deterministic MDPs. A deterministic MDP can be described as a tuple $\langle S,A,T,R,\gamma \rangle$, where $S$ is the state space, A is the action space. $T : S \times A \mapsto S$, is the transition function, $\rho : S \times A \mapsto \mathbb{R}$ is the reward function, where $\rho(s,a)$ is the immediate reward obtained by taking action $a$ from state $s$, and $\gamma$ is the discount factor that acts as a tradeoff between short-term and long-term rewards. We aim to learn a policy $\pi : A \times S \mapsto [0,1]$, using a state-action value function defined as $Q^{\pi}(s,a) = \mathbb{E}_{\pi}[\sum_{t=1}^{\infty} \gamma^{t-1} \rho(s_{t-1},a_{t-1}) | s_0 = s, a_0 = a]$. The optimal policy, $\pi^*$, has action-value function $Q^* = \max_{\pi} Q^{\pi}$.

UCT \cite{kocsis2006bandit} is a Monte-Carlo Tree Search (MCTS) algorithm, that uses the UCB1 algorithm \cite{auer2002finite} for its \emph{tree policy}. The tree policy in an MCTS algorithm dictates action selection at each level of the tree, leading to the creation of a leaf node. Once created, the \emph{default policy} (usually random actions) is followed from the leaf state up to the end of planning horizon or episode termination. Let $D$ denote the set of nodes in the UCT tree. Note that two or more nodes can share the same state $s \in S$. Let $S(d)$ denote the state corresponding to node $d \in D$. Each node maintains the returns obtained by rollouts starting from that node, $R(d)$, as well as the visit count, $n(d)$. The value of the node is estimated as, $V(d) = \frac{R(d)}{n(d)}$. The action value estimate is $Q(d,a) = \rho(S(d),a) + \gamma * V(d')$, where $d'$ is the child node reached by taking action $a$. At each step in the tree policy, UCT treats the action selection step, as a multi-armed bandit problem. It computes a UCB score and picks the action corresponding to the highest score. The score for node $d$ and action $a$ is given by :
\begin{align*}
Q_{ucb}(d,a) = Q(d,a) + C * \sqrt{\frac{2 \ln{\sum_{a' \in A} n(d,a')}}{n(d,a)}}
\end{align*}
The second term is an exploratory bonus, which encourages actions taken less frequently from a node, where $C$ is a positive parameter, $n(d,a)$ is the number of times, action $a$ was taken from node $d$ and hence $n(d,a) = n(d')$, where $d'$ is the child node reached by taking action $a$. We also have, $\sum_{a \in A} n(d,a) = n(d)$. After the computational budget is exhausted in building the tree, the greedy action at the root, $\argmax_{a \in A} Q(d_0,a)$ is picked. It is also common to pick the most frequent action at the root, $\argmax_{a \in A} n(d_0,a)$.

\begin{figure*}
\centering
\begin{subfigure}{.49\hsize}
  \centering
  \includegraphics[width=0.9\hsize]{grid_bar.eps}
  \caption{Grid}
  \label{fig:grid_bar}
\end{subfigure}%
\begin{subfigure}{.49\hsize}
  \centering
  \includegraphics[width=0.9\hsize]{grid_wall_bar.eps}
  \caption{Grid with Wall}
  \label{fig:grid_wall_bar}
\end{subfigure}
\caption{Performance on Grid World Domains}
\label{fig:grid_bar_charts}
\end{figure*}

\section{NN-UCT}
\label{sec:nnUCT}
By adding generalization in the UCB score, a good estimate of the node's statistics can be obtained from the statistics of other nodes in the tree, thus making better use of samples. A natural way to incorporate generalization, is to combine the statistics of \textit{similar} nodes.  One way to achieve this is to compute a nearest neighbor estimate of the return and visit counts for each node $d$, as given by :
\begin{align*}
R_{nn}(d) = \sum_{d' \in D} K(S(d),S(d')) *  R(d')\ \\
n_{nn}(d) = \sum_{d' \in D} K(S(d),S(d')) * n(d') \\
V_{nn}(d) = \frac{R_nn(d)}{n_{nn}(d)}
\end{align*}
where $K(s,s')$ is a kernel function that measures similarity between states $s$ and $s'$. A popular choice for the kernel function is the Gaussian kernel function, $K(s,s') = \exp\left(\frac{\norm{s - s'}_2}{\sigma}\right)$, where $\sigma$ is the Gaussian width parameter which controls the degree of generalization. As $\sigma \to 0$, the nearest neighbor estimate approaches the MC estimate of all the transpositions \cite{childs2008transpositions}. Note that the nearest neighbor estimate adds bias, which could prevent the guarantee of continual exploration of all actions. Hence, asymptotically, we want the returns and visit count estimates to be closer to the unbiased MC estimates. One resolution is to employ a decay scheme, that shrinks the Gaussian widths of the nodes as planning continues. In section \ref{sec:decay}, we discuss one such scheme for reducing $\sigma$ smoothly over time. The new UCB score is computed as :
\begin{align*}
Q_{ucb}(d,a) = Q_{nn}(d,a) + C * \sqrt{\frac{2 \ln{\sum_{a' \in A} n_{nn}(d,a')}}{n_{nn}(d,a)}}
\end{align*}
where $Q_{nn}(d,a) = \rho(S(d),a) + \gamma * V_{nn}(d')$, where $d'$ is the child node of node $d$ when action $a$ is taken from state $S(d)$. The quantity, $n_{nn}(d,a) \equiv n_{nn}(d')$, is taken from the child node $d'$. Thus, the nearest neighbor estimate of the number of times action $a$ was taken from state $S(d)$ is obtained from the number of state visits to the after-state $S(d')$. In our algorithm, unlike UCT, $\sum_{a \in A} n_{nn}(d,a) \neq n_{nn}(d)$. Thus, neither the actual visit count ($n(d)$), nor the nearest neighbor estimate ($n_{nn}(d)$) of the parent node is used in the exploration term. Hence, only the estimates of the child nodes (after-state) are used for action selection and actions leading to after-states similar to the ones already explored are not preferred. Intuitively, the tree policy now directs the tree to be grown in areas whose states have not been encountered already. The idea of using after-state counts in the exploration bonus was also explored previously \cite{mehat2010combining}, but they did not incorporate generalization.

\subsection{Decay Scheme}
\label{sec:decay}
Given the initial Gaussian kernel width $\sigma$, we want to develop a scheme which shrinks the Gaussian widths of nodes such that the value estimates and visit counts are closer to the MC estimate, asymptotically. We use a simple decay rate, $\beta$, where $0 < \beta < 1$. In this scheme, we shrink the Gaussian width of the child nodes whenever the tree policy is executed at the parent node. Thus, the Gaussian width of a node $d'$ is given by $\sigma(d') = \sigma * \beta^{n(d)}$, where $n(d)$ is the count of the visits to the parent node $d$. The Gaussian width of the root node is shrunk after each rollout. This scheme ensures that if erroneous generalization has starved visits to $d'$, it will eventually be explored once its parent has been visited sufficiently often.

\subsection{Generalization in Complex Domains}
In the grid world example shown in Figure \ref{fig:grid}, the Euclidean distance between two points on the grid was a good choice for a distance metric. However, in general, the grid coordinates may not be the best representation. For example, consider a grid world with a wall as shown in Figure \ref{fig:grid_wall}. In this case, distance between grid coordinates is not a good distance metric, as states on either side of the wall, are farther apart than those on the same side. Furthermore, not all state representations have a natural metric space for defining similarity. High dimensional state spaces pose an additional problem of tractably computing a distance metric. Hence, we need a state representation that captures the underlying topology of the environment, that is low dimensional, allowing fast computation of distances between states. Low dimensional manifolds are a natural choice that satisfy all the requirements.


\begin{figure*}
        \centering
        \begin{subfigure}{0.33\hsize}
        		   \centering
                \includegraphics[scale=0.25]{freeway_game.eps}
                \caption{Freeway}
        \end{subfigure}
        \begin{subfigure}{0.33\hsize}
        		   \centering
                \includegraphics[scale=0.25]{invaders_game.eps}
                \caption{Space Invaders}
        \end{subfigure}
        \begin{subfigure}{0.33\hsize}
        		   \centering
                \includegraphics[scale=0.25]{seaquest_game.eps}
                \caption{Seaquest}
        \end{subfigure}
        \caption{Games Domains}\label{fig:games}
\end{figure*}

\section{mNN-UCT}
A manifold $\mathcal{M} \subset \mathbb{R}^m$ is a representation of a set of points such that the Euclidean distance between neighboring points on the manifold reflects the true distances between those points. Figure \ref{fig:mani} shows an example of a 2D manifold of a $20 \times 20$ grid, with a wall as shown. Manifold learning \cite{ma2012manifold} typically involves learning a low-dimensional representation, subject to distance preserving constraints. Manifold learning algorithms rely on the geodesic distances between neighboring states, while approximating other distances. Euclidean distance between points on the manifold can then serve as a distance metric for measuring similarity of states. Thus, the similarity measure needed in our NN-UCT algorithm, can be obtained by embedding the states on a manifold.

The computational complexity of most manifold learning algorithms is
$\BigO{m^3}$, where $m$ is the number of states needed to be
embedded. This cost is due to the distance matrix construction and
subsequent eigen-decomposition. Hence, it is computationally
infeasible to embed all of the possible states in large problems. One
way to deal with the high computational cost of embedding states on a
manifold, is to embed only those states that may be encountered in the
near future. As we are operating in the planning setting, with access
to a model, we can sample the states reachable in the near future and
construct a manifold that is local to the current state. We next
describe an algorithm to construct such a manifold.

\subsection{Local Manifolds}
For computational feasibility, we learn a local manifold over the state space. Local manifolds provide a topology of the state space, in the local neighborhood of the current state. In our work, at every decision step, we learn a local manifold of the deterministic MDP from the sample transitions encountered during a short breadth-first walk of the state space, starting from the current state. We construct the distance matrix of the all-pairs shortest path distances between states in teh BFS tree. We apply the Isomap algorithm \cite{tenenbaum2000global} on the distance matrix to generate a Euclidean embedding of the states. This ensures that states that are nearby in time are nearby in space as well.

During UCT planning, we may encounter states that lie outside of the local manifold. This situation is referred to as the out-of-sample problem and is more likely to occur as the UCT tree grows larger. If we were to embed these states on a manifold, the BFS walk needed from the start state would get deeper, resulting in a possibly exponential increase in the number of states to be embedded.

We address the out-of-sample problem in the following way. We generate the approximate embeddings of these states, by applying an operator learnt for each action in the action set. We learn this operator from the manifold embeddings of the states encountered during the BFS walk for the local manifold. 
Let $T$ denote the set of tuples $\langle x, k, y \rangle$, such that $y$ is the manifold embedding of the state obtained by taking action $k \in A$ from the state with manifold embedding $x$. We learn a simple translation operator ($b_k$) for this action by :
\begin{align*}
b_k = \frac{1}{|T|} \sum_{t \in T, t = \langle x, k, y \rangle} \norm{y - x}_2.
\end{align*}
We found the simple translation operator to be useful in all the domains tested here. Applying more sophisticated solutions to solve the out-of-sample problem is a direction for future work.

NN-UCT with manifolds, referred to as mNN-UCT, uses the manifold embedding as the state representation to compute the kernel function. If a state outside of the BFS walk is encountered during planning, its manifold embedding is computed by applying the translation operator to the state from which the current action was taken.

\section{Experiments}
\label{sec:expt}
\subsection{Experiment Setup}
In our UCT implementation, whenever a new node is created, the default policy (random actions) was run for a constant length of length 50, rather than out to a fixed horizon. This helped prevent finite horizon effects from affecting generalization. The optimal branch of the UCT tree and the BFS tree was retained across planning steps. In all of our experiments, a wide range $\{10^{-5},10^{-4},..,10^3,10^4\}$ was tested for the UCB parameter with 500 trials performed for each parameter setting.
\subsection{Grid World Domain}

We first validate our intuitions about the impact of state generalization in UCT by evaluating the algorithms on the two grid world domains shown in Figure \ref{fig:grid_world} and described in \ref{sec:intro}. An episode terminates, when the agent reaches the goal state, or 1000 time-steps have elapsed, whichever is shorter. We report the number of steps taken to reach the goal state, given a fixed number of samples for planning. For the UCT algorithms with generalization, the Gaussian kernel function was used. The initial Gaussian width $\sigma$, was chosen amongst $\{10,100,1000,10000\}$ and the decay rate $\beta$ from $\{0.1,0.5,0.9,0.99\}$, and the best result is reported. The Euclidean distance was used for the distance metric in the kernel function. For the mNN-UCT algorithm, the BFS walk included at most 400 states for each planning step. The mNN-UCT algorithm with $\sigma = 10^{-6}$, referred to as mNN-UCT(0) shares statistics between nodes with the same state. This tests whether generalizing from similar states is helpful. mNN-UCT-g is the mNN-UCT algorithm using a global instead of a local manifold, where a full BFS walk is used to construct the manifold. This allows us to evaluate the effectiveness of local manifolds.

Figure \ref{fig:grid_bar_charts} summarizes the results of our grid world experiments. The mean number of steps (fewer steps being better), with standard error bars is shown. Sharing statistics between nodes with exactly the same states (mNN-UCT(0)) improves UCT slightly. Adding more widespread generalization to UCT improves the performance dramatically in both the domains, with the NN-UCT algorithm reaching the goal state in less than half the number of steps taken by UCT in the first domain.  The best performing $\sigma$ for the mNN-UCT and mNN-UCT-g algorithms was substantially greater than 0, suggesting that generalizing over nearby states in the neighborhood of the manifold is useful. All the algorithms, as expected, take more steps to reach the goal when the wall is added. The NN-UCT algorithm, which uses the Euclidean distance metric between grid coordinates, does not perform as well in the second domain. The distance metric is not as useful since states on either side of the wall are farther apart than the Euclidean distance indicates. mNN-UCT does not suffer from this problem, as it better captures the topology of the state space. In both the domains, using local manifolds rather than global manifolds does not substantially affect planning performance.

\subsection{Game Domains}
We also test our algorithm on 3 domains. These domains are single player games played on a $16 \times 16$ board, inspired by video games. The initial configuration of the games is shown in Figure \ref{fig:games}. All the games last for 256 time-steps or until the agent loses all of its lives, whichever is shorter. The agent is depicted by the blue square.
\begin{enumerate}
\item{Freeway : Cars are depicted by red chevrons. Their positions on each row and direction of movement is set arbitrarily at the start. On reaching the end of the board, they reappear back on the opposite side. The agent has 3 lives. On collision, the agent loses a life and its position is reset to its start position. The agent has 3 actions - NO-OP (do nothing), UP and DOWN. The agent receives a reward of $+1$ on reaching the topmost row after which the agent's position is reset to start state. The maximum possible score is 16 in the absence of cars, although with cars, the optimal score is likely lower.}
\item{Space Invaders : The aliens are depicted by red triangles. These aliens fire bullets vertically downward, at fixed intervals of 4 time-steps. The aliens move from left to right. They change directions if the rightmost (or leftmost) alien reaches the end of the screen. The agent loses its only life if it gets hit by a bullet from the alien. The following actions are available to the agent : NO-OP, FIRE, LEFT, RIGHT, LEFT-FIRE (fire bullet and move left) and similarly RIGHT-FIRE (fire bullet and move right). The maximum possible score is 32 in the absence of the alien's bullets, with a realizable score likely lower.}
\item{Seaquest : Fish depicted by red chevrons, move from left to right and cycle back on reaching the end of the screen. The diver, indicated by a black triangle, appears at either end, alternately. The diver appears when the current diver is collected by the agent or if the current diver reaches end of the screen. The agent receives a reward of +1 if it collects the diver and drops it at the drop point (top row, indicated in green). The agent loses its only life on collision with a fish, or on reaching the drop point without the diver. The actions available to the agent are NO-OP, UP, DOWN, LEFT and RIGHT. The maximum possible score is 8, as it is impossible to capture any 2 successive divers and drop them at the drop point.}
\end{enumerate}

\begin{figure}[t]
        \centering
        \begin{subfigure}{0.9\hsize}
        		\centering
             \includegraphics[width=\hsize]{freeway.eps}
             \caption{Freeway}
        \end{subfigure}\\
        \begin{subfigure}{0.9\hsize}
        		\centering
             \includegraphics[width=\hsize]{invaders.eps}
             \caption{Space Invaders}
        \end{subfigure}\\
        \begin{subfigure}{0.9\hsize}
        		\centering
             \includegraphics[width=\hsize]{seaquest.eps}
             \caption{Seaquest}
        \end{subfigure}
        \caption{Performance comparison}\label{fig:perfcomp}
\end{figure}

\subsubsection{Performance Comparison.}
We compare mNN-UCT with UCT on the domains described above. We again evaluate the effect of generalization by running our algorithm with $\sigma = 10^{-6}$ referred to mNN-UCT(0). We ensure that all of the algorithms are given the same number of samples per planning step. We vary the number of rollouts from $\{20,40,80,160,320\}$. The discount factor was set at 0.99. We fix the other parameters for the mNN-UCT algorithm with $\sigma = 100$, and $\beta = 0.9$. We learn a local manifold for each planning step from a BFS tree comprising of at most 400 states. For each of the domains, we measure the mean score obtained per episode. The standard errors are indicated as error bars. The optimistic maximum score possible is indicated by a blue line. Note that it may not be possible to achieve this score in all domains.

Figure \ref{fig:perfcomp} shows the performance of the 3 algorithms against the number of rollouts. The results are reported for the parameters which produced the best mean score obtained by performing 500 trials. mNN-UCT outperforms UCT, especially in Freeway and Seaquest. Both these games require the agent to perform a specific sequence of actions in order to obtain reward. UCT's rollout policy is sufficient to achieve a high score in Space Invaders as the game does not have sparse rewards. Generalization using manifolds provides only a marginal improvement in this game.

\subsubsection{Parameter Sensitivity.}
\begin{figure}[t]
        \centering
        \begin{subfigure}{0.9\hsize}
        			\centering
                \includegraphics[width=\hsize]{freeway_scatter.eps}
                \caption{Freeway}
        \end{subfigure}\\
        \begin{subfigure}{0.9\hsize}
        			\centering
                \includegraphics[width=\hsize]{invaders_scatter.eps}
                \caption{Space Invaders}
        \end{subfigure}\\
        \begin{subfigure}{0.9\hsize}
        			\centering
                \includegraphics[width=\hsize]{seaquest_scatter.eps}
                \caption{Seaquest}
        \end{subfigure}
        \caption{Parameter sensitivity}\label{fig:paramsens}
\end{figure}

Compared to UCT, the mNN-UCT algorithm has the additional parameters $\sigma$ and $\beta$ which together control the degree of generalization in the UCT algorithm. We evaluate the sensitivity of the algorithm with respect to these parameters. In this experiment, we fix the number of rollouts at 100. We vary $\sigma$ from $\{10,100,1000,10000\}$ and $\beta$ in $\{0.1,0.5.0.9,0.99\}$. For each of the parameter settings, we measure the performance of the mNN-UCT algorithm and compare it to UCT using various settings of the UCB parameter. 

Scatter plots are shown in Figure \ref{fig:paramsens} where each blue x and red square represents the mean score per episode of the mNN-UCT and UCT algorithms respectively. We see that the mNN-UCT algorithm performs better than UCT for a wide range of parameter settings in all the domains. Using widespread generalization achieves significantly higher score on sparse reward games such as Freeway and Seaquest, while the improvement is modest when rewards are easy to obtain in the game such as Space Invaders. 

\section{Discussion}
We observed that adding generalization improves UCT significantly in sparse reward conditions. By using the nearest neighbor heuristic, we are making the assumption that states located nearby in space also have similar values. Such an assumption may not hold in all domains, although in our experiments it proved helpful. Though the decay scheme to shrink the Gaussian kernel widths of nodes in the UCT tree should help overcome any adverse effects of generalization, we do not have consistency guarantees on the asymptotic performance. It would be interesting to explore whether these benefits can be obtained while retaining asymptotic guarantees.

While we have shown empirically that state generalization helps UCT, the improvement comes at an additional cost. In our algorithm, the cost is due to manifold learning and computing the nearest neighbor estimates. While the former is controlled by the size of the BFS walk, the latter is controlled by the number of rollouts. Our choice of using local manifolds constructed from a small BFS tree, along with operators does not limit UCT from growing deeper. Experimental results have shown that fewer rollouts may be needed to achieve good performance and hence the cost incurred in computing the nearest neighbor estimate would be less expensive. For large UCT trees, we can be more efficient by having a distance threshold, beyond which neighbor estimates are ignored by using K-d trees \cite{bentley1975multidimensional}.

\subsection{Related Work}
A useful enhancement to UCT is the use of transposition tables \cite{childs2008transpositions}. Transposition tables offer a basic form of generalization, by sharing statistics of nodes in the UCT tree having the same state. Our algorithm with $\sigma \approx 0$ closely resembles UCT with transposition tables. Although transposition tables help speed up search, they provide limited exploration benefit as they can only recognize state equality. In large state spaces, the likelihood of encountering transpositions is low, and hence transposition tables may be less useful. This was noticed even in our relatively small experimental domains, with more aggressive generalization improving on basic transposition tables.

State abstraction using homomorphisms in UCT adds another layer of generalization, by grouping states with the same transition and reward distributions. The idea of using local homomorphisms \cite{jiang2014improving} as an approximate state abstraction was an inspiration for the development of using local manifolds. The homomorphism groups states together but like transposition tables, they do not have a degree of similarity, only a hard notion of equivalence. In the case of the grid world domain in Figure \ref{fig:grid}, the only homomorphism is the grid itself. Thus, homomorphisms are no better than transposition tables in this case.

Another approach to generalization in UCT is Rapid Action Value Estimation (RAVE) \cite{gelly2011monte}. RAVE uses the \textit{All-Moves-As-First} (AMAF) heuristic which states that the value of an action is independent of when it is taken, i.e. if an action is profitable at a subtree $\tau(s)$, it is profitable immediately at root $s$ as well. RAVE combines the Monte-Carlo estimate with the AMAF estimate, $Q_{rave}(s,a) = (1-\beta) * Q(s,a) + \beta * Q_{AMAF}(s,a)$, where $0 < \beta < 1$ is a weight parameter that decreases with increasing number of rollouts. The RAVE estimate is used in place of $Q(s,a)$ for the tree policy. RAVE has been shown to be successful in Computer Go and has also been applied in continuous action spaces \cite{couetoux2011continuous}. In that case, the RAVE estimate for action $a$ is obtained by computing a nearest-neighbors estimate of the returns in the subtree where action $a$ was taken. However, the AMAF heuristic does not modify the exploration bonus. Thus when no rewards have been encountered, its exploration strategy suffers from the same problems as UCT.

Previous work on learning manifolds in MDPs include proto-value functions (PVFs) \cite{mahadevan2007proto}. Proto-value functions are used to learn a low dimensional representation from a transition graph generated by following a fixed policy which is subsequently improved using policy iteration. PVFs assume that a random walk of the state space is sufficient to construct basis functions that reflect the underlying topology. However, in many domains with large state space, such as video games, a policy consisting of random actions does not provide a sufficient view of the state space. Additionally, it may be computationally infeasible to embed all of the states encountered by following the policy. Hence our use of local manifolds which, in our experiments, seem to offer similar benefits to using global manifolds.

\section{Conclusion}
In this paper, we presented a method of incorporating state generalization into UCT by using nearest neighbor estimates of the action-value and the visit counts in the tree policy. We also showed that in domains whose states do not have a natural distance metric we can learn a local manifold of the state space that gives us a Euclidean representation of the states in the near future. We demonstrated the benefit of learning such local manifolds on video game domains. We found that our algorithm substantially outperformed UCT with no generalization, especially in environments with sparse rewards.

\bibliographystyle{aaai}
\bibliography{references}

\end{document}

