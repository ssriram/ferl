
grid = {
'UCT' 419
'NN-UCT' 108
'mNN-UCT(0)' 291
'mNN-UCT' 47
'mNN-UCT-g' 59
};
e = [13 10 10 1 1];

grid = {
'UCT' 859 
'NN-UCT' 605
'mNN-UCT(0)' 766
'mNN-UCT' 357
'mNN-UCT-g' 192
};
e = [10 13 14 12 8];

h = bar([grid{:,2}],'c');
set(gca,'XtickL',[grid(:,1)]);
ylabel('Steps to Goal');

hold on
x = get(get(h(1),'children'),'xdata')
barCenters = mean(unique(x,'rows'))
errorbar(barCenters,[grid{:,2}],e,'k','linestyle', 'none');
