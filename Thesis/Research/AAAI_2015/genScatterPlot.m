
vUCTFile = 'vUCT_freeway_scatter.dat';
mUCTFile = 'mUCT_freeway_scatter.dat';

vUCT = importdata(vUCTFile);
mUCT = importdata(mUCTFile);

sh = scatter(log10(vUCT(:,1)),vUCT(:,2),540,'r','s','MarkerFaceColor','r');
hold on
sh = scatter(log10(mUCT(:,1)),mUCT(:,2),540,'b','x');

set(gca,'fontsize',40);
lh = legend('UCT','mNN-UCT');
xlabel('Log10 UCB Constant');
ylabel('Mean Score');
