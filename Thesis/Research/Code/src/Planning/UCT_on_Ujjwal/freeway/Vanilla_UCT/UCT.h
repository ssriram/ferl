#ifndef __UCT__
#define __UCT__

#include <list>
#include <stack>
#include <algorithm>
#include <vector>
#include <utility>
#include <cassert>

#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define all(c) c.begin(),c.end()
#define INF (int) 1e9
#define EPS 1e-3
#define VI vector<int>
#ifndef PII
    #define PII pair<int,int>
#endif

#include <Freeway.h>

using namespace std;

class Node;

typedef map<int,pair<Node *,float> > Nodes;

class Node {
    private:
    Node *parent;
    Nodes children;
    float uct_count;
    float uct_return;
    vector<int> exploredActions;

    public:

    Node(float uct_return, float uct_count, 
         Node *parent) {

        this->uct_return = uct_return;
        this->uct_count = uct_count;
        this->parent = parent;
    }

    void addChild(Node *child, int action, float reward) {
        this->children[action] = make_pair(child,reward);
        this->exploredActions.pb(action);
    }
    
    void update(float totalReward, const float gamma, stack<float>& rewards) {
        assert(sz(rewards) > 0);
        float reward = rewards.top() + gamma * totalReward;
        totalReward = reward;
        rewards.pop();
        this->uct_count += 1.0;
        this->uct_return += reward;
        if(this->parent) (this->parent)->update(totalReward,gamma,rewards);
    }

    float getUCTQ(float reward,float parent_count,const float uct_constant,
          const float gamma) {
        float count = this->uct_count;
        float value = reward + gamma * (this->uct_return / count);
        return value + uct_constant * sqrt(2*logf(parent_count)/ count);
    }

    pair<Node *, int> getBestAction(const float uct_constant,
                      const float gamma) {
        float bestQ = -INF;
        int bestAction = -1;
        Node *bestNode = NULL;
        float parent_count = this->uct_count;
        vector<float> Q(sz(children));
        vector<Node *> N(sz(children));
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            float qValue = (it->second).first->getUCTQ(it->second.second,parent_count,
                           uct_constant,gamma);
            Q[it->first] = qValue;
            N[it->first] = it->second.first;
            if(bestQ < qValue) {
                bestQ = qValue;
                bestAction = it->first, 
                bestNode = it->second.first;
            }
        } 
        // break ties randomly
        VI same;
        REP(i,sz(Q)) if(abs(bestQ - Q[i]) < EPS) same.pb(i);
        if(sz(same) > 1) {
            bestAction = same[rand()%sz(same)];
            bestNode = N[bestAction];
        }

        assert(bestAction != -1 && bestNode != NULL);
        return make_pair(bestNode, bestAction);
    }

    int getRandomAction(int num_actions) {
        vector<int> unexplored;
        for(int i=0;i<num_actions;i++) 
            if(find(all(exploredActions),i) == exploredActions.end()) unexplored.pb(i);

        return unexplored[rand() % sz(unexplored)];
    }

   int getGreedyAction(const float gamma) {
        float bestQ = -INF;
        float bestCount = -INF;
        int bestAction = -1;
        // pick bestQ, break ties using visit counts
        vector<float> Q,C;
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            float count = (it->second.first)->uct_count;
            float value = (it->second.first)->uct_return / count;
            float qValue = it->second.second + gamma * value;
            Q.pb(qValue);
            C.pb(count);
            cout<<it->first<<" "<<qValue<<" "<<count<<endl;
            if(qValue - bestQ > EPS) bestQ = qValue, bestAction = it->first, bestCount = count;
            else if(qValue > 0 && abs(bestQ - qValue) < EPS && bestCount < count) bestQ = qValue, bestAction = it->first, bestCount = count;
        }

        // If there are still ties, break randomly
        VI same;
        REP(i,sz(Q)) {
            if(abs(Q[i] - bestQ) < EPS) {
                if(C[i] == bestCount) same.pb(i);
            }
        }
        if(sz(same) > 1) bestAction = same[rand() % sz(same)];

        assert(bestAction != -1);
        return bestAction;
    } 
    
    pair<Node *,int> treePolicy(Freeway& game, stack<float>& rewards, 
                     const float uct_constant, const float gamma,
                     int num_actions, float& oneStepReward) {
        if(sz(this->exploredActions) == num_actions) {
            pair<Node *,int> bestChild = this->getBestAction(uct_constant,gamma);
            float reward = game.processAction(bestChild.second);
            rewards.push(reward);
            //cout<<bestChild.second<<" -> ";
            return (bestChild.first)->treePolicy(game, rewards,uct_constant, 
                                      gamma,num_actions, oneStepReward);
        }
        else {
            pair<Node *,int> randomChild = make_pair(this,this->getRandomAction(num_actions));
            //cout<<randomChild.second<<endl;
            // load this state, take the action
            float reward = game.processAction(randomChild.second);
            oneStepReward = reward;
            rewards.push(reward);
            return randomChild;
        }
    }

    Node *retainOptimalBranch(int branch) {
        Node *new_root = children[branch].first;
        new_root->parent = NULL;
        children.erase(branch);
        return new_root;
    }

    ~Node() {
        for (Nodes::iterator iter=children.begin(); iter!=children.end(); iter++) {
            Node *child = iter->second.first;
            delete child;
        }
    }
    
};

#endif
