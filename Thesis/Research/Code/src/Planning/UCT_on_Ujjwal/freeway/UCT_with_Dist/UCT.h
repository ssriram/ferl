#ifndef __UCT__
#define __UCT__

#include <list>
#include <stack>
#include <algorithm>
#include <vector>
#include <utility>
#include <cassert>
#include <map>
#include <queue>

#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define all(c) c.begin(),c.end()
#define INF (int) 1e9
#define EPS 1e-3
#define VI vector<int>
#ifndef PII
    #define PII pair<int,int>
#endif


#include <Freeway.h>

using namespace std;

typedef map<PII,pair<int,float> > Trans;

class Node;

typedef map<int,pair<Node *,float> > Nodes;

class Node {
    private:
    Node *parent;
    Nodes children;
    float uct_count;
    float uct_return;
    float sigma;
    vector<int> exploredActions;

    public:
    int idx; // index into Dist and cache

    Node(float uct_return, float uct_count, float sigma,
         int idx, Node *parent) {
        this->uct_return = uct_return;
        this->uct_count = uct_count;
        this->sigma = sigma;
        this->idx = idx;
        this->parent = parent;
    }

    void addChild(Node *child, int action, float reward) {
        this->children[action] = make_pair(child,reward);
        this->exploredActions.pb(action);
    }

    void decaySigma(const float decay) {
        this->sigma = max((float)EPS, this->sigma * decay);
    }
    
    void update(float totalReward, const float gamma, stack<float>& rewards) {
        assert(sz(rewards) > 0);
        float reward = rewards.top() + gamma * totalReward;
        totalReward = reward;
        rewards.pop();
        this->uct_count += 1.0;
        this->uct_return += reward;
        if(this->parent) (this->parent)->update(totalReward,gamma,rewards);
    }

    pair<float,float> getStats(vector<VI >& D, vector<Node *>& v) {
        float final_count = 0;
        float final_return = 0;
        int n = sz(v);
        REP(i,n) {
            float distance = D[this->idx][v[i]->idx];
            float weight = exp(-distance / this->sigma);
            final_count += weight * v[i]->uct_count;
            final_return += weight * v[i]->uct_return;
        }
        return make_pair(final_count,final_return);
    }

    float getUCTQ(pair<float,float>& p, float reward,
          float parent_count, const float uct_constant,const float gamma) {
        float count = p.first;
        float value = reward + gamma * (p.second / p.first);
        return value + uct_constant * sqrt(2*logf(parent_count)/ count);
    }

    pair<Node *, int> getBestAction(vector<VI >& D,vector<Node *>& v,
                      const float uct_constant, const float gamma,
                      const float decay) {
        float bestQ = -INF;
        int bestAction = -1;
        Node *bestNode = NULL;
        vector<float> Q(sz(children));
        vector<Node *> N(sz(children));
        vector<pair<float,float> > C(sz(children));
        float parent_count = 0;
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            C[it->first] = (it->second).first->getStats(D,v);
            parent_count += C[it->first].first;
        }
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            float qValue = (it->second).first->getUCTQ(C[it->first],
                           it->second.second, parent_count,
                           uct_constant,gamma);
            Q[it->first] = qValue;
            N[it->first] = it->second.first;
            if(bestQ < qValue) {
                bestQ = qValue;
                bestAction = it->first, 
                bestNode = it->second.first;
            }
            it->second.first->decaySigma(decay);
        } 
        // break ties randomly
        VI same;
        REP(i,sz(Q)) if(abs(bestQ - Q[i]) < EPS) same.pb(i);
        if(sz(same) > 1) {
            bestAction = same[rand()%sz(same)];
            bestNode = N[bestAction];
        }

        assert(bestAction != -1 && bestNode != NULL);
        return make_pair(bestNode, bestAction);
    }

    int getRandomAction(int num_actions) {
        vector<int> unexplored;
        for(int i=0;i<num_actions;i++) 
            if(find(all(exploredActions),i) == exploredActions.end()) unexplored.pb(i);

        return unexplored[rand() % sz(unexplored)];
    }

    int getGreedyAction(const float gamma) {
        float bestQ = -INF;
        float bestCount = -INF;
        int bestAction = -1;
        // pick bestQ, break ties using visit counts
        vector<float> Q,C;
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            float count = (it->second.first)->uct_count;
            float value = (it->second.first)->uct_return / count;
            float qValue = it->second.second + gamma * value;
            Q.pb(qValue);
            C.pb(count);
            cout<<it->first<<" "<<qValue<<" "<<count<<endl;
            if(qValue - bestQ > EPS) bestQ = qValue, bestAction = it->first, bestCount = count;
            else if(abs(bestQ - qValue) < EPS && bestCount < count) bestQ = qValue, bestAction = it->first, bestCount = count;
        }

        // If there are still ties, break randomly
        VI same;
        REP(i,sz(Q)) {
            if(abs(Q[i] - bestQ) < EPS) {
                if(C[i] == bestCount) same.pb(i);
            }
        }
        if(sz(same) > 1) bestAction = same[rand() % sz(same)];

        assert(bestAction != -1);
        return bestAction;
    }

    int exists(vector<state>& v, state s) {
        for(int i=sz(v)-1;i>=0;i--) if(v[i].equals(s)) return i;

        return -1;
    }

    void updateDist(vector<VI >& D, int par, int idx) {
        if(D[idx][par] > 1) {
            D[idx][par] = 1;
            D[par][idx] = 1;
            int n = sz(D);
            REP(i,n) {
                REP(j,n) {
                    if(j == i) continue;
                    if(D[i][idx] + D[idx][par] + D[par][j] < D[i][j]) {
                        D[i][j] = D[i][idx] + D[idx][par] + D[par][j];
                    }
                    if(D[i][par] + D[par][idx] + D[idx][j] < D[i][j]) {
                        D[i][j] = D[i][par] + D[par][idx] + D[idx][j];
                    }
                }
            }
        }
    }

    void addDist(vector<VI >& D, int par) {
        int n = sz(D);
        VI v(n+1,INF);
        v[n] = 0;
        v[par] = 1;
        D[par].pb(1);
        REP(i,n) {
            if(i == par) continue;
            D[i].pb(D[i][par] + 1);
            v[i] = min(v[i],1 + D[par][i]);
        }
        D.pb(v);
    }

    pair<Node *,int> treePolicy(Freeway& game, map<PII,pair<int,float> >& M,
                     vector<VI >& D, vector<state>& cache, vector<Node *>& v,
                     stack<float>& rewards, const float uct_constant, 
                     const float gamma, const float decay, int num_actions,
                     int& idx, float& oneStepReward, bool limitReached) {
        if(sz(this->exploredActions) == num_actions) {
            pair<Node *,int> bestChild = this->getBestAction(D,v,uct_constant,gamma,decay);
            idx = M[make_pair(idx,bestChild.second)].first;
            float reward = game.processAction(bestChild.second);
            rewards.push(reward);
            //cout<<bestChild.second<<" -> ";
            return (bestChild.first)->treePolicy(game,M,D,cache,v,rewards,uct_constant, 
                                      gamma,decay,num_actions,idx,oneStepReward,
                                      limitReached);
        }
        else {
            pair<Node *,int> randomChild;
            if(!limitReached) {
                randomChild = make_pair(this,this->getRandomAction(num_actions));
                //cout<<randomChild.second<<endl;
                float reward = game.processAction(randomChild.second);
                state s = game.getState();
                int present = exists(cache,s);
                int next = -1;
                if(present != -1) {
                    updateDist(D,idx,present); 
                    next = present;
                }
                else {
                    addDist(D,idx);
                    next = sz(cache);
                    cache.pb(s);
                }
                oneStepReward = reward;
                rewards.push(reward);
                PII p = make_pair(idx,randomChild.second);
                if(M.find(p) == M.end()) { 
                    M[p] = make_pair(next,reward);
                    idx = next;
                }
                else idx = M[p].first;
            }
            else {
                //cout<<endl;
                randomChild = make_pair(this,-1);
            }
            return randomChild;
        }
    }

    void updateDS(Trans& oldM, Trans& newM,
         vector<VI >& oldD, vector<VI >& newD,
         vector<state>& oldCache, vector<state>& newCache,
         vector<Node *>& v, int branch) {
        // Clear new DS
        newM.clear();
        newCache.clear();
        newD.clear();
        v.clear();

        // Build stateMap in BFS manner
        queue<Node *> Q;
        Q.push(this);
        int index = 0;
        map<int,int> stateMap; 
        int idx = oldM[make_pair(0,branch)].first;
        stateMap[idx] = index;
        this->idx = index;
        newCache.pb(oldCache[idx]);
        while(!Q.empty()) {
            Node *p = Q.front();
            v.pb(p);
            for(Nodes::iterator it=p->children.begin();it != p->children.end();it++) {
                Node *child = it->second.first;
                Q.push(child);
                if(stateMap.find(child->idx) == stateMap.end()) {
                    stateMap[child->idx] = index + 1;
                    child->idx = index + 1;
                    newCache.pb(oldCache[child->idx]);
                    ++index;
                }
                else child->idx = stateMap[child->idx];
            }
            Q.pop();
        } 

        // Update Dist
        VI keys;
        for(map<int,int>::iterator it=stateMap.begin();it!=stateMap.end();it++) {
            keys.pb(it->first);
        }
        int n = sz(keys);
        newD.resize(n,VI(n,INF));
        REP(i,n) {
            int curr = stateMap[keys[i]];
            REP(j,n) {
                int next = stateMap[keys[j]];
                newD[curr][next] = oldD[keys[i]][keys[j]]; 
            }
        }
        
        // Update trans map
        for(Trans::iterator it=oldM.begin();it!=oldM.end();it++) {
            PII p = it->first;
            pair<int,float> q = it->second;
            if(stateMap.find(p.first) == stateMap.end()) continue;
            int curr = stateMap[p.first];
            int next = stateMap[q.first];
            newM[make_pair(curr,p.second)] = make_pair(next,q.second);
        }
    }

    Node *retainOptimalBranch(int branch) {
        Node *new_root = children[branch].first;
        new_root->parent = NULL;
        children.erase(branch);
        return new_root;
    }

    ~Node() {
        for (Nodes::iterator iter=children.begin(); iter!=children.end(); iter++) {
            Node *child = iter->second.first;
            delete child;
        }
    }
    
};

#endif
