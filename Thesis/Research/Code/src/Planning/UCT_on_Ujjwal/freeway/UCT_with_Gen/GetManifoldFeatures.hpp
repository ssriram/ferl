/************************************************************************************
Author - Sriram Srinivasan
Purpose - Learn a local manifold and translation operator from Atari Game States
************************************************************************************/

#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <cstdio>

#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#ifndef PII
#define PII pair <int,int>
#endif
typedef long long LL;

/* DSYEVR prototype */
extern "C" void dsyevr_( char* jobz, char* range, char* uplo, int* n, double* a,
                int* lda, double* vl, double* vu, int* il, int* iu, double* abstol,
                int* m, double* w, double* z, int* ldz, int* isuppz, double* work,
                int* lwork, int* iwork, int* liwork, int* info );

using namespace std;

#define NSELECT 100

int exists(vector<state>& v, state& p) {
    for(int i=sz(v)-1;i>=0;i--) {
        if(v[i].equals(p)) return i;
    }    
    return -1;
}

void generateBFS(Freeway& game, vector<VI >& rel, map<PII,pair<int,float> >& M,
                 const int num_actions, const int budget) {
    state start = game.getState();
    queue<state> Q; 
    Q.push(start);
    vector<state> v;
    v.pb(start);
    int prev = 0;
    while(!Q.empty() && sz(rel) < budget) {
        state current = Q.front();
        game.loadState(current);
        REP(i,num_actions) {
            game.saveState();
            float reward = game.processAction(i);
            state next = game.getState();
            int present = exists(v,next);
            if(present == -1) {
                v.pb(next);
                Q.push(next);
                int index = sz(rel);
                rel[prev].pb(index);
                VI p;
                p.pb(prev);
                rel.pb(p);
                M[make_pair(prev,i)] = make_pair(index,reward);
            }
            else {
                if(find(all(rel[prev]),present) == rel[prev].end()) {
                    rel[prev].pb(present);
                    if(present != prev) rel[present].pb(prev);
                }
                M[make_pair(prev,i)] = make_pair(present,reward);
            }
            game.restoreState();
        }
        Q.pop();
        if(sz(rel) > prev + 1) ++prev;
    }
}

void learnTranslationOp(map<PII, pair<int,float> >& M, vector<vector<float> >& Dist,
                        vector<vector<float> >& Op) {
    map<PII, pair<int,float> >::iterator it;
    int start_state, action, end_state;
    map<int,int> Num_Actions;
    int num_dim = sz(Dist[0]);
    for(it = M.begin();it!=M.end();it++) {
        start_state = (it->first).first;
        action = (it->first).second;
        end_state = it->second.first;
        REP(k,num_dim) Op[action][k] += (Dist[end_state][k] - Dist[start_state][k]);
        Num_Actions[action]++;
    }
    REP(i,sz(Num_Actions)) REP(k,num_dim) Op[i][k] /= Num_Actions[i];
}

void getEmbedding(map<PII,pair<int,float> >&M,
                  vector<vector<float> >& embedding,
                  vector<VI >& rel) {
    int nV = sz(rel);

    // Compute all-pairs shortest path
    float D[nV][nV];
    REP(i,nV) REP(j,nV) D[i][j] = INF, D[i][i] = 0;
    REP(i,nV) REP(j,sz(rel[i])) if(rel[i][j] != i) D[i][rel[i][j]] = 1;

    REP(k,nV) {
        REP(i,nV) {
            REP(j,nV) {
                D[i][j] = min(D[i][j],D[i][k]+D[k][j]);
            }
        }
    }

    // Apply Classical MDS on D^2
    REP(i,nV) REP(j,nV) D[i][j] = D[i][j]*D[i][j];
    
    // Center D^2 matrix
    float rowSum[nV], colSum[nV], totSum = 0;
    REP(i,nV) {
        rowSum[i] = 0;colSum[i] = 0;
        REP(j,nV) rowSum[i] += D[i][j], colSum[i] += D[j][i], totSum += D[i][j];
    }
    REP(i,nV) REP(j,nV) D[i][j] = -(D[i][j] - rowSum[i]/nV - colSum[j]/nV + totSum/(nV*nV))/2;

    /* Store 2D matrix as 1D array */
    double a[nV*nV];
    REP(i,nV) REP(j,nV) a[i*nV+j] = (double) D[i][j];

    // Get top 100 eigen vectors
    int n = nV, il, iu, m, lda = nV, ldz = nV, info, lwork, liwork;
    int LDZ = n;
    double abstol, vl, vu;
    int iwkopt;
    int* iwork;
    double wkopt;
    double* work;
    /* Local arrays */
    int isuppz[n];
    double w[n], z[LDZ*NSELECT];
    /* Negative abstol means using the default value */
    abstol = -1.0;
    /* Set il, iu to compute NSELECT largest eigenvalues */
    il = max(1, n -NSELECT + 1);
    iu = n;

    /* Query and allocate the optimal workspace */
    lwork = -1;
    liwork = -1;
    dsyevr_( "Vectors", "Indices", "Upper", &n, a, &lda, &vl, &vu, &il, &iu,
                    &abstol, &m, w, z, &ldz, isuppz, &wkopt, &lwork, &iwkopt, &liwork,
                    &info );
    lwork = (int)wkopt;
    work = (double*)malloc( lwork*sizeof(double) );
    liwork = iwkopt;
    iwork = (int*)malloc( liwork*sizeof(int) );
    /* Solve eigenproblem */
    dsyevr_( "Vectors", "Indices", "Upper", &n, a, &lda, &vl, &vu, &il, &iu,
                    &abstol, &m, w, z, &ldz, isuppz, work, &lwork, iwork, &liwork,
                    &info );
    free(work);
    free(iwork);
    /* Check for convergence */
    if( info > 0 ) {
        printf( "The algorithm failed to compute eigenvalues.\n" );
        exit( 1 );
    }

    // Pick only the top eigen vectors
    VI picked;
    double topEigenValue = w[m-1];
    picked.pb(m-1);
    for(int i=m-2;i>=0;i--) {
        if(w[i] < 0.2 * topEigenValue) {
            //picked.pb(i);
            break;
        }
        else picked.pb(i);
    }

    int topK = sz(picked);
    //cout<<"Reduced Dimensionality = "<<topK<<endl;
    embedding.resize(nV,vector<float>(topK));
    REP(i,nV) {
        REP(j,topK) embedding[i][j] = z[i+n*picked[j]] * sqrt(w[picked[j]]);
    }
}

vector<vector<float> > learnManifold(Freeway& game,
                       vector<vector<float> >& embedding,
                       map<PII,pair<int,float> >& M,
		               vector<VI >& rel,
		               const int num_actions,
                       const int budget) {
    rel.clear();
    VI empty;
    rel.pb(empty);
    generateBFS(game,rel,M,num_actions,budget);
    if(sz(rel) <= 0) {
    REP(i,sz(rel)) {
        REP(j,sz(rel[i])) cout<<rel[i][j]<<" ";
        cout<<endl;
    }
    }

    getEmbedding(M,embedding,rel); 

    /* Learn a translation operator */
    int topK = sz(embedding[0]);
    vector<vector<float> > translation(num_actions,vector<float>(topK,0));
    learnTranslationOp(M,embedding,translation);
    return translation;

}
