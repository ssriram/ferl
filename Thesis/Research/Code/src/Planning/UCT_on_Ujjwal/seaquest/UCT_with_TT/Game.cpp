/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Running UCT on Grid World
*******************************************************************************/
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include "UCT.h"

using namespace std;

int exists(vector<Entry>& table, state st) {
    for(int i=sz(table)-1;i>=0;i--) {
        if(st.equals(table[i].s)) return i;
    }
    return -1;
}

map<string, float> parseParams(const char *param_file) {
    map<string,float> M;
    ifstream file (param_file);
    assert(file.is_open());
    string temp, key;
    float val;
    while(!file.eof()) {
        getline(file,temp,'\n');
        std::istringstream iss(temp);
        iss >> key;
        iss >> val;
        M[key] = val;
    }
    return M;
}

void validateParams(map<string, float>& M) {
    const char *keys[] = {"uct_constant", "gamma", "num_episodes", "depth"};
    vector<string> v(keys,keys+4);
    map<string, float>::iterator it;
    REP(i,sz(v)) assert(M.find(v[i]) != M.end());
}

void displayParams(map<string,float>& M) {
    map<string, float>::iterator it;
    for(it=M.begin();it!=M.end();it++) cout<<it->first<<" "<<it->second<<endl;
}

float defaultPolicy(Seaquest& game, int num_actions, int depth, const float gamma) {
    if(depth == 0) return 1e-5;
    else if(!game.isActive()) return 0;
    else {
        int action = rand() % num_actions;
        float reward = game.processAction(action);
        return reward + gamma * defaultPolicy(game,num_actions,depth-1,gamma);
    }    
}

pair<Node *,int> runUCT(map<string,float>& Params, Seaquest& game,
                        Node *start, vector<Entry>& table) {
    //cout<<"Running Simulations\n";
    if(start == NULL) {
        Entry e(game.getState(),0,0);
        table.pb(e);
        start = new Node(0,0,0,NULL); 
    }
    int num_actions = game.getNumActions();
    int numSim = (int) Params["num_sim"];
    int depth = (int) Params["depth"];
    const float uct_constant = Params["uct_constant"];
    const float gamma = Params["gamma"];
    
    stack<float> rewards;
    REP(i,numSim) {
        Node *p = start;
        assert(sz(rewards) == 0);
        game.restoreState();
        game.saveState();
        // Pick the node from which a roll-out should be done
        float oneStepReward = 0;
        pair<Node *,int> next = p->treePolicy(game,rewards,table,
                                uct_constant,gamma,
                                num_actions,oneStepReward);
        int idx = exists(table,game.getState());
        if(idx == -1) {
            Entry e(game.getState(),0,0);
            table.pb(e);
            idx = sz(table) - 1;
        }
        Node *child = new Node(0,0,idx,next.first);
        (next.first)->addChild(child,next.second,oneStepReward);
        // Do a roll-out from this node
        float rolloutReward = defaultPolicy(game,num_actions,depth,gamma);
        rewards.push(rolloutReward);
        // Update nodes
        child->update(0,gamma,rewards,table);
    }
    int greedyAction = start->getGreedyAction(table,gamma);
    Node *new_root = start->retainOptimalBranch(greedyAction);
    delete start;
    return make_pair(new_root,greedyAction);
}

int main(int argc, char **argv) {
    if(argc < 2) {
        std::cerr << "Usage: " << argv[0] << " param_file"<<std::endl;
        return 1;
    }

    map<string,float> Params = parseParams(argv[1]);
    validateParams(Params);
    displayParams(Params);

    int numEpisodes = (int) Params["num_episodes"];

    srand(time(NULL)); // seed rand num generator

    Seaquest game(0,0);

    vector<Entry> table;

    REP(i,numEpisodes) {
        float totalReward = 0;
        Node *root = NULL;
        int action = -1;
        game.reset();
        table.clear();
        while(game.isActive()) {
            game.saveState();
            pair<Node *,int> p = runUCT(Params,game,root,table);
            root = p.first, action = p.second;
            game.restoreState();
            float reward = game.processAction(action);
            totalReward += reward;
            cout<<"action = "<<action<<", reward = "<<reward<<endl;
        }
        printf("Episode %d ended with total reward = %f\n",i,totalReward);
        cout<<"size of table = "<<sz(table)<<endl;
        delete root;
    }
}
