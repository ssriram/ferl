/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Running UCT on Grid World
*******************************************************************************/
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include "UCT.h"

using namespace std;

typedef map<PII,pair<int,float> > Trans;

map<string, float> parseParams(const char *param_file) {
    map<string,float> M;
    ifstream file (param_file);
    assert(file.is_open());
    string temp, key;
    float val;
    while(!file.eof()) {
        getline(file,temp,'\n');
        std::istringstream iss(temp);
        iss >> key;
        iss >> val;
        M[key] = val;
    }
    return M;
}

void validateParams(map<string, float>& M) {
    const char *keys[] = {"uct_constant", "gamma", "num_episodes", "depth",
                          "sigma", "sigma_decay", "node_limit"};
    vector<string> v(keys,keys+7);
    map<string, float>::iterator it;
    REP(i,sz(v)) assert(M.find(v[i]) != M.end());
}

void displayParams(map<string,float>& M) {
    map<string, float>::iterator it;
    for(it=M.begin();it!=M.end();it++) cout<<it->first<<" "<<it->second<<endl;
}

float defaultPolicy(Invaders& game, int num_actions, int depth, const float gamma) {
    if(depth == 0) return 1e-5;
    else if(!game.isActive()) return 0;
    else {
        int action = rand() % num_actions;
        float reward = game.processAction(action);
        return reward + gamma * defaultPolicy(game,num_actions,depth-1,gamma);
    }    
}

pair<Node *,int> runUCT(map<string,float>& Params, Invaders& game,
                        Trans& M, vector<VI >& Dist,
                        vector<state>& cache, vector<Node *>& v,
                        Node *start) {
    int num_actions = game.getNumActions();
    int numSim = (int) Params["num_sim"];
    int depth = (int) Params["depth"];
    const float uct_constant = Params["uct_constant"];
    const float gamma = Params["gamma"];
    float sigma = Params["sigma"];
    const float decay = Params["sigma_decay"];
    const int nodeLimit = (int) Params["node_limit"];

    bool insert_root = start == NULL;
    start = start == NULL ? (Node *) new Node(0,0,sigma,0,NULL) 
            : start;
    
    if(insert_root) v.pb(start);
    stack<float> rewards;
    int numNodes = sz(v);
    REP(i,numSim) {
        assert(sz(Dist) == sz(cache));
        Node *p = start;
        int step = 0;
        assert(sz(rewards) == 0);
        game.restoreState();
        game.saveState();
        // Pick the node from which a roll-out should be done
        float oneStepReward = 0;
        pair<Node *,int> next = p->treePolicy(game,M,Dist,
                                cache,v,rewards,uct_constant,gamma,
                                decay,num_actions,step,oneStepReward,
                                numNodes >= nodeLimit);
        start->decaySigma(decay);

        Node *child;
        if(numNodes < nodeLimit) {
            child = new Node(0,0,sigma,step,next.first);
            (next.first)->addChild(child,next.second,oneStepReward);
            v.pb(child);
            ++numNodes;
        }
        else {
            child = next.first;
        }
        // Do a roll-out from this node
        float rolloutReward = defaultPolicy(game,num_actions,depth,gamma);
        rewards.push(rolloutReward);
        // Update nodes
        child->update(0,gamma,rewards);
    }
    int greedyAction = start->getGreedyAction(gamma);
    Node *new_root = start->retainOptimalBranch(greedyAction);
    delete start;
    return make_pair(new_root,greedyAction);
}

int main(int argc, char **argv) {
    if(argc < 2) {
        std::cerr << "Usage: " << argv[0] << " param_file"<<std::endl;
        return 1;
    }

    map<string,float> Params = parseParams(argv[1]);
    validateParams(Params);
    displayParams(Params);

    int numEpisodes = (int) Params["num_episodes"];

    Invaders game(0,0);

    int seed = time(NULL);
    cout<<"seed = "<<seed<<endl;
    srand(seed); // seed rand num generator

    /* Data Structures */
    vector<VI > oldDist, newDist;
    vector<state> oldCache, newCache;
    Trans oldM, newM;
    vector<Node *> v;

    /* Refs to Data Structures */
    vector<VI >& currD = oldDist, nextD = newDist;
    vector<state>& currCache = oldCache;
    vector<state>& nextCache = newCache;
    Trans& currM = oldM, nextM = newM;

    REP(i,numEpisodes) {
        float totalReward = 0;
        Node *root = NULL;
        int action = -1;

        /* Clear all data structs */
        game.reset();
        currM.clear();
        currD.clear();
        currCache.clear();

        nextM.clear();
        nextD.clear();
        VI empty;
        empty.pb(0);
        nextD.pb(empty);
        nextCache.clear();
        nextCache.pb(game.getState());

        v.clear();

        while(game.isActive()) {
            game.saveState();
            v.clear();

            if(root) root->updateDS(currM,nextM,currD,nextD,
                     currCache,nextCache,v,action);

            pair<Node *,int> p = runUCT(Params,game,nextM,nextD,
                                 nextCache,v,root);
            root = p.first, action = p.second;

            game.restoreState();
            float reward = game.processAction(action);
            totalReward += reward;
            cout<<"action = "<<action<<", reward = "<<reward<<endl;
        
            if(game.isActive()) {
                swap(currM,nextM);
                swap(currD,nextD);
                swap(currCache,nextCache);    
            }
        }
        printf("Episode %d ended with total reward = %f\n",i,totalReward);
        delete root;
    }
}
