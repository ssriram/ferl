/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Running UCT with Nearest Neighbor on Grid World
*******************************************************************************/
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include "UCT.h"
#include "board.h"

using namespace std;

map<string, float> parseParams(const char *param_file) {
    map<string,float> M;
    ifstream file (param_file);
    assert(file.is_open());
    string temp, key;
    float val;
    while(!file.eof()) {
        getline(file,temp,'\n');
        std::istringstream iss(temp);
        iss >> key;
        iss >> val;
        M[key] = val;
    }
    return M;
}

void validateParams(map<string, float>& M) {
    const char *keys[] = {"N", "uct_constant", "num_features", "num_sim", "num_actions", "num_episodes", "depth"};
    vector<string> v(keys,keys+7);
    map<string, float>::iterator it;
    REP(i,sz(v)) assert(M.find(v[i]) != M.end());
}

void displayParams(map<string,float>& M) {
    map<string, float>::iterator it;
    for(it=M.begin();it!=M.end();it++) cout<<it->first<<" "<<it->second<<endl;
}

string getAction(int action) {
    if(action == UP) return "UP";
    else if(action == DOWN) return "DOWN";
    else if(action == LEFT) return "LEFT";
    else return "RIGHT";
}

int runUCT(map<string,float>& Params, Board& b, Point *q) {
    //cout<<"Running Simulations\n";
    const int num_actions = Params["num_actions"];
    const int numSim = (int) Params["num_sim"];
    const int depth = (int) Params["depth"];
    const float uct_constant = Params["uct_constant"];
    const int num_features = Params["num_features"];
    vector<Node> v(depth,Node(num_actions,num_features));
    Point currState(num_features,q), nextState(num_features);
    Point *curr, *next;
    curr = &currState, next = &nextState;
    vector<Point> states(depth, Point(num_features));
    vector<int> actions(depth);

    REP(i,numSim) {
        float sum = 0;
        stack<float> rewards;
        curr->setPoint(q);
        // Run a simulation/roll-out
        //printf("Running roll-out %d\n",i);
        for(int k=0;k<depth;k++) {
            //curr->displayPoint();
            int action = v[k].getNextAction(uct_constant,curr);  
            b.getNextState(curr,next,action);
            float reward = next->getReward(&(b.goal)); 
            //cout<<"action = "<<getAction(action)<<", reward = "<<reward<<endl;
            rewards.push(sum);
            sum += reward;
            Point p(num_features,curr);
            states[k] = p;
            actions[k] = action;
            swap(curr,next);
        } 
        // Update return to nodes
        for(int k=depth-1;k>=0;k--) {
            v[k].updateNode(actions[k],sum - rewards.top(),&states[k]);
            rewards.pop();
        }
        
    }
    int greedyAction = v[0].getGreedyAction(q);
    return greedyAction;
}

int main(int argc, char **argv) {
    if(argc < 2) {
        std::cerr << "Usage: " << argv[0] << " param_file"<<std::endl;
        return 1;
    }

    map<string,float> Params = parseParams(argv[1]);
    validateParams(Params);
    displayParams(Params);

    const int num_features = Params["num_features"];
    Point goal(num_features);
    goal.coord[0] = 5;
    goal.coord[1] = 5;
    Board b((int) Params["N"], goal);
    int numEpisodes = (int) Params["num_episodes"];

    Point start(num_features);
    Point currState(num_features,&start),nextState(num_features,&start);
    Point *curr = &currState;
    Point *next = &nextState;
    const int episodeLength = 500;
    REP(i,numEpisodes) {
        float totalReward = 0;
        int length = 0;
        while(length < episodeLength) {
            Point savedState(num_features,curr);
            int action = runUCT(Params,b,curr);
            curr->setPoint(&savedState);
            b.getNextState(curr,next,action);
            float reward = next->getReward(&(b.goal));
            totalReward += reward;
            curr->displayPoint();
            cout<<getAction(action)<<", reward = "<<reward<<endl;
            swap(curr,next);
            ++length;
        }
        printf("Episode %d ended with total reward = %f\n",i,totalReward);
        curr->setPoint(&start);
    }
}
