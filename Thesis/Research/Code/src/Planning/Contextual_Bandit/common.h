#ifndef __COMMON__
#define __COMMON__

#include <cmath>
#include <iostream>
#include <vector>

#define EPS 1e-3

using namespace std;

typedef struct Point {
    private:
    int num_dim;
    public:
    vector<float> coord; // coordinates

    Point(const int num_dim) :
    coord(num_dim,0){
        this->num_dim = num_dim;
    }

    Point(const int num_dim, Point *p) :
    coord(num_dim){
        this->num_dim = num_dim;
        for(int i=0;i<num_dim;i++) coord[i] = p->coord[i]; 
    }
    
    void setPoint(Point *p) {
        for(int i=0;i<num_dim;i++) coord[i] = p->coord[i]; 
    }

    void displayPoint() {
        for(int i=0;i<num_dim;i++) std::cout<<coord[i]<<" ";
    }

    float getDistance(Point *q) {
        float distance = 0;
        for(int i=0;i<num_dim;i++) {
            distance += (this->coord[i] - q->coord[i]) * (this->coord[i] - q->coord[i]);
        }
        return sqrt(distance);
    }

    float getReward(Point *q) {
        //return this->equals(q) ? 1 : 0;
        return exp(-this->getDistance(q));
    }

    bool equals(Point *q) {
        return (this->getDistance(q) <= EPS);
    }

} Point;

enum {UP, DOWN, LEFT, RIGHT};

#endif
