/*******************************************************************************
Author - Sriram Srinivasan
Purpose - 2D - Grid World Domain
********************************************************************************/

#include "common.h"
#include <iostream>

#define REP(i,n) for(int i=0;i<n;i++)
#define sz(c) (signed int) c.size()

using namespace std;

class Board {
    private:
    int N; //grid size

    void applyAction(Point *curr, Point *next, int action) {
        next->setPoint(curr);
        if(action == UP) next->coord[0] = max((float) 0, next->coord[0] -1);
        else if(action == DOWN) next->coord[0] = min((float) N, next->coord[0] + 1);
        else if(action == LEFT) next->coord[1] = max((float) 0, next->coord[1] - 1);
        else next->coord[1] = min((float) N, next->coord[1] + 1);
    }

    public: 
    Point goal;
    Board(int N, Point goal):goal(goal) {
        this->N = N;
        normal_distribution<float> distribution(0.0,0.01);
    }

    void getNextState(Point *curr, Point *next, int action) {
        applyAction(curr,next,action);
    }

};
