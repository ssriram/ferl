#ifndef __UCT__
#define __UCT__

#include <list>
#include <stack>
#include <algorithm>
#include <vector>
#include <utility>
#include <cassert>

#include "common.h"

#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define all(c) c.begin(),c.end()
#define INF (int) 1e9

extern "C" {
    // LU decomoposition of a general matrix
    void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);

    // generate inverse of a matrix given its LU decomposition
    void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);
}

using namespace std;

class Node;

class Node {
    private:
    int num_weights;
    int num_actions;
    int num_tilings;
    vector<vector<float> > theta, b;
    vector<pair<pair<int,int>,int> > A;
    
    public:
    Node(const int num_actions, const int num_weights) : 
    theta(num_actions,vector<float>(num_weights,0)),
    b(num_actions,vector<float>(num_weights,0)),
    A(num_actions);
    {
        this->num_actions = num_actions;
        this->num_weights = num_weights;
    }

    void updateA(const int action, int *x) {
        for(int i=0;i<num_tilings;i++)
        updateAinv(action);
    }

    void updateAinv(const int action) {
        int N = num_weights;
        double a[N*N];
        REP(i,N) REP(j,N) a[i*N+j] = A[action][i][j];
        int *IPIV = new int[N+1];
        int LWORK = N*N;
        double *WORK = new double[LWORK];
        int INFO;

        dgetrf_(&N,&N,a,&N,IPIV,&INFO);
        dgetri_(&N,a,&N,IPIV,WORK,&LWORK,&INFO);

        REP(i,N) REP(j,N) Ainv[action][i][j] = a[i*N+j];

        delete IPIV;
        delete WORK;
    }

    void updateB(const int action, float reward, Point *x) {
        for(int i=0;i<num_weights;i++) {
            b[action][i] += reward * x->coord[i];
        }    
    }

    void updateTheta(const int action) {
        // theta = A^-1 * b
        for(int i=0;i<num_weights;i++) {
            float entry = 0;
            for(int j=0;j<num_weights;j++) entry += b[action][i] * Ainv[action][j][i];
            theta[action][i] = entry;
        }
    }

    void updateNode(const int action, float reward, Point *p) {
        updateA(action,p);
        updateB(action,reward,p);
        updateTheta(action);
        //displayParams(action);
    }

    float getQ(const int action, Point *p) {
        float q = 0;
        for(int i=0;i<num_weights;i++) q += theta[action][i] * p->coord[i];
        return q;
    }

    float getUCB(const int action, Point *p) {
        float ucb = 0;
        for(int i=0;i<num_weights;i++) {
            for(int j=0;j<num_weights;j++) ucb += p->coord[j] * Ainv[action][j][i] * p->coord[i];
        }
        return sqrt(ucb);
    }

    float getUCTQ(const int action, const float uct_constant, Point *p) {
        return  getQ(action,p) + uct_constant * getUCB(action,p);
    }

    int getNextAction(const float uct_constant, Point *p) {
        float bestQ = -INF;
        int bestAction = -1;
        vector<float> Q(num_actions);
        REP(i,num_actions) {
            float qValue = getUCTQ(i,uct_constant,p);
            Q[i] = qValue;
            if(bestQ < qValue) bestQ = qValue, bestAction = i;
        } 
        // if ties exists, break ties arbitrarily
        vector<float> same;
        REP(i,num_actions) if(Q[i] == bestQ) same.pb(i);
        if(sz(same) > 1) bestAction = same[rand() % sz(same)];

        assert(bestAction != -1);
        return bestAction;
    }

    int getGreedyAction(Point *p) {
        float bestQ = -INF;
        int bestAction = -1;
        vector<float> Q(num_actions);
        for(int i=0;i<num_actions;i++) {
            float qValue = getQ(i,p);
            Q[i] = qValue;
            if(bestQ < qValue) bestQ = qValue, bestAction = i;
        } 
        // if ties exists, break ties arbitrarily
        vector<float> same;
        REP(i,num_actions) if(Q[i] == bestQ) same.pb(i);
        if(sz(same) > 1) bestAction = same[rand() % sz(same)];

        assert(bestAction != -1);
        return bestAction;
    }

    void displayParams(const int action) {
        cout<<"A = \n";
        REP(i,num_weights) {
            REP(j,num_weights) cout<<A[action][i][j]<<" ";
            cout<<endl;
        }    
        cout<<"Ainv = \n";
        REP(i,num_weights) {
            REP(j,num_weights) cout<<Ainv[action][i][j]<<" ";
            cout<<endl;
        }
        cout<<"theta = \n";
        REP(i,num_weights) cout<<theta[action][i]<<" ";
        cout<<endl;
        cout<<"b = \n";
        REP(i,num_weights) cout<<b[action][i]<<" ";
        cout<<endl;
    }
    
    ~Node() {}

};

#endif
