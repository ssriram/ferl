/*******************************************************************************
Author - Sriram Srinivasan
Purpose - 2D - Grid World Domain
********************************************************************************/

#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <cstdio>

#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#ifndef PII
    #define PII pair <int,int>
#endif
typedef long long LL;

/* DSYEVR prototype */
extern "C" void dsyevr_( char* jobz, char* range, char* uplo, int* n, double* a,
                int* lda, double* vl, double* vu, int* il, int* iu, double* abstol,
                int* m, double* w, double* z, int* ldz, int* isuppz, double* work,
                int* lwork, int* iwork, int* liwork, int* info );

using namespace std;

#define NSELECT 100

int exists(vector<Point *>& v, Point *p) {
    for(int i=sz(v)-1;i>=0;i--) {
        if(v[i]->equals(p)) return i;
    }    
    return -1;
}

void generateBFS(Board *b, Point *start_state, vector<VI >& rel, map<PII,int>& M, const int num_actions, const int budget) {
    queue<Point *> Q; 
    vector<Point *> v;
    Point *start = new Point(start_state);
    Q.push(start);
    v.pb(start);
    int prev = 0;
    Point next_state;
    while(!Q.empty() && sz(rel) < budget) {
        Point *current = Q.front();
        REP(i,num_actions) {
            b->getNextState(current,&next_state,i); 
            int present = exists(v,&next_state);
            if(present == -1) {
                Point *next = new Point(&next_state);
                v.pb(next);
                Q.push(next);
                int index = sz(rel);
                rel[prev].pb(index);
                VI p;
                p.pb(prev);
                rel.pb(p);
                M[make_pair(prev,i)] = index;
            }
            else {
                if(!binary_search(all(rel[prev]),present)) {
                    rel[prev].pb(present);
                    if(present != prev) rel[present].pb(prev);
                }
                M[make_pair(prev,i)] = present;
            }
        }
        Q.pop();
        if(sz(rel) > prev + 1) ++prev;
    }
    REP(i,sz(v)) delete(v[i]);
}

void learnTranslationOp(map<PII, int>& M, vector<vector<float> >& Dist, vector<vector<float> >& Op) {
    map<PII, int>::iterator it;
    int start_state, action, end_state;
    map<int,int> Num_Actions;
    int num_dim = sz(Dist[0]);
    for(it = M.begin();it!=M.end();it++) {
        start_state = (it->first).first;
        action = (it->first).second;
        end_state = it->second;
        REP(k,num_dim) Op[action][k] += (Dist[end_state][k] - Dist[start_state][k]);
        Num_Actions[action]++;
    }
    REP(i,sz(Num_Actions)) REP(k,num_dim) Op[i][k] /= Num_Actions[i];
}

vector<vector<float> > getManifold(Board *b, Point *start, const int num_actions,
                 const int budget, map<PII,int>& M,
                 vector<vector<float> >& embedding) {
    vector<VI > rel; // adjacency list of states
    VI empty;
    rel.pb(empty);
    generateBFS(b,start,rel,M,num_actions,budget);
    int nV = sz(rel);
    //cout<<"# of unique states "<<nV<<endl;

    // Compute all-pairs shortest path
    double D[nV][nV];
    REP(i,nV) REP(j,nV) D[i][j] = INF, D[i][i] = 0;
    REP(i,nV) REP(j,sz(rel[i])) if(rel[i][j] != i) D[i][rel[i][j]] = 1;

    REP(k,nV) {
        REP(i,nV) {
            REP(j,nV) {
                D[i][j] = min(D[i][j],D[i][k]+D[k][j]);
            }
        }
    }

    // Apply Classical MDS on D^2
    REP(i,nV) REP(j,nV) D[i][j] = D[i][j]*D[i][j];
    
    // Center D^2 matrix
    double rowSum[nV], colSum[nV], totSum = 0;
    REP(i,nV) {
        rowSum[i] = 0;colSum[i] = 0;
        REP(j,nV) rowSum[i] += D[i][j], colSum[i] += D[j][i], totSum += D[i][j];
    }
    REP(i,nV) REP(j,nV) D[i][j] = -(D[i][j] - rowSum[i]/nV - colSum[j]/nV + totSum/(nV*nV))/2;

    /* Store 2D matrix as 1D array */
    double a[nV*nV];
    REP(i,nV) REP(j,nV) a[i*nV+j] = D[i][j];

    // Get top 100 eigen vectors
    int n = nV, il, iu, m, lda = nV, ldz = nV, info, lwork, liwork;
    int LDZ = n;
    double abstol, vl, vu;
    int iwkopt;
    int* iwork;
    double wkopt;
    double* work;
    /* Local arrays */
    int isuppz[n];
    double w[n], z[LDZ*NSELECT];
    /* Negative abstol means using the default value */
    abstol = -1.0;
    /* Set il, iu to compute NSELECT largest eigenvalues */
    il = max(1, n -NSELECT + 1);
    iu = n;

    /* Query and allocate the optimal workspace */
    lwork = -1;
    liwork = -1;
    dsyevr_( "Vectors", "Indices", "Upper", &n, a, &lda, &vl, &vu, &il, &iu,
                    &abstol, &m, w, z, &ldz, isuppz, &wkopt, &lwork, &iwkopt, &liwork,
                    &info );
    lwork = (int)wkopt;
    work = (double*)malloc( lwork*sizeof(double) );
    liwork = iwkopt;
    iwork = (int*)malloc( liwork*sizeof(int) );
    /* Solve eigenproblem */
    dsyevr_( "Vectors", "Indices", "Upper", &n, a, &lda, &vl, &vu, &il, &iu,
                    &abstol, &m, w, z, &ldz, isuppz, work, &lwork, iwork, &liwork,
                    &info );
    free(work);
    free(iwork);
    /* Check for convergence */
    if( info > 0 ) {
        printf( "The algorithm failed to compute eigenvalues.\n" );
        exit( 1 );
    }

    // Pick only the top eigen vectors
    VI picked;
    double topEigenValue = w[m-1];
    picked.pb(m-1);
    for(int i=m-2;i>=0;i--) {
        if(w[i] < 0.2 * topEigenValue) {
            //picked.pb(i);
            break;
        }
        else picked.pb(i);
    }

    int topK = sz(picked);
    //cout<<"Reduced Dimensionality = "<<topK<<endl;
    embedding.resize(nV,vector<float>(topK));
    REP(i,nV) {
        REP(j,topK) embedding[i][j] = z[i+n*picked[j]] * sqrt(w[picked[j]]);
    }

    /* Learn a translation operator */
    vector<vector<float> > translation(num_actions,vector<float>(topK,0));
    learnTranslationOp(M,embedding,translation);
    return translation;
}
