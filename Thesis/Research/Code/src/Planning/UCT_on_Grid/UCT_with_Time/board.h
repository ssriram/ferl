/*******************************************************************************
Author - Sriram Srinivasan
Purpose - 2D - Grid World Domain
********************************************************************************/

#include "common.h"
#include <iostream>

#define REP(i,n) for(int i=0;i<n;i++)
#define sz(c) (signed int) c.size()

using namespace std;

class Board {
    private:
    int N; //grid size
    int wall_length; // length of wall
    Point wall_start;

    void applyAction(Point *curr, Point *next, int action) {
        if(action == UP) next->setPoint(max((float) 0, curr->x -1), curr->y, curr->z + 1);
        else if(action == DOWN) next->setPoint(min((float) N-1, curr->x + 1), curr->y, curr->z + 1);
        else if(action == LEFT) next->setPoint(curr->x, max((float) 0, curr->y -1), curr->z + 1);
        else next->setPoint(curr->x, min((float) N-1, curr->y + 1), curr->z + 1);

        if(next->x == wall_start.x && next->y >= wall_start.y 
           && next->y <= wall_start.y + wall_length) {
            next->setPoint(curr);
            ++next->z;
        }
    }

    public: 
    Point goal;
    Board(int N, Point goal, Point *wall_start = NULL, int wall_length = 0):goal(goal),wall_start(wall_start) {
        this->N = N;
        this->wall_length = wall_length;
    }

    void getNextState(Point *curr, Point *next, int action) {
        applyAction(curr,next,action);
    }

    bool endOfEpisode(Point &p) {
        return goal.equals(p);
    }

};
