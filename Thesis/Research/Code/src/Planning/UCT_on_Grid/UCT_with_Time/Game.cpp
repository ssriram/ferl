/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Running UCT with nearest neighbor on manifold from Grid World
*******************************************************************************/
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include "UCT.h"
#include "board.h"
#include "GetManifoldFeatures.hpp"

using namespace std;

map<string, float> parseParams(const char *param_file) {
    map<string,float> M;
    ifstream file (param_file);
    assert(file.is_open());
    string temp, key;
    float val;
    while(!file.eof()) {
        getline(file,temp,'\n');
        std::istringstream iss(temp);
        iss >> key;
        iss >> val;
        M[key] = val;
    }
    return M;
}

void validateParams(map<string, float>& M) {
    const char *keys[] = {"N", "goalx", "goaly", "uct_constant", "num_sim",
                          "sigma_decay", "episode_length", "num_actions",
                          "gamma", "num_episodes", "budget", "depth"};
    vector<string> v(keys,keys+12);
    map<string, float>::iterator it;
    REP(i,sz(v)) assert(M.find(v[i]) != M.end());
}

void displayParams(map<string,float>& M) {
    map<string, float>::iterator it;
    for(it=M.begin();it!=M.end();it++) cout<<it->first<<" "<<it->second<<endl;
}

void getNextManifoldState(vector<float>& curr, vector<float>& action) {
    REP(i,sz(curr)) curr[i] += action[i];
}

float defaultPolicy(Board *b, Point p, int num_actions, int depth, const float gamma) {
    if(depth == 0) return 0;
    else {
        Point q;
        int action = rand() % num_actions;
        b->getNextState(&p,&q,action);
        return q.getReward(b->goal) + gamma * defaultPolicy(b,q,num_actions,depth-1,gamma);
    }    
}

int runUCT(map<string,float>& Params, Board *b, Point *q, map<PII,int>& M,
           vector<vector<float> >& embedding, vector<vector<float> >& Operator,
           int idx) {
    //cout<<"Running Simulations\n";
    float sigma = Params.find("sigma") == Params.end() ? 1.0 : Params["sigma"];
    Node *start = (Node *) new Node((float)0,(float)0,idx,sigma,q,embedding[idx],NULL);
    int num_actions = Params["num_actions"];
    int numSim = (int) Params["num_sim"];
    int depth = (int) Params["depth"];
    const float uct_constant = Params["uct_constant"];
    const float decay = Params["sigma_decay"];
    const float gamma = Params["gamma"];
    Point nextState;

    vector<Node *> v;
    v.pb(start);
    vector<float> manifoldState;
    int token = sz(embedding);
    
    REP(i,numSim) {
        Node *p = start;
        int step = idx;
        // Pick the node from which a roll-out should be done
        pair<Node *,int> next = p->treePolicy(v,M,uct_constant,num_actions,step,token,decay);
        start->decaySigma(decay);
        b->getNextState(&((next.first)->state),&nextState,next.second); 

        if(step != -1) manifoldState = embedding[step];
        else {
            manifoldState = embedding[next.first->index];
            getNextManifoldState(manifoldState,Operator[next.second]);
            embedding.pb(manifoldState);
            step = token;
            ++token;
        }
        
        Node *child = new Node((float)0,(float)0,step,sigma,&nextState,manifoldState,next.first);
        (next.first)->addChild(child,next.second);
        v.pb(child);
        // Do a roll-out from this node
        float totalReward = defaultPolicy(b,child->state,num_actions,depth,gamma);
        // Update nodes
        child->update(totalReward,b->goal,gamma);
    }
    int greedyAction = start->getGreedyAction();
    delete start;
    return greedyAction;
}

string getAction(int action) {
    if(action == UP) return "UP";
    else if(action == DOWN) return "DOWN";
    else if(action == LEFT) return "LEFT";
    else return "RIGHT";
}

int main(int argc, char **argv) {
    if(argc < 2) {
        std::cerr << "Usage: " << argv[0] << " param_file"<<std::endl;
        return 1;
    }

    map<string,float> Params = parseParams(argv[1]);
    validateParams(Params);
    displayParams(Params);

    Board *b = NULL;
    Point goal(Params["goalx"], Params["goaly"]);
    int wall_length = Params.find("wall_length") == Params.end() ?
                      0 : Params["wall_length"];
    if(wall_length) {
        Point wall(Params["wallx"],Params["wally"]);
        b=  new Board((int) Params["N"], goal, &wall, wall_length);
    }
    else b = new Board((int) Params["N"], goal);

    assert(b != NULL);
    Point currState(0,0,0),nextState(0,0,0);
    Point *curr = &currState;
    Point *next = &nextState;

    int numEpisodes = (int) Params["num_episodes"];
    const int episodeLength = (int) Params["episode_length"];

    srand(time(NULL)); // seed rand num generator
    map<PII,int> M;
    vector<vector<float> > embedding;
    vector<vector<float> > Operator;

    REP(i,numEpisodes) {
        float totalReward = 0;
        int length = 0;
        int idx = 0;
        while(length < episodeLength) {
            // Learn a manifold of the grid
            M.clear();
            embedding.clear();
            Operator = getManifold(b,curr,Params["num_actions"],
                       Params["budget"],M,embedding);
            printf("Manifold learnt for %d states with %d dimensions\n",sz(embedding),sz(embedding[0]));
/*
            cout<<"Operator learnt\n";
            REP(i,sz(Operator)) {
                REP(j,sz(Operator[0])) cout<<Operator[i][j]<<" ";
                cout<<endl;
            }
*/
            Point savedState(curr);
            int action = runUCT(Params,b,curr,M,embedding,Operator,idx);
            curr->setPoint(&savedState);
            b->getNextState(curr,next,action);
            float reward = next->getReward(b->goal);
            totalReward += reward;
            cout<<next->x<<" "<<next->y<<", action = "<<getAction(action)<<", reward = "<<reward<<endl;
            swap(curr,next);
            ++length;
        }
        printf("Episode %d ended with total reward = %f\n",i,totalReward);
        curr->setPoint(0,0,0); 
    }
    delete b;
}
