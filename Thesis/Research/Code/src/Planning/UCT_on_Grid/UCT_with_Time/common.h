#ifndef __COMMON__
#define __COMMON__

#include <cmath>
#include <iostream>

#define EPS 1e-3

using namespace std;

typedef struct Point {
    public:
    float x,y,z; // coordinates

    Point() {
        this->x = 0;
        this->y = 0;
        this->z = 0;
    }

    Point(Point *p) {
        if(p == NULL) this->x = this->y = this->z = -1;
        else this->x = p->x, this->y = p->y, this->z = p->z;
    }

    Point(float x,float y, float z = 0) {
        this->x = x;
        this->y = y;
        this->z = z;
    }
    
    void setPoint(float x,float y, float z = 0) {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    void setPoint(Point *p) {
        this->x = p->x;
        this->y = p->y;
        this->z = p->z;
    }

    void displayPoint() {
        cout<<this->x<<" "<<this->y<<" "<<this->z<<" ";
    }

    float getDistance(Point& q) {
        return sqrt((this->x - q.x) * (this->x - q.x) + 
               (this->y - q.y) * (this->y - q.y) + 
               (this->z - q.z) * (this->z - q.z));
    }

    float getDistance(Point *q) {
        return sqrt((this->x - q->x) * (this->x - q->x) +
               (this->y - q->y) * (this->y - q->y) +
               (this->z - q->z) * (this->z - q->z));
    }

    float getDistance2D(Point *q) { 
        return sqrt((this->x - q->x) * (this->x - q->x) +
               (this->y - q->y) * (this->y - q->y));
    }

    float getDistance2D(Point& q) { 
        return sqrt((this->x - q.x) * (this->x - q.x) +
               (this->y - q.y) * (this->y - q.y));
    }

    float getReward(Point& q) {
        return this->equals2D(q) ? 1 : 0;
        //return exp(-this->getDistance(q));
    }

    bool equals(Point& q) {
        return (this->getDistance(q) <= EPS);
    }

    bool equals(Point *q) {
        return (this->getDistance(q) <= EPS);
    }

    bool equals2D(Point *q) {
        return (this->getDistance2D(q) <= EPS);
    }

    bool equals2D(Point& q) {
        return (this->getDistance2D(q) <= EPS);
    }

} Point;

enum {UP, DOWN, LEFT, RIGHT};

#endif
