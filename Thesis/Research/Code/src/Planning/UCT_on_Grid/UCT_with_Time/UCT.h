#ifndef __UCT__
#define __UCT__

#include <list>
#include <stack>
#include <algorithm>
#include <vector>
#include <utility>
#include <cassert>

#include "common.h"

#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define all(c) c.begin(),c.end()
#define INF (int) 1e9
#ifndef PII
    #define PII pair<int,int>
#endif

using namespace std;

class Node;
typedef std::list<pair<Node*, int> > Nodes;

class Node {
    private:
    Node *parent;
    Nodes children;
    float uct_count;
    float uct_value;
    float sigma;
    vector<int> exploredActions;
    vector<float> coord; // manifold embedding of this state

    public:
    Point state;
    int index; // index into embedding vector
    Node(float uct_value, float uct_count, int index, float sigma, 
    Point *p, vector<float>& coord, Node *parent)
    :state(p),
    coord(coord) {
        this->uct_value = uct_value;
        this->uct_count = uct_count;
        this->index = index;
        this->sigma = sigma;
        this->parent = parent;
    }

    void decaySigma(const float decay) {
        this->sigma = max((float)EPS, this->sigma * decay);
    }

    void addChild(Node *child, int action) {
        (this->children).push_front(make_pair(child,action));
        this->exploredActions.pb(action);
    }
    
    void update(float totalReward, Point& q, const float gamma) {
        if(this->parent) totalReward = this->state.getReward(q) + gamma * totalReward;
        this->uct_count += 1.0;
        this->uct_value += (totalReward - this->uct_value) / this->uct_count;
        if(this->parent) (this->parent)->update(totalReward,q,gamma);
    }

    float getDistance(vector<float>& v) {
        float dist = 0;
        REP(i,sz(v)) dist += (this->coord[i] - v[i]) * (this->coord[i] - v[i]);
        return sqrt(dist);
    }

    pair<float,float> getStats(vector<Node *>& v) {
        float final_count = 0;
        float final_value = 0;
        REP(i,sz(v)) {
            float distance = this->getDistance(v[i]->coord);
            float weight = exp(-distance / this->sigma);
            final_count += weight * v[i]->uct_count;
            final_value += weight * v[i]->uct_value;
        }
        return make_pair(final_count,final_value);
    }

    float getUCTQ(vector<Node *>& v, const float uct_constant) {
        pair<float,float> p = this->getStats(v);
        float count = p.first;
        float value = p.second;
        float parent_count = parent->getStats(v).first;
        return value + uct_constant * sqrt(2*logf(parent_count)/ count);
    }

    pair<Node *, int> getBestAction(vector<Node *>& v, const float uct_constant,
                                   const float decay) {
        float bestQ = -INF;
        int bestAction = -1;
        Node *bestNode = NULL;
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            float qValue = (it->first)->getUCTQ(v,uct_constant);
            if(bestQ < qValue) bestQ = qValue, bestAction = it->second, bestNode = it->first;
            it->first->decaySigma(decay);
        } 
        assert(bestAction != -1 && bestNode != NULL);
        return make_pair(bestNode, bestAction);
    }

    int getRandomAction(int num_actions) {
        vector<int> unexplored;
        for(int i=0;i<num_actions;i++) 
            if(find(all(exploredActions),i) == exploredActions.end()) unexplored.pb(i);

        return unexplored[rand() % sz(unexplored)];
    }

    int getGreedyAction() {
        float bestQ = -INF;
        int bestAction = -1;
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            float qValue = (it->first)->uct_value;
            if(bestQ < qValue) bestQ = qValue, bestAction = it->second;
        } 
        assert(bestAction != -1);
        return bestAction;
    }

    pair<Node *,int> treePolicy(vector<Node *>& v, map<PII,int>& M, const float uct_constant,
                     int num_actions, int& idx, int token, const float decay) {
        if(sz(this->exploredActions) == num_actions) {
            pair<Node *,int> bestChild = this->getBestAction(v,uct_constant,decay);
            idx = M[make_pair(idx,bestChild.second)];
            return (bestChild.first)->treePolicy(v,M,uct_constant,
                                      num_actions,idx,token,decay);
        }
        else {
            pair<Node *,int> randomChild = make_pair(this,this->getRandomAction(num_actions));
            PII next = make_pair(idx,randomChild.second);
            if(M.find(next) == M.end()) {
                M[next] = token;
                idx = -1;
            }
            else idx = M[next];

            return randomChild;
        }
    }

    ~Node() {
        for (Nodes::iterator iter=children.begin(); iter!=children.end(); iter++) {
            Node *child = iter->first;
            delete child;
        }
    }
    
    string getAction(int action) {
        if(action == UP) return "UP";
        else if(action == DOWN) return "DOWN";
        else if(action == LEFT) return "LEFT";
        else return "RIGHT";
    }

};

#endif
