/************************************************************************************
Author - Sriram Srinivasan
Purpose - Learn a local manifold and translation operator from Atari Game States
************************************************************************************/

#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <cstdio>

#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#ifndef PII
#define PII pair <int,int>
#endif
typedef long long LL;

/* DSYEVR prototype */
extern "C" void dsyevr_( char* jobz, char* range, char* uplo, int* n, double* a,
                int* lda, double* vl, double* vu, int* il, int* iu, double* abstol,
                int* m, double* w, double* z, int* ldz, int* isuppz, double* work,
                int* lwork, int* iwork, int* liwork, int* info );

using namespace std;

#define NSELECT 100

int exists(vector<state>& v, state& p) {
    for(int i=sz(v)-1;i>=0;i--) {
        if(v[i].equals(p)) return i;
    }    
    return -1;
}

void generateBFS(GridWorld& game, vector<VI >& rel, map<PII,pair<int,float> >& M,
                 const int num_actions, const int budget) {
    state start = game.getState();
    queue<state> Q; 
    Q.push(start);
    vector<state> v;
    v.pb(start);
    int prev = 0;
    while(!Q.empty() && sz(rel) < budget) {
        state current = Q.front();
        game.loadState(current);
        REP(i,num_actions) {
            game.saveState();
            float reward = game.processAction(i);
            state next = game.getState();
            int present = exists(v,next);
            if(present == -1) {
                v.pb(next);
                Q.push(next);
                int index = sz(rel);
                rel[prev].pb(index);
                VI p;
                p.pb(prev);
                rel.pb(p);
                M[make_pair(prev,i)] = make_pair(index,reward);
            }
            else {
                if(find(all(rel[prev]),present) == rel[prev].end()) {
                    rel[prev].pb(present);
                    if(present != prev) rel[present].pb(prev);
                }
                M[make_pair(prev,i)] = make_pair(present,reward);
            }
            game.restoreState();
        }
        Q.pop();
        if(sz(rel) > prev + 1) ++prev;
    }
}

void learnTranslationOp(map<PII, pair<int,float> >& M, vector<vector<float> >& Dist,
                        vector<vector<float> >& Op) {
    map<PII, pair<int,float> >::iterator it;
    int start_state, action, end_state;
    map<int,int> Num_Actions;
    int num_dim = sz(Dist[0]);
    for(it = M.begin();it!=M.end();it++) {
        start_state = (it->first).first;
        action = (it->first).second;
        end_state = it->second.first;
        REP(k,num_dim) Op[action][k] += (Dist[end_state][k] - Dist[start_state][k]);
        Num_Actions[action]++;
    }
    REP(i,sz(Num_Actions)) REP(k,num_dim) Op[i][k] /= Num_Actions[i];
}

void computeD(vector<vector<float> >& D,
              map<PII,pair<int,float> >&M,
              vector<VI >& rel) {
    int nV = sz(rel);
    D.clear();
    D.resize(nV,vector<float>(nV,INF));

    // Compute all-pairs shortest path
    REP(i,nV) D[i][i] = 0;
    REP(i,nV) REP(j,sz(rel[i])) if(rel[i][j] != i) D[i][rel[i][j]] = 1;

    REP(k,nV) {
        REP(i,nV) {
            REP(j,nV) {
                D[i][j] = min(D[i][j],D[i][k]+D[k][j]);
            }
        }
    }

}

void getDistanceMatrix(GridWorld& game,
                       vector<vector<float> >& D,
                       map<PII,pair<int,float> >& M,
		               vector<VI >& rel,
		               const int num_actions,
                       const int budget) {
    rel.clear();
    VI empty;
    rel.pb(empty);
    generateBFS(game,rel,M,num_actions,budget);

    computeD(D,M,rel); 

}
