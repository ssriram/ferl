#ifndef __UCT__
#define __UCT__

#include <list>
#include <stack>
#include <algorithm>
#include <vector>
#include <utility>
#include <cassert>

#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define all(c) c.begin(),c.end()
#define INF (int) 1e9
#define EPS 1e-3
#define VI vector<int>
#ifndef PII
    #define PII pair<int,int>
#endif

using namespace std;

class Node;

typedef map<int,pair<Node *,float> > Nodes;

class Node {
    private:
    Node *parent;
    Nodes children;
    float uct_count;
    float uct_return;
    float sigma;
    vector<int> exploredActions;

    public:
    ALEState state;
    vector<float> coord; // manifold embedding

    Node(float uct_return, float uct_count, float sigma,
         vector<float>& embedding, ALEState state,Node *parent) 
    :coord(embedding) {
        this->uct_return = uct_return;
        this->uct_count = uct_count;
        this->sigma = sigma;
        this->parent = parent;
        this->state = state;
    }

    void addChild(Node *child, int action, float reward) {
        this->children[action] = make_pair(child,reward);
        this->exploredActions.pb(action);
    }

    void decaySigma(const float decay) {
        this->sigma = max((float)EPS, this->sigma * decay);
    }
    
    void update(float totalReward, const float gamma, stack<float>& rewards) {
        assert(sz(rewards) > 0);
        float reward = rewards.top() + gamma * totalReward;
        totalReward = reward;
        rewards.pop();
        this->uct_count += 1.0;
        this->uct_return += reward;
        if(this->parent) (this->parent)->update(totalReward,gamma,rewards);
    }

    float getDistance(vector<float>& v) {
        float dist = 0;
        int n = sz(v);
        REP(i,n) dist += (this->coord[i] - v[i]) * (this->coord[i] - v[i]);
        return sqrt(dist);
    }

    pair<float,float> getStats(vector<Node *>& v) {
        float final_count = 0;
        float final_return = 0;
        int n = sz(v);
        REP(i,n) {
            float distance = this->getDistance(v[i]->coord);
            float weight = exp(-distance / this->sigma);
            final_count += weight * v[i]->uct_count;
            final_return += weight * v[i]->uct_return;
        }
        return make_pair(final_count,final_return);
    }

    float getUCTQ(pair<float,float> p,float reward,float parent_count,
          const float uct_constant,const float gamma) {
        float count = p.first;
        float value = reward + gamma * (p.second / p.first);
        return value + uct_constant * sqrt(2*logf(parent_count)/ count);
    }

    pair<Node *, int> getBestAction(vector<Node *>& v,const float uct_constant,
                      const float gamma, const float decay) {
        float bestQ = -INF;
        int bestAction = -1;
        Node *bestNode = NULL;
        vector<float> Q(sz(children));
        vector<Node *> N(sz(children));
        vector<pair<float,float> > C(sz(children));
        float parent_count = 0;
        // Add child counts to get parent count
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            C[it->first] = (it->second).first->getStats(v);
            parent_count += C[it->first].first;
        }
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            float qValue = (it->second).first->getUCTQ(C[it->first],
                           it->second.second, parent_count,
                           uct_constant, gamma);
            Q[it->first] = qValue;
            N[it->first] = it->second.first;
            if(bestQ < qValue) {
                bestQ = qValue;
                bestAction = it->first,
                bestNode = it->second.first;
            }
            it->second.first->decaySigma(decay);
        }
        // break ties randomly
        VI same;
        REP(i,sz(Q)) if(abs(bestQ - Q[i]) < EPS) same.pb(i);
        if(sz(same) > 1) {
            bestAction = same[rand()%sz(same)];
            bestNode = N[bestAction];
        }

        assert(bestAction != -1 && bestNode != NULL);
        return make_pair(bestNode, bestAction);
    }

    int getRandomAction(int num_actions) {
        vector<int> unexplored;
        for(int i=0;i<num_actions;i++)
            if(find(all(exploredActions),i) == exploredActions.end()) unexplored.pb(i);

        return unexplored[rand() % sz(unexplored)];
    }

    int getGreedyAction(const float gamma) {
        float bestQ = -INF;
        float bestCount = -INF;
        int bestAction = -1;
        // pick bestQ, break ties using visit counts
        vector<float> Q,C;
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            float count = (it->second.first)->uct_count;
            float value = (it->second.first)->uct_return / count;
            float qValue = it->second.second + gamma * value;
            Q.pb(qValue);
            C.pb(count);
            cout<<it->first<<" "<<qValue<<" "<<count<<endl;
            if(qValue - bestQ > EPS) bestQ = qValue, bestAction = it->first, bestCount = count;
           else if(qValue > 0 && abs(bestQ - qValue) < EPS && bestCount < count) bestQ = qValue, bestAction = it->first, bestCount = count;
        }
            
        // If there are still ties, break randomly
        VI same;
        REP(i,sz(Q)) {
            if(abs(Q[i] - bestQ) < EPS) {
                if(C[i] == bestCount) same.pb(i);
            }
        }
        if(sz(same) > 1) bestAction = same[rand() % sz(same)];
                     
        assert(bestAction != -1);
        return bestAction;
    }    

    pair<Node *,int> treePolicy(ALEInterface& ale, 
                     map<PII,pair<int,float> >& M,vector<ALEState>& cached,
                     vector<Node *>& v,stack<float>& rewards, 
                     const float uct_constant,const float gamma,
                     const float decay, int num_actions,
                     int& idx, float& oneStepReward, bool limitReached) {
        if(sz(this->exploredActions) == num_actions) {
            pair<Node *,int> bestChild = this->getBestAction(v,uct_constant,gamma,decay);
            pair<int,float> p = M[make_pair(idx,bestChild.second)];
            idx = p.first;
            float reward = p.second;
            rewards.push(reward);
            //cout<<bestChild.second<<" -> ";
            return (bestChild.first)->treePolicy(ale,M,cached,v,rewards,uct_constant, 
                                      gamma,decay,num_actions,idx,oneStepReward,
                                      limitReached);
        }
        else {
            pair<Node *,int> randomChild;
            if(!limitReached) {
                randomChild = make_pair(this,this->getRandomAction(num_actions));
                //cout<<randomChild.second<<endl;
                pair<int,int> next = make_pair(idx,randomChild.second);
                if(M.find(next) == M.end()) {
                    // save previous state
                    ale.saveState();
                    OSystem* sys = (ale.theOSystem).get();
                    RomSettings* rom = (ale.settings.get());
                    string md5 = sys->console().properties().get(Cartridge_MD5);
                    ActionVect actions = ale.getMinimalActionSet();
                    // load this state, take the action
                    ALEState state = (ale.environment)->getState();
                    state.load(sys,rom,md5,this->state);
                    float reward = 0;
                    REP(i,5) reward += ale.act(actions[randomChild.second]);
                    int token = sz(cached);
                    M[next] = make_pair(token,reward);
                    oneStepReward = reward;
                    ALEState new_state = state.save(sys,rom,md5);
                    cached.pb(new_state);
                    oneStepReward = reward;
                    rewards.push(reward);
                    // load back saved state
                    ale.loadState();
                    idx = -1; 
                }
                else {
                    pair<int,float> p = M[next];
                    idx = p.first;
                    oneStepReward = p.second;
                    rewards.push(p.second);
                }
            }
            else {
                //cout<<endl;
                randomChild = make_pair(this,-1);
            }
            return randomChild;
        }
    }

    void setEmbedding(map<PII,pair<int,float> >& M,
         vector<vector<float> >& embedding, vector<vector<float> >& Operator,
         vector<ALEState>& cached, vector<Node *>& v, int idx) {
        this->coord = embedding[idx];
        v.pb(this);
        for(Nodes::iterator it=children.begin();it != children.end();it++) {
            Node *child = it->second.first;
            int action = it->first;
            int next = -1;

            if(M.find(make_pair(idx,action)) == M.end()) {
                next = sz(embedding);
                M[make_pair(idx,action)] = make_pair(next,it->second.second);
                child->coord = embedding[idx];
                REP(i,sz(Operator[action])) child->coord[i] += Operator[action][i];
                embedding.pb(child->coord);
                cached.pb(child->state);
            }
            else next = M[make_pair(idx,action)].first;

            child->setEmbedding(M,embedding,Operator,cached,v,next);
        }
    } 

    Node *retainOptimalBranch(int branch) {
        Node *new_root = children[branch].first;
        new_root->parent = NULL;
        children.erase(branch);
        return new_root;
    }

    ~Node() {
        for (Nodes::iterator iter=children.begin(); iter!=children.end(); iter++) {
            Node *child = iter->second.first;
            delete child;
        }
    }
    
};

#endif
