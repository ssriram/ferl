/************************************************************************************
Author - Sriram Srinivasan
Purpose - Learn a local manifold and translation operator from Atari Game States
************************************************************************************/

#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <cstdio>

#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#ifndef PII
#define PII pair <int,int>
#endif
typedef long long LL;

/* DSYEVR prototype */
extern "C" void dsyevr_( char* jobz, char* range, char* uplo, int* n, double* a,
                int* lda, double* vl, double* vu, int* il, int* iu, double* abstol,
                int* m, double* w, double* z, int* ldz, int* isuppz, double* work,
                int* lwork, int* iwork, int* liwork, int* info );

using namespace std;

#define NSELECT 100

int exists(vector<ALEState>& v, ALEState& p) {
    for(int i=sz(v)-1;i>=0;i--) {
        if(v[i].equals(p)) return i;
    }    
    return -1;
}

void generateBFS(ALEInterface& ale, vector<VI >& rel, map<PII,pair<int,float> >& M,
                 vector<ALEState>& v, const int num_actions, const int budget) {
    ActionVect actions = ale.getMinimalActionSet();
    OSystem* sys = (ale.theOSystem).get();
    RomSettings* rom = (ale.settings.get());
    string md5 = sys->console().properties().get(Cartridge_MD5);
    ALEState state = (ale.environment)->getState();
    ALEState start = state.save(sys,rom,md5);
    queue<ALEState> Q; 
    Q.push(start);
    v.pb(start);
    int prev = 0;
    while(!Q.empty() && sz(rel) < budget) {
        ALEState current = Q.front();
        state = (ale.environment)->getState();
        state.load(sys,rom,md5,current);
        REP(i,num_actions) {
            ale.saveState();
            float reward = 0;
            REP(j,5) reward += ale.act(actions[i]);
            state = (ale.environment)->getState();
            ALEState next = state.save(sys,rom,md5);
            int present = exists(v,next);
            if(present == -1) {
                v.pb(next);
                Q.push(next);
                int index = sz(rel);
                rel[prev].pb(index);
                VI p;
                p.pb(prev);
                rel.pb(p);
                M[make_pair(prev,i)] = make_pair(index,reward);
            }
            else {
                if(find(all(rel[prev]),present) == rel[prev].end()) {
                    rel[prev].pb(present);
                    if(present != prev) rel[present].pb(prev);
                }
                M[make_pair(prev,i)] = make_pair(present,reward);
            }
            ale.loadState();
        }
        Q.pop();
        if(sz(rel) > prev + 1) ++prev;
    }
}

void expandBFS(ALEInterface& ale, 
     vector<VI >& oldRel, vector<VI >& newRel,
     vector<ALEState>& oldCache, vector<ALEState>& newCache,
     map<PII,pair<int,float> >& oldM, map<PII,pair<int,float> >& newM,
     int branch, const int num_actions,
     const int budget) {

    // ALE Stuff    
    ActionVect actions = ale.getMinimalActionSet();
    OSystem* sys = (ale.theOSystem).get();
    RomSettings* rom = (ale.settings.get());
    string md5 = sys->console().properties().get(Cartridge_MD5);
    ALEState state = (ale.environment)->getState();

    int start = oldM[make_pair(0,branch)].first;    
    queue<int> Q;
    Q.push(start);
    newCache.pb(oldCache[start]);
    map<int,int> stateMap;
    stateMap[start] = 0;
    int idx = 0;
    VI empty;
    newRel.pb(empty);
    while(!Q.empty() && sz(newCache) < budget) {
        int current = Q.front();
        int first = stateMap[current];
        REP(i,num_actions) {
            PII p = make_pair(current,i);
            map<PII,pair<int,float> >::iterator it = oldM.find(p);
            if(it == oldM.end()) {
                state.load(sys,rom,md5,oldCache[current]);
                float reward = 0;
                REP(j,5) reward += ale.act(actions[i]);   
                state = (ale.environment)->getState();
                ALEState next = state.save(sys,rom,md5);
                int present = exists(newCache,next);
                if(present == -1) {
                    newM[make_pair(first,i)] = make_pair(idx+1,reward);
                    oldM[make_pair(current,i)] = make_pair(sz(oldCache),reward);
                    stateMap[sz(oldCache)] = idx+1;
                    newCache.pb(next);
                    Q.push(sz(oldCache));
                    oldCache.pb(next); 
                    newRel[first].pb(idx+1);
                    VI v;
                    v.pb(first);
                    newRel.pb(v);
                    ++idx;
                }
                else {
                    newM[make_pair(first,i)] = make_pair(present,reward);
                    if(find(all(newRel[first]),present) == newRel[first].end()) {
                        newRel[first].pb(present);
                        if(present != first) newRel[present].pb(first);
                    } 
                }
            }
            else {
                if(stateMap.find((it->second).first) == stateMap.end()) {
                    newM[make_pair(first,i)] = make_pair(idx+1,(it->second).second);
                    newCache.pb(oldCache[(it->second).first]);
                    stateMap[(it->second).first] = idx + 1;
                    Q.push((it->second).first);
                    newRel[first].pb(idx+1);
                    VI v;
                    v.pb(first);
                    newRel.pb(v);
                    ++idx;
                }
                else {
                    int present = stateMap[(it->second).first];
                    newM[make_pair(first,i)] = make_pair(present,
                                             (it->second).second);
                    if(find(all(newRel[first]),present) == newRel[first].end()) {
                        newRel[first].pb(present);
                        if(present != idx) newRel[present].pb(first); 
                    }
                }
            }
        }  
        Q.pop();
    }

    assert(sz(newCache) == sz(newRel));
}
     
void learnTranslationOp(map<PII, pair<int,float> >& M, vector<vector<float> >& Dist,
                        vector<vector<float> >& Op) {
    map<PII, pair<int,float> >::iterator it;
    int start_state, action, end_state;
    map<int,int> Num_Actions;
    int num_dim = sz(Dist[0]);
    for(it = M.begin();it!=M.end();it++) {
        start_state = (it->first).first;
        action = (it->first).second;
        end_state = it->second.first;
        REP(k,num_dim) Op[action][k] += (Dist[end_state][k] - Dist[start_state][k]);
        Num_Actions[action]++;
    }
    REP(i,sz(Num_Actions)) REP(k,num_dim) Op[i][k] /= Num_Actions[i];
}

void getEmbedding(map<PII,pair<int,float> >&M,
                  vector<vector<float> >& embedding,
                  vector<VI >& rel) {
    int nV = sz(rel);

    // Compute all-pairs shortest path
    float D[nV][nV];
    REP(i,nV) REP(j,nV) D[i][j] = INF, D[i][i] = 0;
    REP(i,nV) REP(j,sz(rel[i])) if(rel[i][j] != i) D[i][rel[i][j]] = 1;

    REP(k,nV) {
        REP(i,nV) {
            REP(j,nV) {
                D[i][j] = min(D[i][j],D[i][k]+D[k][j]);
            }
        }
    }

    // Apply Classical MDS on D^2
    REP(i,nV) REP(j,nV) D[i][j] = D[i][j]*D[i][j];
    
    // Center D^2 matrix
    float rowSum[nV], colSum[nV], totSum = 0;
    REP(i,nV) {
        rowSum[i] = 0;colSum[i] = 0;
        REP(j,nV) rowSum[i] += D[i][j], colSum[i] += D[j][i], totSum += D[i][j];
    }
    REP(i,nV) REP(j,nV) D[i][j] = -(D[i][j] - rowSum[i]/nV - colSum[j]/nV + totSum/(nV*nV))/2;

    /* Store 2D matrix as 1D array */
    double a[nV*nV];
    REP(i,nV) REP(j,nV) a[i*nV+j] = (double) D[i][j];

    // Get top 100 eigen vectors
    int n = nV, il, iu, m, lda = nV, ldz = nV, info, lwork, liwork;
    int LDZ = n;
    double abstol, vl, vu;
    int iwkopt;
    int* iwork;
    double wkopt;
    double* work;
    /* Local arrays */
    int isuppz[n];
    double w[n], z[LDZ*NSELECT];
    /* Negative abstol means using the default value */
    abstol = -1.0;
    /* Set il, iu to compute NSELECT largest eigenvalues */
    il = max(1, n -NSELECT + 1);
    iu = n;

    /* Query and allocate the optimal workspace */
    lwork = -1;
    liwork = -1;
    dsyevr_( "Vectors", "Indices", "Upper", &n, a, &lda, &vl, &vu, &il, &iu,
                    &abstol, &m, w, z, &ldz, isuppz, &wkopt, &lwork, &iwkopt, &liwork,
                    &info );
    lwork = (int)wkopt;
    work = (double*)malloc( lwork*sizeof(double) );
    liwork = iwkopt;
    iwork = (int*)malloc( liwork*sizeof(int) );
    /* Solve eigenproblem */
    dsyevr_( "Vectors", "Indices", "Upper", &n, a, &lda, &vl, &vu, &il, &iu,
                    &abstol, &m, w, z, &ldz, isuppz, work, &lwork, iwork, &liwork,
                    &info );
    free(work);
    free(iwork);
    /* Check for convergence */
    if( info > 0 ) {
        printf( "The algorithm failed to compute eigenvalues.\n" );
        exit( 1 );
    }

    // Pick only the top eigen vectors
    VI picked;
    double topEigenValue = w[m-1];
    picked.pb(m-1);
    for(int i=m-2;i>=0;i--) {
        if(w[i] < 0.2 * topEigenValue) {
            //picked.pb(i);
            break;
        }
        else picked.pb(i);
    }

    int topK = sz(picked);
    //cout<<"Reduced Dimensionality = "<<topK<<endl;
    embedding.resize(nV,vector<float>(topK));
    REP(i,nV) {
        REP(j,topK) embedding[i][j] = z[i+n*picked[j]] * sqrt(w[picked[j]]);
    }
}

vector<vector<float> > learnManifold(ALEInterface& ale,
                       vector<vector<float> >& embedding,
                       map<PII,pair<int,float> >& M,
                       vector<ALEState>& cached, 
		               vector<VI >& rel,
		               const int num_actions,
                       const int budget) {
    VI empty;
    rel.pb(empty);
    generateBFS(ale,rel,M,cached,num_actions,budget);

    getEmbedding(M,embedding,rel); 

    /* Learn a translation operator */
    int topK = sz(embedding[0]);
    vector<vector<float> > translation(num_actions,vector<float>(topK,0));
    learnTranslationOp(M,embedding,translation);
    return translation;

}

vector<vector<float> > updateManifold(ALEInterface& ale,
     vector<vector<float> >& embedding,
     map<PII,pair<int,float> >& oldM, map<PII,pair<int,float> >& newM,
     vector<ALEState>& oldCache, vector<ALEState>& newCache,
     vector<VI >& oldRel, vector<VI >& newRel,
     int branch, const int num_actions, const int budget) {
    
    newM.clear();
    newCache.clear();
    embedding.clear();
    newRel.clear();
    expandBFS(ale,oldRel,newRel,oldCache,newCache,oldM,newM,
              branch,num_actions,budget);
    
    getEmbedding(newM,embedding,newRel);

    /* Learn a translation operator */
    int topK = sz(embedding[0]);
    vector<vector<float> > translation(num_actions,vector<float>(topK,0));
    learnTranslationOp(newM,embedding,translation);
    return translation;
}
