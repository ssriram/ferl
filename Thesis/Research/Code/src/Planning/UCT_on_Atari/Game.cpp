/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Running UCT with nearest neighbor on manifold from Grid World
*******************************************************************************/
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include <ale_interface.hpp>
#include "UCT.h"
#include "GetManifoldFeatures.hpp"

typedef map<PII,pair<int,float> > Trans;

using namespace std;

map<string, float> parseParams(const char *param_file) {
    map<string,float> M;
    ifstream file (param_file);
    assert(file.is_open());
    string temp, key;
    float val;
    while(!file.eof()) {
        getline(file,temp,'\n');
        std::istringstream iss(temp);
        iss >> key;
        iss >> val;
        M[key] = val;
    }
    return M;
}

void validateParams(map<string, float>& M) {
    const char *keys[] = {"mode", "uct_constant", "num_sim",
                          "sigma_decay", "gamma", "num_episodes",
                          "node_limit", "budget", "depth"};
    vector<string> v(keys,keys+9);
    map<string, float>::iterator it;
    REP(i,sz(v)) assert(M.find(v[i]) != M.end());
}

void displayParams(map<string,float>& M) {
    map<string, float>::iterator it;
    for(it=M.begin();it!=M.end();it++) cout<<it->first<<" "<<it->second<<endl;
}

void getNextManifoldState(vector<float>& curr, vector<float>& action) {
    REP(i,sz(curr)) curr[i] += action[i];
}

float defaultPolicy(ALEInterface& ale, ActionVect& actions,
      const int num_actions,int depth,const float gamma) {
    if(depth == 0) return 1e-5;
    else if(ale.game_over()) return 0;
    else {
        float reward = 0;
        int action = rand() % num_actions;
        REP(i,5) reward += ale.act(actions[action]);
        return reward + gamma * defaultPolicy(ale,actions,
               num_actions,depth-1,gamma);
    }    
}

pair<Node *,int> runUCT(map<string,float>& Params, ALEInterface& ale, Trans& M,
    vector<vector<float> >& embedding, vector<ALEState>& cached, 
    vector<vector<float> >& Operator, vector<Node *>& v, Node *start) {
    ActionVect actions = ale.getMinimalActionSet();

    const int num_actions = sz(actions);
    float sigma = Params.find("sigma") == Params.end() ? 1.0 : Params["sigma"];
    bool insert_root = start == NULL;
    start = start == NULL ? (Node *) new Node((float)0,(float)0,sigma,
            embedding[0],cached[0],NULL) : start;
    int numSim = (int) Params["num_sim"];
    int depth = (int) Params["depth"];
    const float uct_constant = Params["uct_constant"];
    const float decay = Params["sigma_decay"];
    const float gamma = Params["gamma"];
    const int nodeLimit = (int) Params["node_limit"];

    // ALE Stuff
    OSystem* sys = (ale.theOSystem).get();
    RomSettings* rom = (ale.settings.get());
    string md5 = sys->console().properties().get(Cartridge_MD5);
    ALEState state = (ale.environment)->getState();

    if(insert_root) v.pb(start);
    vector<float> manifoldState;
    stack<float> rewards;
    int numNodes = sz(v);

    assert(sz(cached) == sz(embedding));

    REP(i,numSim) {
        Node *p = start;
        int step = 0;
        assert(sz(rewards) == 0);

        // Pick the node from which a roll-out should be done
        float oneStepReward = 0;
        pair<Node *,int> next = p->treePolicy(ale,M,cached,v,
                                rewards,uct_constant,gamma,
                                decay,num_actions,step,
                                oneStepReward,numNodes >= nodeLimit);
        start->decaySigma(decay);

        if(step != -1) manifoldState = embedding[step];
        else {
            manifoldState = next.first->coord;
            getNextManifoldState(manifoldState,Operator[next.second]);
            embedding.pb(manifoldState);
            step = sz(cached)-1;
        }
        assert(sz(cached) == sz(embedding));
        
        Node *child;
        if(numNodes < nodeLimit) {
            child = new Node((float)0,(float)0,sigma,
                        manifoldState,cached[step],next.first);
            (next.first)->addChild(child,next.second,oneStepReward);
            v.pb(child);
        }
        else {
            child = next.first;
        }
        // Do a roll-out from this node
        state.load(sys,rom,md5,child->state);
        float rollout_reward = defaultPolicy(ale,actions,num_actions,
              depth,gamma);
        rewards.push(rollout_reward);
        // Update nodes
        child->update(0,gamma,rewards);
    }
    int greedyAction = start->getGreedyAction(gamma);
    Node *new_root = start->retainOptimalBranch(greedyAction);
    delete start;
    return make_pair(new_root,greedyAction);
}

vector<vector<float> > getManifold(ALEInterface& ale,
                       vector<vector<float> >& embedding,
                       Trans& currM, Trans& nextM,
                       vector<ALEState>& currCache,
                       vector<ALEState>& nextCache,
                       vector<VI >& currRel,vector<VI >& nextRel,
                       vector<Node *>& v,Node *root,int action,
                       const int num_actions,const int budget) {

    vector<vector<float> > Operator = updateManifold(ale,embedding,currM,nextM,
    currCache, nextCache,currRel,nextRel,action,num_actions,budget);

    v.clear();
    root->setEmbedding(nextM,embedding,Operator,nextCache,v,0);
    
    return Operator;
}

int main(int argc, char **argv) {
    if(argc < 3) {
        std::cerr << "Usage: " << argv[0] << " param_file rom_file"<<std::endl;
        return 1;
    }

    map<string,float> Params = parseParams(argv[1]);
    validateParams(Params);
    displayParams(Params);

    const int display = Params.find("display") == Params.end() ? 0 : 
                        (int) Params["display"];
    ALEInterface ale(display);
    const string mode = (int) Params["mode"] ? "A" : "B";
    string romFile(argv[2]);
    ale.loadROM(romFile,mode);

    ActionVect actions = ale.getMinimalActionSet();
    const int num_actions = sz(actions);
    int numEpisodes = (int) Params["num_episodes"];

    int seed = time(NULL);
    cout<<"seed = "<<seed<<endl;
    srand(seed); // seed rand num generator
    /* Data Structures */
    vector<vector<float> > embedding;
    vector<vector<float> > Operator;
    vector<ALEState> oldCache, newCache;
    vector<VI > oldRel, newRel;
    Trans oldM, newM;
    vector<Node *> v;

    /* Current & Next Refs */
    vector<ALEState>& currCache = oldCache;
    vector<ALEState>& nextCache = newCache;
    vector<VI >& currRel = oldRel;
    vector<VI >& nextRel = newRel;
    Trans& currM = oldM;
    Trans& nextM = newM;

    //REP(i,100) ale.act(actions[0]);

    REP(i,numEpisodes) {
        float totalReward = 0;
        Node *root = NULL;
        int action = -1;
        ale.saveState();

        /* Clear all data structs */
        currM.clear();nextM.clear();
        currCache.clear();nextCache.clear();
        currRel.clear();nextRel.clear();
        embedding.clear();
        v.clear();

        while(!ale.game_over() ) {
            ale.saveState();
            // Learn a manifold of the grid
            Operator = root == NULL ? 
                       learnManifold(ale,embedding,nextM,nextCache,
                       nextRel,num_actions,Params["budget"]) : 
                       getManifold(ale,embedding,currM,nextM,currCache,
                       nextCache,currRel,nextRel,v,root,action,num_actions,
                       Params["budget"]);
            printf("Manifold learnt for %d states with %d dimensions\n"
            ,sz(embedding),sz(embedding[0]));

            cout<<"Operator learnt\n";
            REP(i,sz(Operator)) {
                REP(j,sz(Operator[0])) cout<<Operator[i][j]<<" ";
                cout<<endl;
            }

            ale.loadState();

            ale.saveState();
            pair<Node *,int> p = runUCT(Params,ale,nextM,embedding,
                                 nextCache,Operator,v,root);
            root = p.first, action = p.second;
            ale.loadState();

            float reward = 0;
            REP(i,5) reward += ale.act(actions[action]);
            cout<<"action = "<<action<<", reward = "<<reward<<endl;
            totalReward += reward;
            
            if(!ale.game_over()) {
                swap(currM,nextM);
                swap(currCache,nextCache);
                swap(currRel,nextRel);
            }
        }
        delete root;
        printf("Episode %d ended with total reward = %f\n",i,totalReward);
        ale.loadState();
    }
}
