#!/bin/bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ssriram/ferl/Thesis/Research/Code/lib/ale_0.4.1/ale_0_4

echo "Starting run at: `date`"
outDir="/home/ssriram/ferl/Thesis/Research/Output/Experiments/Runs/Planning/Vanilla-UCT_on_Atari/~~ARG1~~";
confDir="/home/ssriram/ferl/Thesis/Research/Code/conf/Planning/Vanilla-UCT_on_Atari/~~ARG1~~";
romDir="/home/ssriram/roms";

/home/ssriram/ferl/Thesis/Research/Code/src/Planning/Vanilla-UCT_on_Atari/uct "$confDir/Params~~run~~.cfg" "$romDir/~~ARG1~~.bin" 1>"$outDir/run~~run~~"
echo "Job finished with exit code $? at: `date`"

