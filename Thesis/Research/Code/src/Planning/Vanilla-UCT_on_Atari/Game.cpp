/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Running UCT with nearest neighbor on manifold from Grid World
*******************************************************************************/
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include <ale_interface.hpp>
#include "UCT.h"

typedef map<PII,pair<int,float> > Trans;

using namespace std;

map<string, float> parseParams(const char *param_file) {
    map<string,float> M;
    ifstream file (param_file);
    assert(file.is_open());
    string temp, key;
    float val;
    while(!file.eof()) {
        getline(file,temp,'\n');
        std::istringstream iss(temp);
        iss >> key;
        iss >> val;
        M[key] = val;
    }
    return M;
}

void validateParams(map<string, float>& M) {
    const char *keys[] = {"mode", "uct_constant", "num_sim",
                          "gamma", "num_episodes", "depth",
                          "node_limit"};
    vector<string> v(keys,keys+7);
    map<string, float>::iterator it;
    REP(i,sz(v)) assert(M.find(v[i]) != M.end());
}

void displayParams(map<string,float>& M) {
    map<string, float>::iterator it;
    for(it=M.begin();it!=M.end();it++) cout<<it->first<<" "<<it->second<<endl;
}

float defaultPolicy(ALEInterface& ale, ActionVect& actions,
      const int num_actions,int depth,const float gamma) {
    if(depth == 0) return 1e-5;
    else if(ale.game_over()) return 0;
    else {
        float reward = 0;
        int action = rand() % num_actions;
        REP(i,5) reward += ale.act(actions[action]);
        return reward + gamma * defaultPolicy(ale,actions,
               num_actions,depth-1,gamma);
    }    
}

pair<Node *,int> runUCT(map<string,float>& Params, ALEInterface& ale,
                        Node *start) {
    //cout<<"Running Simulations\n";
    ActionVect actions = ale.getMinimalActionSet();
    const int num_actions = sz(actions);

    OSystem* sys = (ale.theOSystem).get();
    RomSettings* rom = (ale.settings.get());
    string md5 = sys->console().properties().get(Cartridge_MD5);
    ALEState state = (ale.environment)->getState();
    ALEState new_state;

    if(start == NULL) new_state = state.save(sys,rom,md5);
    start = start == NULL ? (Node *) new Node((float)0,(float)0,
                     new_state,NULL) : start;
    int numSim = (int) Params["num_sim"];
    int depth = (int) Params["depth"];
    const float uct_constant = Params["uct_constant"];
    const float gamma = Params["gamma"];
    const int nodeLimit = (int) Params["node_limit"];

    stack<float> rewards;
    int numNodes = start->countNodes() + 1;

    REP(i,numSim) {
        Node *p = start;
        assert(sz(rewards) == 0);

        // Pick the node from which a roll-out should be done
        float oneStepReward = 0;
        pair<Node *,int> next = p->treePolicy(ale,actions,new_state,
                                rewards,uct_constant,gamma,
                                num_actions,oneStepReward,numNodes >= nodeLimit);

        Node *child;
        if(numNodes < nodeLimit) {
            child = new Node((float)0,(float)0,new_state,next.first);
            (next.first)->addChild(child,next.second,oneStepReward);
            ++numNodes;
        }
        else {
            child = next.first;
        }
        // Do a roll-out from this node
        state.load(sys,rom,md5,child->state);    
        float rollout_reward = defaultPolicy(ale,actions,
              num_actions,depth,gamma);
        rewards.push(rollout_reward);
        // Update nodes
        child->update(0,gamma,rewards);
    }
    int greedyAction = start->getGreedyAction(gamma);
    Node *new_root = start->retainOptimalBranch(greedyAction);
    delete start;
    return make_pair(new_root,greedyAction);
}

int main(int argc, char **argv) {
    if(argc < 3) {
        std::cerr << "Usage: " << argv[0] << " param_file rom_file"<<std::endl;
        return 1;
    }

    map<string,float> Params = parseParams(argv[1]);
    validateParams(Params);
    displayParams(Params);

    const int display = Params.find("display") == Params.end() ? 0 : 
                        (int) Params["display"];
    ALEInterface ale(display);
    const string mode = (int) Params["mode"] ? "A" : "B";
    ale.loadROM(argv[2],mode);

    ActionVect actions = ale.getMinimalActionSet();
    int numEpisodes = (int) Params["num_episodes"];

    int seed = time(NULL);
    cout<<"seed = "<<seed<<endl; 
    srand(seed); // seed rand num generator

    REP(i,numEpisodes) {
        float totalReward = 0;
        Node *root = NULL;
        int action = -1;
        ale.saveState();

        while(!ale.game_over()) {

            ale.saveState(); 
            pair<Node *,int> p = runUCT(Params,ale,root);
            root = p.first, action = p.second;
            ale.loadState();

            float reward = 0;
            REP(i,5) reward += ale.act(actions[action]);
            cout<<"action = "<<action<<", reward = "<<reward<<endl;
            totalReward += reward;
        }
        delete root;
        printf("Episode %d ended with total reward = %f\n",i,totalReward);
        ale.loadState();
    }
}
