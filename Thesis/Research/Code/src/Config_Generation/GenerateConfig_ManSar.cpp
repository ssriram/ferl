
/*
Author : SRIRAM S
*/
// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cassert>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

using namespace std;

string getString(int n) {
    string s = "";
    while(n > 0) s.pb(n%10 + '0'), n /= 10;
    reverse(all(s));
    return s;
}

int main(int argc, char** argv) {
    assert(argc == 2);
    vector<pair<string, float> > v;
    v.pb(make_pair("gamma", 0.99));
    v.pb(make_pair("epsilon", 1));
    v.pb(make_pair("alpha", 0.1));
    v.pb(make_pair("mode", 0));
    v.pb(make_pair("display", 0));
    v.pb(make_pair("num_steps", 5));
    v.pb(make_pair("num_runs", 10));
    v.pb(make_pair("num_tilings", 16));
    v.pb(make_pair("max_depth", 10));
    v.pb(make_pair("episode_length", 160));

    ofstream file;
    string configFile;
    int counter = 1;
    const int policy_length = 80;
    const int episode_length = 160;
    const int max_num_sarsa = 10;
    
    for(int i =1;i * policy_length <= episode_length;i *= 2) {
        for(int num_sarsa = 10; num_sarsa <= max_num_sarsa; num_sarsa *= 2) {
            configFile.assign(argv[1]);
            configFile += "/Params" + getString(counter) + ".cfg";
            file.open(configFile.c_str());
            
            REP(k,sz(v)) file<<v[k].first<<" "<<v[k].second<<endl;
            file<<"policy_length "<<i<<endl;
            file<<"num_sarsa "<<num_sarsa<<endl;
            file.close();

            ++counter;
        }
    }
    return 0;
}
