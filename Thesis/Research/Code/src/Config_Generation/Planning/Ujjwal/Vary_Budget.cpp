
/*
Author : SRIRAM S
*/
// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cassert>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

using namespace std;

vector<pair<string, float> > parseCommonParams(const char *param_file) {
    vector<pair<string, float> > v;
    ifstream file (param_file);
    assert(file.is_open());
    string temp, key;
    float val;
    while(!file.eof()) {
        getline(file,temp,'\n');
        std::istringstream iss(temp);
        iss >> key;
        iss >> val;
        v.pb(make_pair(key,val));
    }
    return v;
}

string getString(int n) {
    string s = "";
    while(n > 0) s.pb(n%10 + '0'), n /= 10;
    reverse(all(s));
    return s;
}

int main(int argc, char** argv) {
    assert(argc == 3);
    vector<pair<string, float> > v = parseCommonParams(argv[1]);
    float uct_constant[5] = {0.01,0.1,1,10,100};
    float budget[5] = {1000,2000,4000,8000,16000};

    ofstream file;
    string configFile;
    int counter = 1;
    REP(i,5) {
        for(int depth=20;depth<=100;depth+=40) {
            int num_sim = budget[i] / depth;
            REP(j,5) {
                configFile.assign(argv[2]);
                configFile += "/Params" + getString(counter) + ".cfg";
                file.open(configFile.c_str());
                
                REP(l,sz(v)) file<<v[l].first<<" "<<v[l].second<<endl;
                file<<"uct_constant "<<uct_constant[j]<<endl;
                file<<"depth "<<depth<<endl;
                file<<"num_sim "<<num_sim<<endl;
                file.close();
                
                ++counter;
            }
        }    
    }
    return 0;
}
