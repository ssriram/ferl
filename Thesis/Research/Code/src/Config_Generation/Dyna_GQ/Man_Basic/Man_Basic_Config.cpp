
/*
Author : SRIRAM S
*/
// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cassert>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

using namespace std;

string getString(int n) {
    string s = "";
    while(n > 0) s.pb(n%10 + '0'), n /= 10;
    reverse(all(s));
    return s;
}

int main(int argc, char** argv) {
    assert(argc == 2);
    vector<pair<string, float> > v;
    v.pb(make_pair("gamma", 0.99));
    v.pb(make_pair("epsilon", 1));
    v.pb(make_pair("alpha", 0.1));
    v.pb(make_pair("mode", 0));
    v.pb(make_pair("display", 0));
    v.pb(make_pair("num_steps", 5));
    v.pb(make_pair("num_colors", 8)); 
    v.pb(make_pair("num_tilings", 1024));
    v.pb(make_pair("max_states", 500));

    ofstream file;
    string configFile;
    int counter = 1;
    const int max_blocks = 64;
    const int total_decisions = 5000;
    
    for(int i = 8;i <= max_blocks;i *= 4) {
        for(int j = 8; j <= max_blocks; j *= 4) {
            for(int episode_length = 50;episode_length <=200;episode_length += 50) {
                for(int policy_length = 0.1*episode_length;policy_length <= episode_length;policy_length *= 3) {
                    int num_sarsa = total_decisions / episode_length;
                    configFile.assign(argv[1]);
                    configFile += "/Params" + getString(counter) + ".cfg";
                    file.open(configFile.c_str());
                    
                    REP(k,sz(v)) file<<v[k].first<<" "<<v[k].second<<endl;
                    file<<"num_rows "<<i<<endl;
                    file<<"num_cols "<<j<<endl;
                    file<<"num_sarsa "<<num_sarsa<<endl;
                    file<<"episode_length "<<episode_length<<endl;
                    file<<"policy_length "<<policy_length<<endl;
                    file.close();

                    ++counter;
                }
            }
        }
    }
    return 0;
}
