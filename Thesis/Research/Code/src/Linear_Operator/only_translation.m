function [b,loss] = only_translation(Xtrain,Ytrain)

num_actions = size(Ytrain,1) / size(Xtrain,1);
d = size(Xtrain,2);

cvx_begin quiet
variable b(num_actions,d);
loss = L2Loss(Xtrain,Ytrain,b);
minimize (loss)
cvx_end
fprintf('Training loss = %f\n',loss);

end

function loss = L2Loss(X,Y,b)
actions = size(Y,1) / size(X,1);
% X_t * w = X_{t+1}
loss = 0;
for i=1:size(X,1)
    for k=1:actions
        cur_loss = norm(X(i,:) + b(k,:) - Y((i-1)*actions + k,:));
        loss = loss + cur_loss;
    end
end
loss = loss / size(X,1);
end