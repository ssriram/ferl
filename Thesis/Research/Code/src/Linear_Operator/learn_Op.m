function [w,b,loss] = learn_Op(Xtrain,Ytrain)

num_actions = size(Ytrain,1) / size(Xtrain,1);
d = size(Xtrain,2);

weights = zeros(num_actions*d, d);

cvx_begin quiet
variable w(num_actions*d, d);
variable b(num_actions,d);
loss = L2Loss(Xtrain,Ytrain,w,b);
minimize (loss)
subject to 
cvx_end
fprintf('Training loss = %f\n',loss);

end

function loss = L2Loss(X,Y,w,b)
d = size(X,2);
actions = size(Y,1) / size(X,1);
% X_t * w = X_{t+1}
loss = 0;
for i=1:size(X,1)
    for k=1:actions
        cur_loss = norm(X(i,:) * w((k-1)*d + 1 : k*d, :) + b(k,:) - Y(i*k,:));
        loss = loss + cur_loss;
    end
end
loss = loss / size(X,1);
end
