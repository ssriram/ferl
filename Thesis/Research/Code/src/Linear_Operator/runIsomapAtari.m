function [w,b,test_loss,norm_depth] = runIsomapAtari(distFileName, transFileName,depthFileName, outDir)
D = importdata(distFileName);

[Y, R] = IsomapII(D,'k',4)
embedding = Y.coords{2}';
embedding(1,:)

% Load depth file
state_depth = importdata(depthFileName);

% Load transition graph
G = importdata(transFileName);
training_depth = 30;
train_start_index = find(state_depth(:,2) == training_depth, 1,'first');
train_end_index = find(state_depth(:,2) == training_depth, 1,'last');
next_state_index = find(G(:,1)<=train_end_index);
cur_state = [];
next_state = [];
prev = -1;
for i=1:size(next_state_index,1) 
   start_state = G(i,1);
   end_state = G(i,3);
   if(start_state ~= prev)
       cur_state = [cur_state;embedding(start_state+1,:)];
       prev = start_state;
   end
   next_state = [next_state; embedding(end_state+1,:)];
end

[w,b,loss] = learn_stable_Op(cur_state,next_state);
dlmwrite(strcat(outDir,'/weights.txt'),w,'delimiter',' ','precision',6);
dlmwrite(strcat(outDir,'/b.txt'),b,'delimiter',' ','precision',6);

% Compute test loss by depth
new_embedding = zeros(size(embedding,1),size(embedding,2));
new_embeddding(1:train_end_index+1,:) = embedding(1:train_end_index+1,:);
test_state_index = find(G(:,1)>=train_start_index);
d = size(embedding,2);
test_loss = zeros(max(state_depth(:,2)),1);
num_states = zeros(max(state_depth(:,2)),1);
norm_depth = zeros(max(state_depth(:,2)),1);

for i=1:size(test_state_index)
    idx = test_state_index(i,1);
    start_state = G(idx,1);
    action = G(idx,2);
    end_state = G(idx,3);
    depth = state_depth(end_state+1,2);
    prediction = embedding(start_state+1,:) * w(action*d +1:(action+1)*d,:) + b(action+1,:);
    new_embedding(end_state+1,:) = prediction;
    test_loss(depth,1) = test_loss(depth,1) + norm(prediction - embedding(end_state+1,:));
    num_states(depth,1) = num_states(depth,1) + 1;
    norm_depth(depth,1) = norm_depth(depth,1) + norm(embedding(end_state+1,:));
    prediction;
    embedding(end_state+1,:);
    %fprintf('\n');
end


for i=1:size(test_loss,1)
    if(num_states(i,1) > 0) 
        test_loss(i,1) = test_loss(i,1) / num_states(i,1);
        norm_depth(i,1) = norm_depth(i,1) / num_states(i,1);
    end
end

test_loss = test_loss(training_depth+1:end,:);
norm_depth = norm_depth(training_depth+1:end,:);

end
