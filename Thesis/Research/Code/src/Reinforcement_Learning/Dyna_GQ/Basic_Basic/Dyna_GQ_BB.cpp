/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Dyna_GQ with Basic features as local and global features
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cassert>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>
#include "/home/ssriram/ferl/Thesis/Research/Code/src/Reinforcement_Learning/Sarsa/Dyna_GQ/Basic_Basic/Sarsa.cpp"
using namespace std;

map<string, float> parseParams(const char *param_file) {
    map<string,float> M;
    ifstream file (param_file);
    assert(file.is_open());  
    string temp, key;
    float val;
    while(!file.eof()) {
        getline(file,temp,'\n');
        std::istringstream iss(temp);
        iss >> key;
        iss >> val;
        M[key] = val; 
    }
    return M;
}

void validateParams(map<string, float>& M) {
    const char *keys[] = {"gamma", "alpha", "epsilon", "mode", "display", "num_sarsa",
                          "num_rows", "num_cols", "num_colors"};
    vector<string> v(keys,keys+9);
    map<string, float>::iterator it;
    REP(i,sz(v)) assert(M.find(v[i]) != M.end());
}

void displayParams(map<string,float>& M) {
    map<string, float>::iterator it;
    for(it=M.begin();it!=M.end();it++) cout<<it->first<<" "<<it->second<<endl;
}

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " rom_file param_file"<<std::endl;
        return 1;
    }

    map<string,float> Params = parseParams(argv[2]);
    validateParams(Params);
    displayParams(Params);
    
    const string mode = (int) Params["mode"] ? "A" : "B";
    const int display = (int) Params["display"];
    const int numStepsPerAction = Params.find("num_steps") == Params.end() ? 1 : (int) Params["num_steps"];
    ALEInterface ale(display);

    // Load the ROM file
    ale.loadROM(argv[1],mode);

    // Get the vector of minimal actions
    ActionVect actions = ale.getMinimalActionSet();

    const int numFeatures = Params["num_cols"] * Params["num_rows"] * Params["num_colors"] * 2;

    int runs = 5000;
    float cumReward = 0;
    REP(episode,runs) {
        // Initialize global weights
        vector<vector<float> > global(sz(actions),vector<float>(numFeatures,0));
        int maxActiveFeatures = 0;

        float totalReward = 0;
        while(!ale.game_over()) {
            float currReward = 0;
            // Save current state
            ale.saveState();

            cout<<"Running Sarsa trajectories\n";
            // Get policy from Sarsa
            VI policy = SarsaLearner(ale, Params, global, maxActiveFeatures);
            assert(sz(policy) > 0);

            // Load back current state
            ale.loadState();

            // Take action as per policy
            REP(i,sz(policy)) {
                REP(k,numStepsPerAction) {
                    currReward += ale.act(actions[policy[i]]);
                }
            }
            cout<<"real world reward = "<<currReward<<endl;
            totalReward += currReward;
        }
        printf("Real world Episode %d ended with total reward = %f\n",episode,totalReward);
        ale.reset_game();
        cumReward += totalReward;
    }
    cout<<"Cumulative Reward = "<<cumReward<<endl;
    return 0;
}
