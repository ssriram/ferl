/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Commonly Used Functions in Sarsa
****************************************************************************** */

#include "/home/sriram/ferl/Thesis/Research/Code/src/Reinforcement_Learning/Tile_Coding/tiles.cpp"

#define REP(i,n) for(int i=0;i<n;i++)
#define sz(c) (signed int) c.size()
#define TOL 1e-9

using namespace std;

/* Compute Q-value from local features */
float getQ(int tiles[], const int num_tilings, vector<float>& w) {
    float Q = 0;
    REP(i,num_tilings) Q += w[tiles[i]];
    return Q;
}

/* Compute Q-value from local and global features */
float getQ(int tiles[], const int num_tilings, float Q, vector<float>& w) {
    REP(i,num_tilings) Q += w[tiles[i]]; // Q-value from global already computed
    return Q;
}

/* Compute Q-value from local and global features for all actions */
void computeQ(int tiles[], const int num_tilings, vector<float>& Q,
              vector<vector<float> >& w) {
    int n = sz(Q);
    REP(i,n) Q[i] += getQ(tiles,num_tilings,w[i]);
}

/* Pick best action among computed Q-values */
pair<float,int> getBestAction(vector<float>& Q) {
    int bestAction = -1;
    float bestQ = -INF;
    int n = sz(Q);

    for(int i=0;i<n;i++) if(bestQ < Q[i]) bestQ = Q[i], bestAction = i;

    // break ties randomly
    VI same;
    REP(i,n) if(abs(Q[i] - bestQ) < TOL) same.pb(i);

    if(sz(same) > 1) bestAction = same[rand() % sz(same)];

    return make_pair(Q[bestAction],bestAction);
}

/* Pick best action using Q-value computed from local features */
pair<float,int> getBestAction(int tiles[], const int num_tilings, vector<vector<float> >& w) {
    int bestAction = -1;
    float bestQ = -INF;
    int n = sz(w);
    float Q[n];
    
    for(int i=0;i<n;i++) {
        Q[i] = getQ(tiles,num_tilings,w[i]);
        if(bestQ < Q[i]) bestQ = Q[i], bestAction = i;
    }

    // break ties randomly
    VI same;
    REP(i,n) if(abs(Q[i] - bestQ) < TOL) same.pb(i);

    if(sz(same) > 1) bestAction = same[rand() % sz(same)];

    return make_pair(Q[bestAction],bestAction);
}

/* Using Q-value computed using global and local features */
pair<float,int> getBestAction(int tiles[], const int num_tilings, vector<float>&Q, vector<vector<float> >& w) {
    int bestAction = -1;
    float bestQ = -INF;
    int n = sz(Q);

    for(int i=0;i<n;i++) {
        Q[i] += getQ(tiles,num_tilings,w[i]); // Q-value from global features already computed and stored
        if(bestQ < Q[i]) bestQ = Q[i], bestAction = i;
    }

    // break ties randomly
    VI same;
    REP(i,n) if(abs(Q[i] - bestQ) < TOL) same.pb(i);

    if(sz(same) > 1) bestAction = same[rand() % sz(same)];

    return make_pair(Q[bestAction],bestAction);
}

/* Compute next state using translation operator */
void getNextState(vector<float>& b,float currState[],float nextState[]) {
    // x_{t+1} = x_t + b 
    int num_dim = sz(b);
    for(int k=0;k<num_dim;k++) {
        nextState[k] = currState[k] + b[k];
    }
}

/* Compute Norm of a vector */
float Norm(vector<float>& v) {
    assert(sz(v));
    int m = sz(v);
    float norm = 0;
    REP(i,m) norm += v[i] * v[i];
    return sqrt(norm) / (float) m;
}

/* Perform tile coding for a state */
void fillTiles(int tiles_array[], const int num_tilings, const int num_weights,
          float state[], const int num_dim) {
    int oneD_tilings = num_tilings/2;
    int offset = oneD_tilings / num_dim;
    int idx = 0;
    // Perform 1-d tiling on each dimension
    REP(i,num_dim) {
        tiles1(&tiles_array[idx],offset,num_weights,state[i],i);
        idx += offset;
    }
    // Perform joint tile coding 
    tiles(&tiles_array[oneD_tilings],num_tilings/2,num_weights,state,num_dim);
}
