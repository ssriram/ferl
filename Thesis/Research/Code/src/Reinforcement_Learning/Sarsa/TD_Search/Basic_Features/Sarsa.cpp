/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Sarsa for local features (Basic)
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>
#include "/home/sriram/ferl/Thesis/Research/Code/src/Feature_Extraction/GetBasicFeatures.cpp"
#include "/home/sriram/ferl/Thesis/Research/Code/src/Reinforcement_Learning/Sarsa/Sarsa.h"

using namespace std;

VI SarsaLearner(ALEInterface& ale, map<string,float>& Params, vector<vector<float> >& local) {

    srand(time(NULL));
    
    // Get the vector of minimal actions
    ActionVect actions = ale.getMinimalActionSet();
    const int num_actions = sz(actions);

    // read params
    const float gamma = Params["gamma"];
    const int epsilon = (int) Params["epsilon"];
    const int numStepsPerAction = Params.find("num_steps") == Params.end() ? 1 : (int) Params["num_steps"];
    const int numRows = (int) Params["num_rows"];
    const int numCols = (int) Params["num_cols"];
    const int numColors = (int) Params["num_colors"];
    const int numRuns = Params.find("num_sarsa") == Params.end() ? 1 : (int) Params["num_sarsa"];
    const float alpha = Params["alpha"] / (float) (numRows * numCols * numColors);
    const int episodeLength = Params.find("episode_length") == Params.end() ? 60 : (int) Params["episode_length"];
    const int policyLength = Params.find("policy_length") == Params.end() ? 1 : (int) Params["policy_length"];

    const int numFeatures = numCols * numRows * numColors * 2;
    const int half = numFeatures / 2;
    
    // initialize variables
    VI currState, nextState;
    vector<int>& curr = currState;
    vector<int>& next = nextState;
    float cumReward = 0;
    int currAction, nextAction;
    float currQlocal, nextQlocal;
    pair<float,int> p;
    float firstReward = 0;

    // pre-compute Q-values for all actions. Assume all features upto half are off
    vector<float> Qlocal(num_actions,0);

    REP(episode,numRuns) {
        float totalReward = 0;
        // Get features for start state
        curr.clear();
        next.clear();
        getBasicFeatures(ale.getScreen(), numCols, numRows, numColors, next);

        // Update Q-values
        fill(all(Qlocal),0);
        REP(i,half) REP(j,num_actions) Qlocal[j] += local[j][half+i];
        REP(i,sz(next)) {
            REP(j,num_actions) {
                Qlocal[j] += local[j][next[i]] - local[j][half+next[i]];
            }
        }
        
        if(rand()%100 > epsilon - 1) {
            p = getBestAction(Qlocal);
            currQlocal = p.first; currAction = p.second;
        }
        else currAction = rand() % num_actions, currQlocal = Qlocal[currAction];

        cout<<currQlocal<<endl;
        // save ale state, so that we return to it when we finish episode
        ale.saveState();
        swap(curr,next);
        int length = 0;

        while(!ale.game_over() && length < episodeLength) {
            float currReward = 0;
            for(int i=0;i<numStepsPerAction && !ale.game_over();i++) {
                currReward += ale.act(actions[currAction]);
            }
            // Normalize rewards using first reward
            if(firstReward) currReward /= firstReward;
            else if(currReward) firstReward = currReward, currReward = 1;
            totalReward += currReward;
            
            bool gameOver = ale.game_over();
            int common = 0;
            if(!gameOver) {
                // Get Features for next state
                next.clear();
                getBasicFeatures(ale.getScreen(), numCols, numRows, numColors, next);

                // Compute Q-values for next state
                int curridx = 0, nextidx = 0;
                int m = sz(curr), n = sz(next);
                while(curridx < m && nextidx < n) {
                    if(curr[curridx] == next[nextidx]) ++curridx, ++nextidx, ++common;
                    else if(curr[curridx] < next[nextidx]) {
                        REP(j,num_actions) {
                            Qlocal[j] += local[j][half+curr[curridx]] - local[j][curr[curridx]];
                        }
                        ++curridx;
                    }
                    else {
                        REP(j,num_actions) {
                            Qlocal[j] += local[j][next[nextidx]] - local[j][half+next[nextidx]];
                        }
                        ++nextidx;
                    }
                }
                for(;curridx < m;curridx++) {
                    REP(j,num_actions) {
                        Qlocal[j] += local[j][half+curr[curridx]] - local[j][curr[curridx]];
                    }
                }
                for(;nextidx < n;nextidx++) {
                    REP(j,num_actions) {
                        Qlocal[j] += local[j][next[nextidx]] - local[j][half+next[nextidx]];
                    }
                }

                if(rand()%100 > epsilon - 1) {
                    p = getBestAction(Qlocal);
                    nextQlocal = p.first; nextAction = p.second;
                }
                else nextAction = rand() % num_actions, nextQlocal = Qlocal[nextAction];
            }
            else nextQlocal = 0;

            float deltaLocal = currReward + gamma * nextQlocal - currQlocal;

            // Update local weights using on-policy update
            REP(i,half) local[currAction][half+i] += alpha * deltaLocal;
            REP(i,sz(curr)) {
                local[currAction][curr[i]] += alpha * deltaLocal;
                local[currAction][half+curr[i]] -= alpha * deltaLocal;
            }
            // Qlocal for currAction has changed, update it
            int count = half - sz(curr) - sz(next) + 2 * common;
            Qlocal[currAction] += count * alpha * deltaLocal;

            if(!gameOver) {
                // Reset variables
                if(currAction != nextAction) currQlocal = nextQlocal;
                else currQlocal = Qlocal[currAction]; // use updated Q-value
                currAction = nextAction;
                swap(curr,next);
            }
            ++length;
        }
        //printf("Episode %d ended with total reward = %f\n",episode,totalReward);
        cumReward += totalReward;
        ale.loadState(); // Reload start state
    }
    cout<<"Average Reward = "<<cumReward/numRuns<<endl;

    // Return the learned policy for the required length
    ale.saveState(); // save ale state
    VI policy(policyLength);
    fill(all(Qlocal),0);
    curr.clear();
    REP(len, policyLength) {
        next.clear();
        getBasicFeatures(ale.getScreen(), numCols, numRows, numColors, next);
        // Compute Q-values for next state
        fill(all(Qlocal),0);
        REP(i,half) REP(j,num_actions) Qlocal[j] += local[j][half+i];
        REP(i,sz(next)) REP(j,num_actions) Qlocal[j] += local[j][next[i]] - local[j][half+next[i]];
        p = getBestAction(Qlocal);
        nextQlocal = p.first; nextAction = p.second;
        policy[len] = p.second;
        REP(i,numStepsPerAction) ale.act(actions[p.second]);
        if(ale.game_over()) break;
        swap(curr,next);
    }
    ale.loadState(); // load back saved state

    return policy;
}
