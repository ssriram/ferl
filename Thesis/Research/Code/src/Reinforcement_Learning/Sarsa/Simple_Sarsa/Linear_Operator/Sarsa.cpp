/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Apply Sarsa learning algorithm on states (obtained from Isomap)
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string.h>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define TOL (int) 1e-9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>
#include <shogun/lib/SGMatrix.h>
#include <shogun/lib/SGVector.h>
#include "/home/sriram/Bitbucket/Thesis/Research/Code/src/Reinforcement_Learning/Tile_Coding/tiles.cpp"
#include "/home/sriram/Bitbucket/Thesis/Research/Code/src/Feature_Extraction/GetManifoldFeatures.cpp"

using namespace std;
using namespace shogun;

const int num_weights = 10000000;

float getQ(int tiles_array[], const int num_tilings, int action, vector<vector<float> >& w) {
    float Q = 0;
    REP(i,num_tilings) Q += w[action][tiles_array[i]];
    
    return Q;
}

int getBestAction(int tiles_array[], const int num_tilings, vector<vector<float> >& w) {
    int bestAction = -1;
    float bestQ = -INF;
    int n = sz(w);
    vector<float> Q(n);
    
    REP(i,n) {
        Q[i] = getQ(tiles_array, num_tilings, i, w);
        if(bestQ < Q[i]) bestQ = Q[i], bestAction = i;
    }
    
    VI same;
    REP(i,n) if(abs(Q[i] - bestQ)<TOL) same.pb(i);

    if(sz(same) > 1) bestAction = same[rand() % sz(same)];

    return bestAction;
}

void getNextState(vector<vector<float> >& b, int currAction,float currState[],float nextState[]) {
    // x_{t+1} = x_t + b 
    int num_dim = sz(b[0]);
    for(int k=0;k<num_dim;k++) {
        nextState[k] = currState[k] + b[currAction][k];
    }
}

// End of Functions

Action SarsaLearner(ALEInterface& ale, const int num_tilings, const int num_dim, const int max_depth, const float gamma, const int epsilon, const float alpha) {

    srand(time(NULL));
    
    // Get the vector of minimal actions
    ActionVect actions = ale.getMinimalActionSet();
    const int num_actions = sz(actions);

    cout<<"gamma = "<<gamma<<", epsilon = "<<epsilon<<", alpha = "<<alpha<<endl;

    // Get Features for start state, and Operator matrix
    SGMatrix<float64_t> Matrix = generateDistanceGraph(ale,actions,max_depth,num_dim);

    assert(Matrix.num_rows == num_actions + 1 && Matrix.num_cols == num_dim);
    float startState[num_dim];
    REP(i,num_dim) startState[i] = Matrix(1,i);
    vector<vector<float> > b(num_actions,vector<float>(num_dim));
    REP(i,num_actions) REP(k,num_dim) b[i][k] = Matrix(i+1,k);

    vector<vector<float> >w(sz(actions),vector<float>(num_weights,(float)1.0/(float)num_tilings));        

    // set start state, tile coding params
    float nextState[num_dim], currState[num_dim];
    int curr_tiles_array[num_tilings], next_tiles_array[num_tilings];
    const float divisor = (float) num_tilings;

    int runs = 100;
    float cumReward = 0;
    float tempCurr[num_dim], tempNext[num_dim];
    REP(episode,runs) {
        float totalReward = 0;
        REP(i,num_dim) currState[i] = startState[i];
        REP(i,num_dim) tempCurr[i] = currState[i] / divisor;
        tiles(curr_tiles_array,num_tilings,num_weights,tempCurr,num_dim);

        int currAction, nextAction;
        if(rand()%100 > epsilon - 1) currAction = getBestAction(curr_tiles_array,num_tilings,w);
        else {currAction = rand() % sz(actions);cout<<"random\n";}

        float currQ, nextQ;
        currQ = getQ(curr_tiles_array,num_tilings,currAction,w);
        cout<<"Value of start state = "<<currQ<<", action = "<<currAction<<endl;
        int length = 0;

        while(!ale.game_over() && length < 160) {
            float currReward = 0;
            REP(i,5) {
                currReward += ale.act(actions[currAction]);
            }
            // apply operator on current state to get next state
            getNextState(b,currAction,currState,nextState);
            totalReward += currReward;
            REP(i,num_dim) tempNext[i] = nextState[i] / divisor;

            tiles(next_tiles_array,num_tilings,num_weights,tempNext,num_dim);
            
            if(rand()%100 > epsilon - 1) nextAction = getBestAction(next_tiles_array,num_tilings,w);
            else {nextAction = rand() % sz(actions);cout<<"random\n";}

            nextQ = getQ(next_tiles_array,num_tilings,nextAction,w);

            currQ = getQ(curr_tiles_array,num_tilings,currAction,w);

            float delta = currReward + gamma * nextQ - currQ;

            // Update w
            REP(i,num_tilings) w[currAction][curr_tiles_array[i]] += alpha * delta; 

            // Reset variables
            currAction = nextAction;
            REP(i,num_dim) currState[i] = nextState[i];
            REP(i,num_tilings) curr_tiles_array[i] = next_tiles_array[i];
            ++length;
        }
        printf("Episode %d ended with total reward = %f\n",episode,totalReward);
        ale.reset_game();
        cumReward += totalReward;
    }
    cout<<"Cumulative Reward = "<<cumReward<<endl;

    // Get best action from the learned policy
    REP(i,num_dim) tempCurr[i] = startState[i] / divisor;
    tiles(curr_tiles_array,num_tilings,num_weights,tempCurr,num_dim);
    return actions[getBestAction(curr_tiles_array,num_tilings,w)];
}
