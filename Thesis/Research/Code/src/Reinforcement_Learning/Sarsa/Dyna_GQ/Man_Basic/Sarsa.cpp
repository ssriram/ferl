/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Greedy GQ using global (Basic) and local (Manifold) features
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>
#include "/home/ssriram/ferl/Thesis/Research/Code/src/Feature_Extraction/GetManifoldFeatures.cpp"
#include "/home/ssriram/ferl/Thesis/Research/Code/src/Feature_Extraction/GetBasicFeatures.cpp"
#include "/home/ssriram/ferl/Thesis/Research/Code/src/Reinforcement_Learning/Sarsa/Sarsa.h"

using namespace std;

const int num_weights = 1000000;

VI SarsaLearner(ALEInterface& ale, map<string,float>& Params, 
                vector<vector<float> >& prevGlobal, int& maxActiveFeatures) {

    srand(time(NULL));
    
    // Get the vector of minimal actions
    ActionVect actions = ale.getMinimalActionSet();
    const int num_actions = sz(actions);

    // read params
    const float gamma = Params["gamma"];
    const int epsilon = (int) Params["epsilon"];
    const int numStepsPerAction = Params.find("num_steps") == Params.end() ? 1 : (int) Params["num_steps"];
    const int max_states = (int) Params["max_states"];
    const int num_tilings = (int) Params["num_tilings"];
    const int numRows = (int) Params["num_rows"];
    const int numCols = (int) Params["num_cols"];
    const int numColors = (int) Params["num_colors"];
    const int numRuns = Params.find("num_sarsa") == Params.end() ? 1 : (int) Params["num_sarsa"];
    const float alpha = Params["alpha"] / (float) num_tilings;
    float beta = Params["alpha"] / (float) (numRows * numCols * numColors); // This learning rate will not be constant
    const int episodeLength = Params.find("episode_length") == Params.end() ? 60 : (int) Params["episode_length"];
    const int policyLength = Params.find("policy_length") == Params.end() ? 1 : (int) Params["policy_length"];

    // Set # of Global features
    const int numGlobalFeatures = numCols * numRows * numColors;
    
    // Initialize local and global weights
    vector<vector<float> > curGlobal = prevGlobal;
    vector<vector<float> > local(sz(actions),vector<float>(num_weights,1.0/(float)num_tilings));
    vector<vector<float> > u(sz(actions),vector<float>(numGlobalFeatures,0));

    // Learn manifold and translation operator
    cout<<"Learning a manifold\n";
    ale.saveState();
    vector<vector<float> > Matrix = learnManifold(ale,actions,max_states,numStepsPerAction);
    ale.loadState();
    assert(sz(Matrix) == num_actions + 1);
    const int num_dim = sz(Matrix[0]);
    const float divisor = (float) num_tilings;
    vector<vector<float> > b(num_actions,vector<float>(num_dim));
    REP(i,num_actions) REP(k,num_dim) b[i][k] = Matrix[i+1][k] / divisor;
    float startState[num_dim];
    REP(i,num_dim) startState[i] = Matrix[0][i] / divisor;
    
    // initialize local feature variables
    float currLocalState[num_dim], nextLocalState[num_dim];
    float *currLocal = currLocalState, *nextLocal = nextLocalState;
    int currTiles[num_tilings], nextTiles[num_tilings];
    int *currT = currTiles, *nextT = nextTiles;

    // initialize global feature variables
    VI currGlobalState, nextGlobalState;
    vector<int>& currGlobal = currGlobalState;
    vector<int>& nextGlobal = nextGlobalState;

    float cumReward = 0;
    int currAction, nextAction, bestGlobalAction;
    float currQlocal, nextQlocal, currQglobal, nextQglobal;
    pair<float,int> p;
    float firstReward = 0;

    // pre-compute local and global Q-values for all actions. Assume all features are off
    vector<float> Qlocal(sz(actions),0);
    vector<float> Qglobal(sz(actions),0);

    cout<<"Running Sarsa trajectories\n";
    REP(episode,numRuns) {
        float totalReward = 0;

        // Get local features for start state
        REP(i,num_dim) currLocal[i] = startState[i];
        tiles(currT,num_tilings,num_weights,currLocal,num_dim);

        // Get global features for start state
        currGlobal.clear();
        nextGlobal.clear();
        getBasicFeatures(ale.getScreen(), numCols, numRows, numColors, nextGlobal);

        // Update local and global Q-values
        fill(all(Qlocal),0);
        fill(all(Qglobal),0);
        maxActiveFeatures = max(maxActiveFeatures, sz(nextGlobal));
        REP(i,sz(nextGlobal)) {
            REP(j,num_actions) {
                Qlocal[j] += prevGlobal[j][nextGlobal[i]];
                Qglobal[j] += curGlobal[j][nextGlobal[i]];
            }
        }
        
        // Compute Q-value for local features for all actions
        computeQ(currT,num_tilings,Qlocal,local);

        // Choose action based on eps-Greedy
        if(rand()%100 > epsilon - 1) {
            p = getBestAction(Qlocal);
            currQlocal = p.first; currAction = p.second;
        }
        else {
            currAction = rand() % sz(actions);
            currQlocal = Qlocal[currAction];
        }

        currQglobal = Qglobal[currAction];

        cout<<currQlocal<<" "<<currQglobal<<endl;
        // save ale state, so that we return to it when we finish episode
        ale.saveState();
        swap(currGlobal,nextGlobal);
        int length = 0;

        while(!ale.game_over() && length < episodeLength) {
            float currReward = 0;
            for(int i=0;i<numStepsPerAction && !ale.game_over();i++) {
                currReward += ale.act(actions[currAction]);
            }
            // Normalize rewards using first reward
            if(firstReward) currReward /= firstReward;
            else if(currReward) firstReward = currReward, currReward = 1;
            totalReward += currReward;
            
            bool gameOver = ale.game_over();
            int common = 0;
            if(!gameOver) {
                // Get Local Features for next state
                getNextState(b[currAction],currLocal,nextLocal);
                tiles(nextT,num_tilings,num_weights,nextLocal,num_dim);

                // Get Global Features for next state
                nextGlobal.clear();
                getBasicFeatures(ale.getScreen(), numCols, numRows, numColors, nextGlobal);

                // Compute Qlocal for contribution from global features
                fill(all(Qlocal),0);
                maxActiveFeatures = max(maxActiveFeatures, sz(nextGlobal));
                REP(i,sz(nextGlobal)) REP(j,num_actions) Qlocal[j] += prevGlobal[j][nextGlobal[i]];

                // Compute Qglobal 
                int curridx = 0, nextidx = 0;
                int m = sz(currGlobal), n = sz(nextGlobal);
                while(curridx < m && nextidx < n) {
                    if(currGlobal[curridx] == nextGlobal[nextidx]) ++curridx, ++nextidx, ++common;
                    else if(currGlobal[curridx] < nextGlobal[nextidx]) {
                        REP(j,num_actions) Qglobal[j] -= curGlobal[j][currGlobal[curridx]];
                        ++curridx;
                    }
                    else {
                        REP(j,num_actions) Qglobal[j] += curGlobal[j][nextGlobal[nextidx]];
                        ++nextidx;
                    }
                }
                for(;curridx < m;curridx++) REP(j,num_actions) Qglobal[j] -= curGlobal[j][currGlobal[curridx]];
                for(;nextidx < n;nextidx++) REP(j,num_actions) Qglobal[j] += curGlobal[j][nextGlobal[nextidx]];

                // Compute Q-value for local features for all actions
                computeQ(nextT,num_tilings,Qlocal,local);

                // Choose nextAction using eps-Greedy
                if(rand()%100 > epsilon - 1) {
                    p = getBestAction(Qlocal);
                    nextQlocal = p.first; nextAction = p.second;
                }
                else {
                    nextAction = rand() % sz(actions);
                    nextQlocal = Qlocal[nextAction];
                } 
            }
            else nextQlocal = 0;

            float deltaLocal = currReward + gamma * nextQlocal - currQlocal;

            // Update local weights using on-policy update
            REP(i,num_tilings) local[currAction][currT[i]] += alpha * deltaLocal;

            // Compute deltaGlobal for Off-policy update
            if(!gameOver) {
                p = getBestAction(Qglobal); 
                nextQglobal = p.first, bestGlobalAction = p.second;
            }
            else nextQglobal = 0;

            float deltaGlobal = currReward + gamma * nextQglobal - currQglobal;

            float dotProduct = 0;
            REP(i,sz(currGlobal)) dotProduct += u[currAction][currGlobal[i]];

            // Set learning rate
            beta = Params["alpha"] / (float) maxActiveFeatures;

            // Update global weights using Greedy GQ 
            float x = beta * deltaGlobal, y = 5.0 * beta * (deltaGlobal - dotProduct);
            REP(i,sz(currGlobal)) {
                curGlobal[currAction][currGlobal[i]] += x;
                u[currAction][currGlobal[i]] += y;
            }

            // Update Q-values of currAction because of change in weights
            Qglobal[currAction] += common * x;

            if(!gameOver) {
                // Greedy GQ - update for bestAction
                float update = beta * gamma * dotProduct;
                REP(i,sz(nextGlobal)) {
                    curGlobal[bestGlobalAction][nextGlobal[i]] -= update;
                }
                // Update Q-values of bestAction because of change in weights
                Qglobal[bestGlobalAction] -= sz(nextGlobal) * update;

                // Reset variables
                if(currAction != nextAction) currQlocal = nextQlocal, currQglobal = Qglobal[nextAction];
                else {
                    // get count of overlapping tiles
                    map<int,bool> M;
                    REP(i,num_tilings) M[currT[i]] = true;
                    int count = 0;
                    REP(i,num_tilings) if(M.find(nextT[i]) != M.end()) ++count;
                    Qlocal[nextAction] += count * alpha * deltaLocal;
                    currQlocal = Qlocal[nextAction]; // use updated Q-value
                    currQglobal = Qglobal[nextAction]; // use updated Q-value
                }
                currAction = nextAction;
                swap(currLocal,nextLocal);
                swap(currT,nextT);
                swap(currGlobal,nextGlobal);
            }
            ++length;
        }
        //printf("Episode %d ended with total reward = %f\n",episode,totalReward);
        cumReward += totalReward;
        ale.loadState(); // Reload start state
    }
    cout<<"Average Reward = "<<cumReward/numRuns<<endl;

    // Return the learned policy for the required length
    ale.saveState(); // save ale state
    VI policy(policyLength);
    REP(i,num_dim) currLocal[i] = startState[i];
    REP(len, policyLength) {
        // Get local features
        tiles(currT,num_tilings,num_weights,currLocal,num_dim);
        // Get global features
        nextGlobal.clear();
        getBasicFeatures(ale.getScreen(), numCols, numRows, numColors, nextGlobal);

        // Compute Q-value from global weights
        fill(all(Qlocal),0);
        REP(i,sz(nextGlobal)) REP(j,sz(actions)) Qlocal[j] += prevGlobal[j][nextGlobal[i]];
        
        // Compute Q-value from local weights
        computeQ(currT,num_tilings,Qlocal,local);

        // Choose best action for this state
        p = getBestAction(Qlocal);
        nextQlocal = p.first; nextAction = p.second;
        policy[len] = p.second;

        // Go to next state
        getNextState(b[p.second],currLocal,nextLocal); 
        REP(i,numStepsPerAction) ale.act(actions[p.second]);
        if(ale.game_over()) break;

        // Reset variables
        swap(currLocal,nextLocal);
    }
    ale.loadState(); // load back saved state

    // set global weights to newly learnt weights
    REP(i,sz(actions)) REP(j,numGlobalFeatures) prevGlobal[i][j] = curGlobal[i][j];

    return policy;
}
