
use POSIX;
my @arr;
while(<>) {
    if($_ =~ m/Episode (\d+) ended with total reward = ([1-9]+)/) {
        $episodeNum = $1;
        $reward = $2;
        my $index = floor($episodeNum/1000);
        $arr[$index] += $reward;
    }
}
print "@arr\n";
