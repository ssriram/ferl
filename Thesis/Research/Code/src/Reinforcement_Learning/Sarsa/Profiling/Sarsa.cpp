/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Apply Sarsa learning algorithm on states (obtained from Isomap)
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string.h>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define TOL (int) 1e-9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>
#include <iostream>
#include "/home/sriram/Bitbucket/Thesis/Research/Code/src/Reinforcement_Learning/Tile_Coding/tiles.cpp"
using namespace std;

int num_tilings;
const int num_dim = 7;
const int num_weights = 10000000;

// Functions
vector<vector<float> > loadMatrix(const char *fileName, int num_rows, int num_cols) {
    vector<vector<float> > v(num_rows,vector<float>(num_cols));
    ifstream file (fileName);
    assert(file.is_open());
    int lineNum = 0;
    string temp;
    char line[5000];
    while(!file.eof()) {
        getline(file,temp,'\n');
        strcpy(line,temp.c_str());
        char *p;
        p = strtok(line," ");
        int k = 0;
        while(p!=NULL) {
            printf("%s ",p);
            v[lineNum][k++] = atof(p);
            p = strtok(NULL," ");
        }
        cout<<endl;
        lineNum++;
    }
    return v;
}

float getQ(int tiles_array[], int action, vector<vector<float> >& w) {
    float Q = 0;
    REP(i,num_tilings) Q += w[action][tiles_array[i]];
    
    return Q;
}

int getBestAction(int tiles_array[], vector<vector<float> >& w) {
    int bestAction = -1;
    float bestQ = -INF;
    int n = sz(w);
    vector<float> Q(n);
    
    REP(i,n) {
        Q[i] = getQ(tiles_array, i, w);
        if(bestQ < Q[i]) bestQ = Q[i], bestAction = i;
    }
    
    VI same;
    REP(i,n) if(abs(Q[i] - bestQ)<TOL) same.pb(i);

    if(sz(same) > 1) bestAction = same[rand() % sz(same)];

    return bestAction;
}

void getNextState(vector<vector<float> >& b, int currAction,float currState[],float nextState[]) {
    // x_{t+1} = x_t + b 
    for(int k=0;k<num_dim;k++) {
        nextState[k] = currState[k] + b[currAction][k];
    }
}

// End of Functions

int main(int argc, char** argv) {
    if (argc < 7) {
        std::cerr << "Usage: " << argv[0] << " rom_file Linear_operator_dir num_tilings gamma epsilon alpha"<<std::endl;
        return 1;
    }

    srand(time(NULL));
    
    ALEInterface ale(0);

    // Load the ROM file
    ale.loadROM(argv[1],"A");

    // Get the vector of minimal actions
    ActionVect actions = ale.getMinimalActionSet();

    // Set params
    string b_file(argv[2]);
    b_file += "/b.txt";
    vector<vector<float> > b = loadMatrix(b_file.c_str(),sz(actions),num_dim);
    num_tilings = atoi(argv[3]);
    const float gamma = atof(argv[4]);
    const int epsilon = (int) ceil(atof(argv[5]) * 100);
    const float alpha = (float) atof(argv[6]) / num_tilings;
    cout<<"gamma = "<<gamma<<", epsilon = "<<epsilon<<", alpha = "<<alpha<<endl;

    vector<vector<float> >w(sz(actions),vector<float>(num_weights,(float)1.0/(float)num_tilings));        

    // set start state, tile coding params
    float nextState[num_dim];
    int curr_tiles_array[num_tilings], next_tiles_array[num_tilings];
    const float divisor = (float) num_tilings;

    int runs = 100;
    float cumReward = 0;
    float tempCurr[num_dim], tempNext[num_dim];
    REP(episode,runs) {
        float totalReward = 0;
        float currState[num_dim] = {-23.4089, -2.2179, 1.76911, -0.372229, 1.06019, 0.606012, -0.987044};
        REP(i,num_dim) tempCurr[i] = currState[i] / divisor;
        tiles(curr_tiles_array,num_tilings,num_weights,tempCurr,num_dim);

        int currAction, nextAction;
        if(rand()%100 > epsilon - 1) currAction = getBestAction(curr_tiles_array, w);
        else {currAction = rand() % sz(actions);cout<<"random\n";}

        float currQ, nextQ;
        currQ = getQ(curr_tiles_array,currAction,w);
        cout<<"Value of start state = "<<currQ<<", action = "<<currAction<<endl;
        int length = 0;

        while(!ale.game_over() && length < 160) {
            float currReward = 0;
            REP(i,5) {
                currReward += ale.act(actions[currAction]);
            }
            // apply operator on current state to get next state
            getNextState(b,currAction,currState,nextState);
            totalReward += currReward;
            REP(i,num_dim) tempNext[i] = nextState[i] / divisor;

            tiles(next_tiles_array,num_tilings,num_weights,tempNext,num_dim);
            
            if(rand()%100 > epsilon - 1) nextAction = getBestAction(next_tiles_array, w);
            else {nextAction = rand() % sz(actions);cout<<"random\n";}

            nextQ = getQ(next_tiles_array,nextAction,w);

            currQ = getQ(curr_tiles_array,currAction,w);

            float delta = currReward + gamma * nextQ - currQ;

            // Update w
            REP(i,num_tilings) w[currAction][curr_tiles_array[i]] += alpha * delta; 

            // Reset variables
            currAction = nextAction;
            REP(i,num_dim) currState[i] = nextState[i];
            REP(i,num_tilings) curr_tiles_array[i] = next_tiles_array[i];
            ++length;
        }
        printf("Episode %d ended with total reward = %f\n",episode,totalReward);
        ale.reset_game();
        cumReward += totalReward;
    }
    cout<<"Cumulative Reward = "<<cumReward<<endl;
    return 0;
}
