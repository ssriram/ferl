/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Apply Sarsa learning algorithm on states (obtained from Isomap)
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string.h>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>
#include <iostream>
#include "/home/sriram/Desktop/Atari/Code/FERL/Reinforcement_Learning/Tile_Coding/tiles.cpp"
#include "/home/sriram/Bitbucket/Thesis/Research/Code/src/Feature_Extraction/GenDistMatrix.h"
using namespace std;

// Constants
const int oneD_tilings = 1024;
const int num_tilings = oneD_tilings;
const int num_weights = 100000;
const int num_dim = 2;
const int future_depth = 5;
const int topK = 2;

// Functions
float getQ(int tiles_array[], int action, vector<vector<float> >& w) {
    float Q = 0;
    REP(i,num_tilings) Q += w[action][tiles_array[i]];
    
    return Q;
}

int getBestAction(float state[], vector<vector<float> >& w) {
    int bestAction = -1;
    float bestQ = -INF;
    vector<float> Q;
    int tiles_array[num_tilings];
    tiles1(tiles_array,oneD_tilings/2,num_weights,state[0],0);
    tiles1(&tiles_array[oneD_tilings/2],oneD_tilings/2,num_weights,state[1],1);
    
    REP(i,sz(w)) {
        float temp = getQ(tiles_array, i, w);
        Q.pb(temp);
        if(bestQ < temp) bestQ = temp, bestAction = i;
    }
    
    VI same;
    REP(i,sz(Q)) if(Q[i] == bestQ) same.pb(i);

    if(sz(same) > 1) bestAction = rand() % sz(same); 

    return bestAction;
}

vector<vector<float> > loadEmbedding(const char *fileName) {
    vector<vector<float> > v;
    ifstream file (fileName);
    if(file.is_open()) {
        int lineNum = 0;
        string temp;
        char line[5000];
        while(!file.eof()) {
            vector<float> row;
            getline(file,temp,'\n');
            strcpy(line,temp.c_str());
            char *p;
            p = strtok(line," ");
            while(p!=NULL) {
                row.pb(atof(p));
                p = strtok(NULL," ");
            }
            v.pb(row);
        }
    }
    file.close();
    return v;
}

// End of Functions

int main(int argc, char** argv) {
    if (argc < 6) {
        std::cerr << "Usage: " << argv[0] << " rom_file Embedding_file gamma epsilon alpha"<<std::endl;
        return 1;
    }

    srand(time(NULL));
    
    ALEInterface ale(1);

    // Load the ROM file
    ale.loadROM(argv[1]);

    // Get the vector of minimal actions
    ActionVect actions = ale.getMinimalActionSet();

    // Initialize shogun
    init_shogun(&print_message,&print_warning,&print_error);

    // Set params
    const float gamma = atof(argv[3]);
    const float epsilon = atof(argv[4]);
    const float alpha = atof(argv[5]) / num_tilings;

    vector<vector<float> >w(sz(actions),vector<float>(num_weights,1.0/num_tilings));        

    // set start state, tile coding params
    float currState[num_dim];
    float nextState[num_dim];
    int tiles_array[num_tilings];

    int runs = 1000;
    REP(episode,runs) {
        ale.saveState();
        // Get the states for future
        map<PII,int> M = generateDistanceGraph(ale,actions,future_depth,num_dim,argv[2]);
        vector<vector<float> > Embedding = loadEmbedding(argv[2]);
        ale.loadState();
        
        float totalReward = 0;
        int currAction = rand() % sz(actions);
        int nextAction;
        int length = 0;
        int currIndex = 0;
        int nextIndex;
        REP(i,num_dim) cout<<Embedding[currIndex][i]<<" ";
        REP(k,num_dim) currState[k] = Embedding[currIndex][k];
        tiles1(tiles_array,oneD_tilings/2,num_weights,currState[0],0);
        tiles1(&tiles_array[oneD_tilings/2],oneD_tilings/2,num_weights,currState[1],1);
        cout<<getQ(tiles_array,currAction,w)<<endl;

        while(!ale.game_over() && length <= future_depth) {
            float currReward = 0;
            // Get current state
            REP(k,num_dim) currState[k] = Embedding[currIndex][k];
            REP(i,1) {
                currReward += ale.act(actions[currAction]);
            }
            nextIndex = M[make_pair(currIndex,currAction)];
            REP(k,num_dim) nextState[k] = Embedding[nextIndex][k];
            totalReward += currReward;

            if((float)(rand()%100)/100 > epsilon) nextAction = getBestAction(nextState, w);
            else nextAction = rand() % sz(actions);

            tiles1(tiles_array,oneD_tilings/2,num_weights,nextState[0],0);
            tiles1(&tiles_array[oneD_tilings/2],oneD_tilings/2,num_weights,nextState[1],1);
            float nextQ = getQ(tiles_array,nextAction,w);
            
            tiles1(tiles_array,oneD_tilings/2,num_weights,currState[0],0);
            tiles1(&tiles_array[oneD_tilings/2],oneD_tilings/2,num_weights,currState[1],1);
            float currQ = getQ(tiles_array,currAction,w);
	    
            float delta = currReward + gamma * nextQ - currQ;
            //cout<<currAction<<" "<<delta<<endl;

            // Update w
            REP(i,num_tilings) w[currAction][tiles_array[i]] += alpha * delta; 

            currAction = nextAction;
            currIndex = nextIndex;
            length++;
        }
        if(ale.game_over()) {
            printf("Episode %d ended with total reward = %f\n",episode,totalReward);
            ale.reset_game();
        }
    }
    cout<<"Weights = \n";
    REP(i,sz(w)) {
        REP(j,sz(w[0])) cout<<w[i][j]<<" ";
        cout<<endl;
    }

    return 0;
}
