#include <boost/thread.hpp>
#include <iostream>

using namespace std;
using namespace boost;
using namespace boost::this_thread;

// Global function called by thread
void GlobalFunction(int n) {
    cout<<"Thread called with n = "<<n<<endl;
    boost::this_thread::sleep(boost::posix_time::seconds(n));
}

int main() {
    boost::thread t1(GlobalFunction, 10);
    boost::thread t2(GlobalFunction, 1);
    t1.join();
    t2.join();
 
    return 0;
}
