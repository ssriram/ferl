/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Apply Sarsa learning algorithm on states (obtained from Isomap)
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string.h>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define TOL (int) 1e-9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>
#include "/home/ssriram/ferl/Thesis/Research/Code/src/Reinforcement_Learning/Sarsa/Only_Translation/Sarsa.cpp"
using namespace std;

map<string, float> parseParams(const char *param_file) {
    map<string,float> M;
    ifstream file (param_file);
    assert(file.is_open());  
    string temp, key;
    float val;
    while(!file.eof()) {
        getline(file,temp,'\n');
        std::istringstream iss(temp);
        iss >> key;
        iss >> val;
        M[key] = val; 
    }
    return M;
}

void validateParams(map<string, float>& M) {
    const char *keys[] = {"gamma", "alpha", "epsilon", "mode", "num_tilings", "display", "max_depth", "episode_length", "num_sarsa"};
    vector<string> v(keys,keys+9);
    map<string, float>::iterator it;
    REP(i,sz(v)) assert(M.find(v[i]) != M.end());
}

void displayParams(map<string,float>& M) {
    map<string, float>::iterator it;
    for(it=M.begin();it!=M.end();it++) cout<<it->first<<" "<<it->second<<endl;
}

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " rom_file param_file"<<std::endl;
        return 1;
    }

    map<string,float> Params = parseParams(argv[2]);
    validateParams(Params);
    displayParams(Params);
    
    const int display = (int) Params["display"];
    const int num_tilings = (int) Params["num_tilings"];
    const float gamma = Params["gamma"];
    const int epsilon = (int) Params["epsilon"];
    const float alpha = Params["alpha"] / (float) num_tilings;
    const string mode = (int) Params["mode"] ? "A" : "B";
    const int max_depth = (int) Params["max_depth"];
    const int numStepsPerAction = Params.find("num_steps") == Params.end() ? 1 : (int) Params["num_steps"];
    const int episodeLength = (int) Params["episode_length"];
    const int policyLength = Params.find("policy_length") == Params.end() ? 1 : (int) Params["policy_length"];
    const int sarsaRuns = (int) Params["num_sarsa"];

    ALEInterface ale(display);

    // Load the ROM file
    ale.loadROM(argv[1],mode);

    // Get the vector of minimal actions
    ActionVect actions = ale.getMinimalActionSet();

    cout<<"num_tilings = "<<num_tilings<<", gamma = "<<gamma<<", epsilon = "<<epsilon<<", alpha = "<<alpha<<endl;

    int runs = 1;
    float cumReward = 0;
    OSystem* sys = (ale.theOSystem).get();
    RomSettings* rom = (ale.settings.get());
    string md5 = sys->console().properties().get(Cartridge_MD5);
    REP(episode,runs) {
        float totalReward = 0;
        while(!ale.game_over()) {
            float currReward = 0;
            // Save current state
            ALEState state = (ale.environment)->getState();
            ALEState current_state = state.save(sys,rom,md5);

            cout<<"Running Sarsa trajectories\n";
            // Get policy from Sarsa
            VI policy = SarsaLearner(ale, num_tilings, max_depth, gamma, epsilon, alpha,
                                     episodeLength, numStepsPerAction, policyLength, sarsaRuns);
            assert(sz(policy) > 0);

            // Load back current state
            state = (ale.environment)->getState();
            state.load(sys,rom,md5,current_state);

            // Take action as per policy
            REP(i,sz(policy)) {
                REP(k,numStepsPerAction) {
                    currReward += ale.act(actions[policy[i]]);
                }
            }
            cout<<"real world reward = "<<currReward<<endl;
            totalReward += currReward;
            break;
        }
        printf("Real world Episode %d ended with total reward = %f\n",episode,totalReward);
        ale.reset_game();
        cumReward += totalReward;
    }
    cout<<"Cumulative Reward = "<<cumReward<<endl;
    return 0;
}
