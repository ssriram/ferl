/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Apply Sarsa learning algorithm on states (obtained from Isomap)
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string.h>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define TOL (int) 1e-9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>
#include "/home/ssriram/ferl/Thesis/Research/Code/src/Reinforcement_Learning/Sarsa/Sarsa_Basic/Sarsa.cpp"
using namespace std;

map<string, float> parseParams(const char *param_file) {
    map<string,float> M;
    ifstream file (param_file);
    assert(file.is_open());  
    string temp, key;
    float val;
    while(!file.eof()) {
        getline(file,temp,'\n');
        std::istringstream iss(temp);
        iss >> key;
        iss >> val;
        M[key] = val; 
    }
    return M;
}

void validateParams(map<string, float>& M) {
    const char *keys[] = {"gamma", "alpha", "epsilon", "mode", "display", "num_rows", "num_cols", "num_colors"};
    vector<string> v(keys,keys+8);
    map<string, float>::iterator it;
    REP(i,sz(v)) {
        assert(M.find(v[i]) != M.end());
    }
}

void displayParams(map<string, float>& M) {
    map<string, float>::iterator it;
    cout<<"Params for this experiment are \n";
    for(it = M.begin();it != M.end();it++) cout<<it->first<<" "<<it->second<<endl;
}

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " rom_file param_file"<<std::endl;
        return 1;
    }

    map<string,float> Params = parseParams(argv[2]);
    validateParams(Params);
    displayParams(Params);
    
    const int display = (int) Params["display"];
    const float gamma = Params["gamma"];
    const int epsilon = (int) Params["epsilon"];
    const string mode = (int) Params["mode"] ? "A" : "B";
    const int numStepsPerAction = Params.find("num_steps") == Params.end() ? 1 : (int) Params["num_steps"];
    const int numRows = (int) Params["num_rows"];
    const int numCols = (int) Params["num_cols"];
    const int numColors = (int) Params["num_colors"];
    const int numRuns = Params.find("num_runs") == Params.end() ? 1 : (int) Params["num_runs"];
    const float alpha = Params["alpha"] / (float) (numRows * numCols * numColors);

    ALEInterface ale(display);

    // Load the ROM file
    ale.loadROM(argv[1],mode);

    // Get the vector of minimal actions
    ActionVect actions = ale.getMinimalActionSet();

    cout<<"Running Sarsa trajectories\n";
    SarsaLearner(ale, gamma, epsilon, alpha, numStepsPerAction,
                 numRows, numCols, numColors, numRuns);

    return 0;
}
