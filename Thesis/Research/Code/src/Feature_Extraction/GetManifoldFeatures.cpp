/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Learn a low dim manifold, and a translation operator
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cassert>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>
/* DSYEVR prototype */
extern "C" void dsyevr_( char* jobz, char* range, char* uplo, int* n, double* a,
                int* lda, double* vl, double* vu, int* il, int* iu, double* abstol,
                int* m, double* w, double* z, int* ldz, int* isuppz, double* work,
                int* lwork, int* iwork, int* liwork, int* info );

using namespace std;

#define NSELECT 100

int exists(vector<ALEState>& v, ALEState& state) {
    for(vector<ALEState>::iterator it = v.end()-1;it!=v.begin()-1;it--) if((*it).equals(state)) return (it - v.begin());
    return -1;
}

void generateBFS(vector<VI >& rel, ALEInterface& ale, map<PII, int>& M, ActionVect actions, const int max_states, const int numStepsPerAction) {
    OSystem* sys = (ale.theOSystem).get();
    RomSettings* rom = (ale.settings.get());
    string md5 = sys->console().properties().get(Cartridge_MD5);
    ALEState state = (ale.environment)->getState();
    ALEState start_state = state.save(sys,rom,md5);
    queue<ALEState> Q;
    Q.push(start_state);
    vector<ALEState> v;
    v.pb(start_state);
    int prev = 0;
    while(!Q.empty() && !ale.game_over() && sz(rel) < max_states) {
        ALEState current = Q.front();
        ALEState state = (ale.environment)->getState();
        state.load(sys,rom,md5,current);
        REP(j,sz(actions)) {
            ale.saveState();
            REP(i,numStepsPerAction) ale.act(actions[j]);
            ALEState state = (ale.environment)->getState();
            ALEState next_state = state.save(sys,rom,md5);
            int present = exists(v,next_state);
            if(present == -1) {
                v.pb(next_state);
                Q.push(next_state);
                int index = sz(rel);
                rel[prev].pb(index);
                VI p;
                p.pb(prev);
                rel.pb(p);
                M[make_pair(prev,j)] = index;
            }
            else {
                if(!binary_search(all(rel[prev]),present)) { // Order of insertion is increasing
                    rel[prev].pb(present);
                    if(present != prev) rel[present].pb(prev);
                }
                M[make_pair(prev,j)] = present;
            }
            ale.loadState();
        }
        Q.pop();
        if(sz(rel) > prev + 1 ) prev++;
    }
}

void learnTranslationOp(map<PII, int>& M, vector<vector<float> >& Dist, vector<vector<float> >& Op) {
    map<PII, int>::iterator it;
    int start_state, action, end_state;
    map<int,int> Num_Actions;
    int num_dim = sz(Dist[0]);
    for(it = M.begin();it!=M.end();it++) {
        start_state = (it->first).first;
        action = (it->first).second;
        end_state = it->second;
        REP(k,num_dim) Op[action][k] += (Dist[end_state][k] - Dist[start_state][k]); 
        Num_Actions[action]++;
    } 
    REP(i,sz(Num_Actions)) REP(k,num_dim) Op[i][k] /= Num_Actions[i];
}

vector<vector<float> > learnManifold(ALEInterface& ale, ActionVect actions, const int max_states, const int numStepsPerAction) {

    vector<VI > rel; // adjacency list of states
    VI empty;
    rel.pb(empty);
    map<PII, int> M;
    generateBFS(rel,ale,M,actions,max_states,numStepsPerAction);
    int nV = sz(rel);
    cout<<"# of unique states "<<nV<<endl;
    
    // Compute all-pairs shortest path
    double D[nV][nV];
    REP(i,nV) REP(j,nV) D[i][j] = INF, D[i][i] = 0;
    REP(i,nV) REP(j,sz(rel[i])) if(rel[i][j] != i) D[i][rel[i][j]] = 1;

    REP(k,nV) {
        REP(i,nV) {
            REP(j,nV) {
                D[i][j] = min(D[i][j],D[i][k]+D[k][j]);
            }
        }
    }

    // Apply Classical MDS on D^2
    REP(i,nV) REP(j,nV) D[i][j] = D[i][j]*D[i][j];

    // Center D^2 matrix
    double rowSum[nV], colSum[nV], totSum = 0;
    REP(i,nV) {
        rowSum[i] = 0;colSum[i] = 0;
        REP(j,nV) rowSum[i] += D[i][j], colSum[i] += D[j][i], totSum += D[i][j];
    } 
    REP(i,nV) REP(j,nV) D[i][j] = -(D[i][j] - rowSum[i]/nV - colSum[j]/nV + totSum/(nV*nV))/2;

    /* Store 2D matrix as 1D array */
    double a[nV*nV];
    REP(i,nV) REP(j,nV) a[i*nV+j] = D[i][j];

    // Get top 100 eigen vectors
    int n = nV, il, iu, m, lda = nV, ldz = nV, info, lwork, liwork;
    int LDZ = n;
    double abstol, vl, vu;
    int iwkopt;
    int* iwork;
    double wkopt;
    double* work;
    /* Local arrays */
    int isuppz[n];
    double w[n], z[LDZ*NSELECT];
    /* Negative abstol means using the default value */
    abstol = -1.0;
    /* Set il, iu to compute NSELECT largest eigenvalues */
    il = max(1, n -NSELECT + 1);
    iu = n;
    /* Query and allocate the optimal workspace */
    lwork = -1;
    liwork = -1;
    dsyevr_( "Vectors", "Indices", "Upper", &n, a, &lda, &vl, &vu, &il, &iu,
                    &abstol, &m, w, z, &ldz, isuppz, &wkopt, &lwork, &iwkopt, &liwork,
                    &info );
    lwork = (int)wkopt;
    work = (double*)malloc( lwork*sizeof(double) );
    liwork = iwkopt;
    iwork = (int*)malloc( liwork*sizeof(int) );
    /* Solve eigenproblem */
    dsyevr_( "Vectors", "Indices", "Upper", &n, a, &lda, &vl, &vu, &il, &iu,
                    &abstol, &m, w, z, &ldz, isuppz, work, &lwork, iwork, &liwork,
                    &info );
    free(work);
    free(iwork);
    /* Check for convergence */
    if( info > 0 ) {
        printf( "The algorithm failed to compute eigenvalues.\n" );
        exit( 1 );
    }

    // Pick only the top eigen vectors
    VI picked;
    double topEigenValue = w[m-1];
    picked.pb(m-1);
    for(int i=m-2;i>=0;i--) {
        if(w[i] < 0.1 * topEigenValue) {
            picked.pb(i);
            break;
        }
        else picked.pb(i);
    }
    int topK = sz(picked);
    cout<<"Reduced Dimensionality = "<<topK<<endl;
    vector<vector<float> > Reduced(nV,vector<float>(topK));
    REP(i,nV) {
        REP(j,topK) Reduced[i][j] = z[i+n*picked[j]] * sqrt(w[picked[j]]);
    }

    /* Learn a translation operator */
    int numActions = sz(actions);
    vector<vector<float> > translation(numActions,vector<float>(topK,0));
    learnTranslationOp(M,Reduced,translation);
    vector<vector<float> > Output(numActions+1,vector<float>(topK));
    REP(k,topK) Output[0][k] = Reduced[0][k];
    REP(i,numActions) REP(k,topK) Output[i+1][k] = translation[i][k];

    return Output;
}
