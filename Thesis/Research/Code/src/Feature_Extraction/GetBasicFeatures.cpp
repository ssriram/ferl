/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Extract Basic Color features from ALE Game Screen
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cassert>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>

using namespace std;


void getBasicFeatures(const ALEScreen &screen, const int numCols,
                      const int numRows, const int numColors, vector<int>& features) {
    int width = screen.width();
    int height = screen.height();
    int M = height % numRows ? numRows * (height / numRows + 1) : height;
    int N = width % numCols ? numCols * (width / numCols + 1) : width;
    int sizeR = M / numRows;
    int sizeC = N / numCols;

	int blockIndex = 0;

    // Tile M x N using numRows x numCols
    for(int r = 0;r < numRows;r++) {
        int startR = r*sizeR, endR = (r+1)*sizeR;
        for(int c = 0;c < numCols;c++) {
            vector<bool> hasColor(numColors, false);
            int startC = c*sizeC, endC = (c+1)*sizeC;
            for(int x = startC;x < endC;x++) {
                for(int y = startR;y < endR;y++) {
                    if(x < width && y < height) {
                        unsigned char pixel = screen.get(y,x);
                        hasColor[(pixel) >> 1] = true;
                    }
                }
            }

            for(int color = 0;color < numColors;color++) {
                if(hasColor[color]) features.pb(color + blockIndex);
            }
            blockIndex += numColors;
        }
    }
}

std::vector<unsigned int> expandBASICFeatures(std::vector<unsigned int> features, unsigned int numFeatures)
{
	std::vector<unsigned int> expandedFeatures(features);
	expandedFeatures.reserve(features.size() + features.size()*(features.size()-1));

	for(std::vector<unsigned int>::iterator it1=features.begin(); it1 != features.end(); it1++)
	{
		for(std::vector<unsigned int>::iterator it2=features.begin(); it2 != features.end(); it2++)
		{
			// Ensure that each expanded feature is added only once
			if((*it1) < (*it2))
			{
				unsigned int expandedIndex = ((*it1)+1)*numFeatures - (*it1)*((*it1)+1)/2 + (*it2) - (*it1) - 1;
				assert(expandedIndex < numFeatures*(numFeatures+1)/2);
				expandedFeatures.push_back(((*it1)+1)*numFeatures - (*it1)*((*it1)+1)/2 + (*it2) - (*it1) - 1);
			}
		}
	}
	
	return expandedFeatures;
}
