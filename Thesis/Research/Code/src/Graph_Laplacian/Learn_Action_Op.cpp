/*******************************************************************************
Author - Sriram Srinivasan
Purpose - Learn the linear action operator A, s.t A x_t = x_{t+1}
****************************************************************************** */

// Libs 
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string.h>
#define REP(i,n) for(int i=0;i<n;i++)
#define FOR(i,A,n) for(int i=A;i<n;i++)
#define sz(c) (signed int) c.size()
#define pb(c) push_back(c)
#define INF (int) 1e9
#define all(c) c.begin(),c.end()
#define GI(t) scanf("%d",&t)
#define VI vector<int>
#define PII pair <int,int>
typedef long long LL;

#include <ale_interface.hpp>
#include <iostream>
#include <shogun/preprocessor/PCA.h>
#include <shogun/features/DenseFeatures.h>
#include <shogun/base/init.h>
#include <shogun/lib/common.h>
#include <shogun/lib/SGMatrix.h>
#include <shogun/io/SGIO.h>
#include <shogun/io/AsciiFile.h>
using namespace std;
using namespace shogun;

// Functions
SGMatrix<float64_t> generateFeatures(ALEInterface& ale, ActionVect actions, int max_depth, int topK, const char *weights_file);
SGMatrix<float64_t> computeEmbedding(vector<VI >& rel, SGVector<float64_t>& eigenvalues, int topK);
map<PII, int> generateBFS(vector<pair<ALEState, int> >& v, vector<VI >& rel, queue<pair<ALEState,int> >& Q, ALEInterface& ale, ActionVect actions, int prev, int max_depth);
int exists(vector<pair<ALEState,int> >& v, ALEState state);
SGMatrix<float64_t> extractMatrix(vector<vector<float64_t> >& w, int start_row_index, int end_row_index, int num_cols);
vector<vector<float64_t> > getWeights(const char *fileName, int num_rows, int num_cols);
// End of Functions

// Shogun debug functions
void print_message(FILE* target, const char* str)
{
    cout<<str<<endl;
}

void print_warning(FILE* target, const char* str)
{
    fprintf(target, "%s", str);
    cout<<"in err\n";
}

void print_error(FILE* target, const char* str)
{
    fprintf(target, "%s", str);
}
// End of shogun debug functions

int main(int argc, char** argv) {
    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << " rom_file max_depth (>10) topK  weights_file"<<std::endl;
        return 1;
    }

    init_shogun(&print_message,&print_warning,&print_error);
    ALEInterface ale(1);

    // Load the ROM file
    ale.loadROM(argv[1]);

    // Get the vector of minimal actions
    ActionVect legal_actions = ale.getMinimalActionSet();

    // Set params
    int max_depth = atoi(argv[2]);
    int topK = atoi(argv[3]);

    // Generate BFS tree and compute low dim. embedding
    SGMatrix<float64_t> full_features = generateFeatures(ale,legal_actions,max_depth,topK,argv[4]);
    ofstream file;
    file.open("Embedding.txt");
    REP(i,full_features.num_rows) {REP(j,full_features.num_cols) {file<<full_features(i,j)<<" ";} file<<endl;}
    file.close();

    return 0;
}

SGMatrix<float64_t> generateFeatures(ALEInterface& ale, ActionVect actions, int max_depth, int topK, const char* weights_dir) {
    vector<pair<ALEState,int> > v; // list of states encountered so far
    vector<VI > rel; // adjacency list of states
    queue<pair<ALEState,int> >Q;
    OSystem* sys = (ale.theOSystem).get();
    RomSettings* rom = (ale.settings.get());
    string md5 = sys->console().properties().get(Cartridge_MD5);
    // Simulate game states upto certain depth
    int start_depth = 0;
    ALEState state = (ale.environment)->getState();
    ALEState start_state = state.save(sys,rom,md5);
    v.pb(make_pair(start_state,1));
    Q.push(make_pair(start_state,1));
    VI empty;
    rel.pb(empty);
    map<PII, int> M = generateBFS(v,rel,Q,ale,actions,0,max_depth);
    int nV = sz(rel);
    cout<<"# of unique states with full BFS = "<<nV<<endl;
    cout<<"size of map = "<<sz(M)<<endl;

    // compute embedding
    SGVector<float64_t> eigenvalues;
    SGMatrix<float64_t> embedding = computeEmbedding(rel, eigenvalues, topK);
/*

    // Training data consists of current states at depth k
    const int training_depth = 20;

    map<int,float64_t> normMap;
    map<int,int> numDepthStates;
    int index = 0;
    for(index = 0;v[index].second < training_depth;index++) ;
    for(;index<nV;index++) {
        float64_t norm = 0;
        REP(k,topK) norm += embedding(index,k) * embedding(index,k);
        normMap[v[index].second] += sqrt(norm);
        numDepthStates[v[index].second]++;
    }

    ofstream file1, file2;
    // Dump the data in the form X_t * w = X_{t+1}
    file1.open("Train_Cur_states.txt");
    file2.open("Train_Next_states.txt");
    map<PII, int>::iterator it = M.begin();
    int iter = 0;
    int last_depth = -1;
    for(iter=0;v[iter].second <= training_depth;iter++) {
        REP(j,topK) file1<<embedding(iter,j)<<" ";
        file1<<endl;
        REP(j,sz(actions)) {
            REP(k,topK) {
                file2<<embedding(M[make_pair(iter,j)],k)<<" ";
            }
            file2<<endl;
        }
        if(v[iter].second == training_depth) if(last_depth == -1) last_depth = iter;
    }
    file1.close();
    file2.close();

    char weights_file[500], b_file[500];
    strcpy(weights_file,weights_dir);
    strcat(weights_file,"/weights_10.txt");
    strcpy(b_file,weights_dir);
    strcat(b_file,"/b.txt");

    // Get the weights learned from linear regression
    vector<vector<float64_t> >w = getWeights(weights_file, sz(actions)*topK,topK);
    vector<vector<float64_t> >b = getWeights(b_file, sz(actions),topK);
    REP(i,sz(b)) {REP(k,topK) {cout<<b[i][k]<<" ";}cout<<endl;}
    
    // Compute the accumulated test loss, by applying the linear operator recursively
    map<int,vector<float64_t> >stateMap;
    for(iter=last_depth;v[iter].second == training_depth;iter++) {
        vector<float64_t> temp(topK);
        REP(k,topK) temp[k] = embedding(iter,k);
        stateMap[iter] = temp;
    }

    file1.open("prediction.txt");
    file2.open("actual.txt");
    int num_actions = sz(actions);
    float64_t cum_error = 0;
    map<int,float64_t> depthError;
    map<int,int> numStates;
    int prev_depth = -1;
    for(it=M.begin();v[(it->first).first].second < training_depth;it++) ;
    for(;it!=M.end();it++) {
        PII p = it->first;
        int cur_state = p.first;
        int action = p.second;
        // Extract the sub-matrix from w
        SGMatrix<float64_t> op = extractMatrix(w,action*num_actions,(action+1)*num_actions-1,topK);

        // Apply operator - embedding(index) * op
        vector<float64_t> next_state(topK);
        vector<float64_t> cur_embedding = stateMap[cur_state];
        REP(k,topK) {
            next_state[k] = 0;
            REP(l,op.num_rows) next_state[k] += cur_embedding[k] * op(l,k);
            next_state[k] += b[action][k];
        }
        if(prev_depth < v[(it->first).first].second) {
            prev_depth = v[(it->first).first].second;
        }
        REP(k,topK) file1<<next_state[k]<<" ";
        file1<<endl;
        REP(k,topK) file2<<embedding(M[p],k)<<" ";
        file2<<endl;

        // store the embedding of next state
        stateMap[M[p]] = next_state;
    
        // Compare the error between predicted state and the embedding computed using graph laplacian
        float64_t error = 0;
        REP(k,topK) error += (next_state[k] - embedding(M[p],k)) * (next_state[k] - embedding(M[p],k));
        error = sqrt(error);

        // Compute cumulative error by depth
        depthError[v[it->second].second] += error;
        numStates[v[it->second].second]++;
    }
    file1.close();
    file2.close();

    // Dump error by depth
    file1.open("Cum_Error.txt");
    map<int,float64_t>::iterator it2 = depthError.begin();
    map<int,int>::iterator numIt = numStates.begin();
    float64_t prev_error = 0;
    for(;it2!=depthError.end() && numIt != numStates.end();it2++,numIt++) {
        file1<<it2->first<<" "<<it2->second/numIt->second<<" "<<normMap[it2->first]/numDepthStates[it2->first]<<endl;
    }
    file1.close();
*/

    return embedding;
}

SGMatrix<float64_t> computeEmbedding(vector<VI >& rel, SGVector<float64_t>& eigenvalues, int topK) {
    // Compute degree of each vertex
    int nV = sz(rel);
    VI D(nV,0);
    REP(i,nV) D[i] = sz(rel[i]);
    // Construct laplacian matrix and compute eigen vectors
    SGMatrix<float64_t> lap(nV,nV);
    REP(i,nV) {
        lap(i,i) = 1;
        REP(j,sz(rel[i])) {
            lap(i,rel[i][j]) = -1/(double)sqrt(D[i]*D[rel[i][j]]);
        }
    }
    eigenvalues = SGMatrix<float64_t>::compute_eigenvectors(lap);
    SGVector<float64_t>::display_vector(eigenvalues);

    // Pick top K eigen vectors, starting from the second
    SGMatrix<float64_t> embedding(nV,topK);
    REP(i,nV) REP(j,topK) embedding(i,j) = lap(i,j+1);
    
    return embedding;
}

int exists(vector<pair<ALEState,int> >& v, ALEState state) {
    for(vector<pair<ALEState,int> >::iterator it = v.end()-1;it!=v.begin()-1;it--) if((it->first).equals(state)) return (it - v.begin());
    return -1;
}

map<PII, int> generateBFS(vector<pair<ALEState, int> >& v, vector<VI >& rel, queue<pair<ALEState,int> >& Q, ALEInterface& ale, ActionVect actions, int prev, int max_depth) {
    OSystem* sys = (ale.theOSystem).get();
    RomSettings* rom = (ale.settings.get());
    string md5 = sys->console().properties().get(Cartridge_MD5);
    ALEState state = (ale.environment)->getState();
    ALEState start_state = state.save(sys,rom,md5);
    map<PII, int> M;
    while(!Q.empty() && !ale.game_over()) {
        ALEState current = Q.front().first;
        int depth = Q.front().second;
        ALEState state = (ale.environment)->getState();
        state.load(sys,rom,md5,current);
        REP(j,sz(actions)) {
            ale.saveState();
            ale.act(actions[j]);
            ALEState state = (ale.environment)->getState();
            ALEState next_state = state.save(sys,rom,md5);
            int present = exists(v,next_state);
            cout<<"prev = "<<prev<<", action = "<<j<<", present = "<<present<<endl;
            if(present == -1) {
                if(depth + 1 <= max_depth) {
                    v.pb(make_pair(next_state,depth+1));
                    Q.push(make_pair(next_state,depth+1));
                    int index = sz(rel);
                    rel[prev].pb(index);
                    VI p;
                    p.pb(prev);
                    rel.pb(p);
                    M[make_pair(prev,j)] = index;
                }
            }
            else {
                if(!binary_search(all(rel[present]),prev)) { // Order of insertion is increasing
                    rel[present].pb(prev);
                    rel[prev].pb(present);
                }
                M[make_pair(prev,j)] = present;
            }
            ale.loadState();
        }
        Q.pop();
        prev++;
    }
    return M;
}

SGMatrix<float64_t> extractMatrix(vector<vector<float64_t> >& w, int start_row_index, int end_row_index, int num_cols) {
    SGMatrix<float64_t> temp(end_row_index - start_row_index + 1, num_cols);
    int ri = 0;
    for(int i=start_row_index;i<=end_row_index;i++) {
        REP(j,num_cols) temp(ri,j) = w[i][j];
        ri++;    
    }
    return temp;
}

vector<vector<float64_t> > getWeights(const char *fileName, int num_rows, int num_cols) {
    vector<vector<float64_t> > v(num_rows,vector<float64_t>(num_cols));
    ifstream file (fileName);
    if(file.is_open()) {
        int lineNum = 0;
        string temp;
        char line[500];
        while(!file.eof()) {
            getline(file,temp,'\n');
            strcpy(line,temp.c_str());
            char *p;
            p = strtok(line," ");
            int k = 0;
            while(p!=NULL) {
                v[lineNum][k++] = atof(p);
                p = strtok(NULL," ");
            }
            lineNum++;
        }
    }
    return v;
}
