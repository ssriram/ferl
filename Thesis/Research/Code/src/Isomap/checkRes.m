function [] = checkRes(dirName,r,c)
screen = [];
for i=100:100
    fname = strcat(dirName,'/screen',num2str(i),'.txt');
    A = importdata(fname);
    Ares = imresize(A,[r,c]);
    image(Ares)
end
end