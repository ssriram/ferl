function [mY] = runIsomapAtari(dirName)
screen = [];
for i=0:900
    fname = strcat(dirName,'/screen',num2str(i),'.txt');
    A = importdata(fname);
    %Ares = imresize(A,[55,40]);
    screen = cat(2,screen,A(:));
end
global X;
X = zscore(screen);
[Y R] = IsomapII('dfun','k',10)
mY = Y.coords{3}';
figure;
plot3(mY(:,1),mY(:,2),mY(:,3),'b-');

end
