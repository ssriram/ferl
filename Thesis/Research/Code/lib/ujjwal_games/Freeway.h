#include <vector>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <unordered_set>
#include <stack>
#include <iostream>

struct state {
    bool savedActive;
    int savedCurrTime;
    int savedAgentLocation;
    int savedLives;
    std::vector<int> savedCarLocations;
    std::vector<bool> savedCarDirections;
    public:
    state(bool active,int currTime,int agentLocation,int lives,
            std::vector<int>& carLocations,std::vector<bool>& carDirections) {
        savedActive = active;
        savedCurrTime = currTime;
        savedAgentLocation = agentLocation;
        savedLives = lives;
        savedCarLocations = carLocations;
        savedCarDirections = carDirections;
    }

    bool equals(state& st) {
        return  this->savedActive == st.savedActive && 
                this->savedCurrTime == st.savedCurrTime &&
                this->savedLives == st.savedLives &&
                this->savedAgentLocation == st.savedAgentLocation &&
                this->savedCarLocations == st.savedCarLocations &&
                this->savedCarDirections == st.savedCarDirections ;
    }
};

class Freeway
{
    static const int height = 16;
    static const int width = 16;
    int featureType;
    int proximity;
    bool active;
    int currTime;
    static const int numColours = 3;
    static const int numActions = 3;
    static const int maxEpisodeLength = 1024;
    static const int agentColumn = 4;
    int agentLocation;
    std::vector<std::vector<int> > screen;
    std::vector<int> carLocations;
    std::vector<bool> carDirections;
    int lives;

    /* Possible screen values:
     * 0: Blank
     * 1: Agent
     * 2: Car
     */
    
    enum colour
    {
        COLOUR_BLANK = 0,
        COLOUR_AGENT,
        COLOUR_CAR
    };

    std::stack<state> s;
    
    public:

    Freeway(int featureType, int proximity) : featureType(featureType), proximity(proximity), active(false), currTime(0), agentLocation(0), screen(height, std::vector<int>(width, 0)), carLocations(height-1, 0), carDirections(height-1, false), lives(0)
    {
    }
    
    bool isActive()
    {
        return active;
    }

    std::vector<std::vector<int> > getScreen()
    {
        return screen;
    }
    
    void generateScreen()
    {
        screen = std::vector<std::vector<int> >(height, std::vector<int>(width, 0));
        
        for(int i=0; i < height-1; i++)
        {
            assert(carLocations[i] >= 0);
            assert(carLocations[i] < width);
            screen[i][carLocations[i]] = COLOUR_CAR;
        }
        
        assert(agentLocation < width);
        assert(agentLocation >= 0);
        
        screen[agentLocation][agentColumn] = COLOUR_AGENT;
    }
    
    void reset()
    {
        for(int i=0; i < height-1; i++)
        {
            carLocations[i] = rand()%width;
            carDirections[i] = rand()%2;
        }
        
        agentLocation = height-1;
        currTime = 0;
        lives = 3;
        //generateScreen();
        active = true;
    }
    
    float processAction(int action)
    {
        /* Possible actions:
         * 0: Do nothing
         * 1: Up
         * 2: Down
         */
        
        if(!active)
            return 0;
        
        assert(lives > 0);
        float reward = 0;
        bool collide = false;
        
        switch(action)
        {
            case 0:
                break;
                
            case 1:
                if(agentLocation > 0)
                {
                    agentLocation--;
                }
                else
                {
                    agentLocation = height-1;
                    reward = 1;
                }
                
                break;
                
            case 2:
                if(agentLocation < height-1)
                {
                    agentLocation++;
                }
                
                break;
                
            default:
                assert(0);
        }
        
        for(int i=0; i < height-1; i++)
        {
            if(carDirections[i])
            {
                if(carLocations[i] < width-1)
                {
                    carLocations[i]++;
                }
                else
                {
                    carLocations[i] = 0;
                }
            }
            else
            {
                if(carLocations[i] > 0)
                {
                    carLocations[i]--;
                }
                else
                {
                    carLocations[i] = width-1;
                }
            }
            
            if(i == agentLocation && carLocations[i] == agentColumn)
            {
                assert(!collide);
                collide = true;
            }
        }
        
        if(collide)
        {
            lives--;
            agentLocation = height-1;
        }
        
        currTime++;
        
        if(lives == 0 || currTime >= maxEpisodeLength)
        {
            active = false;
        }
        
        //generateScreen();
        return reward;
    }

    int getNumActions()
    {
        return numActions;
    }

    int getAgentLocation() {
        return agentLocation;
    }

    void saveState() {
        state st(active,currTime,agentLocation,lives,carLocations,carDirections);
        s.push(st);
    }

    void restoreState() {
        assert(s.size() > 0);
        state st = s.top();
        loadState(st);
        s.pop();
    }

    state getState() {
        state st(active,currTime,agentLocation,lives,carLocations,carDirections);
        return st;
    }

    void loadState(state st) {
        currTime = st.savedCurrTime;
        active = st.savedActive;
        agentLocation = st.savedAgentLocation;
        lives = st.savedLives;
        carLocations = st.savedCarLocations;
        carDirections = st.savedCarDirections;
    }

    void printState() {
        std::cout<<"active = "<<active<<std::endl;
        std::cout<<"currTime = "<<currTime<<std::endl;
        std::cout<<"lives = "<<lives<<std::endl;
        std::cout<<"agent Loc = "<<agentLocation<<std::endl;
        std::cout<<"Car Locations and Directions = \n";
        for(uint i=0;i<carLocations.size();i++) std::cout<<carLocations[i]<<" "<<carDirections[i]<<std::endl;
    }
    
    std::unordered_set<int> getFeatures()
    {
        if(featureType == 3)
        {
            return getPairwiseLogarithmicRelativeFeatures();
        }
        else if(featureType == 2)
        {
            return getPairwiseRelativeFeatures();
        }
        else if(featureType == 1)
        {
            return getRelativeFeatures();
        }
        else
        {
            return getAbsoluteFeatures();
        }
    }
    
    int getNumFeatures()
    {
        if(featureType == 3)
        {
            return 4*(floor(log2(height-1))+2)*(floor(log2(width-1))+2)*(numColours-1)*(numColours)/2;
        }
        else if(featureType == 2)
        {
            return (2*proximity-1)*(2*proximity-1)*(numColours-1)*(numColours)/2;
        }
        else if(featureType == 1)
        {
            return (2*proximity-1)*(2*proximity-1)*(numColours-1);
        }
        else
        {
            return height*width*(numColours-1);
        }
    }
    
    std::unordered_set<int> getAbsoluteFeatures()
    {
        std::unordered_set<int> features;
        
        for(int i=0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                if(screen[i][j] > 0)
                {
                    features.insert((screen[i][j]-1)*height*width + i*width + j);
                }
            }
        }
        
        return features;
    }
    
    std::unordered_set<int> getRelativeFeatures()
    {
        std::unordered_set<int> features;
        
        for(int i = 1-proximity; i < proximity; i++)
        {
            if(i+height-1 < 0 || i+height-1 >= height)
            {
                continue;
            }
            
            for(int j = 1-proximity; j < proximity; j++)
            {
                if(j+agentLocation < 0 || j+agentLocation >= width || screen[i+height-1][j+agentLocation] == 0)
                {
                    continue;
                }
                
                features.insert((screen[i+height-1][j+agentLocation]-1)*(2*proximity-1)*(2*proximity-1) + (i+proximity-1)*(2*proximity-1) + (j+proximity-1));
            }
        }
        
        return features;
    }
    
    std::unordered_set<int> getPairwiseRelativeFeatures()
    {
        std::unordered_set<int> features;
        
        for(int i = 0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                for(int k = std::max(0, i-proximity+1); k < std::min(int(height), i+proximity); k++)
                {              
                    for(int l = std::max(0, j-proximity+1); l < std::min(int(width), j+proximity); l++)
                    {
                        if(screen[i][j] == 0 || screen[i][j] > screen[k][l])
                        {
                            continue;
                        }
                        
                        int featureIndex = ((screen[i][j]-1)*(numColours-1)-((screen[i][j]-1)*(screen[i][j]-2))/2+screen[k][l]-screen[i][j])*(2*proximity-1)*(2*proximity-1) + (k-i+proximity-1)*(2*proximity-1) + (l-j+proximity-1);
                        
                        if(features.find(featureIndex) == features.end())
                        {
                            features.insert(featureIndex);
                        }
                    }
                }
            }
        }
        
        return features;
    }
    
    std::unordered_set<int> getPairwiseLogarithmicRelativeFeatures()
    {
        std::unordered_set<int> features;
        
        
        for(int i = 0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                for(int k = 0; k < height; k++)
                {              
                    for(int l = 0; l < width; l++)
                    {
                        if(screen[i][j] == 0 || screen[i][j] > screen[k][l])
                        {
                            continue;
                        }
                        
                        int quadrant = (k>i)*2 + (l>j);
                        int logX;
                        int logY;
                        
                        if(k==i)
                        {
                            logX = 0;
                        }
                        else
                        {
                            logX = floor(log2(abs(k-i)))+1;
                        }
                        
                        if(l==j)
                        {
                            logY = 0;
                        }
                        else
                        {
                            logY = floor(log2(abs(l-j)))+1;
                        }
                        
                        int featureIndex = ((screen[i][j]-1)*(numColours-1)-((screen[i][j]-1)*(screen[i][j]-2))/2+screen[k][l]-screen[i][j])*4*(floor(log2(height-1))+2)*(floor(log2(width-1))+2) + quadrant*(floor(log2(height-1))+2)*(floor(log2(width-1))+2) + logX*(floor(log2(width-1))+2) + logY;
                        
                        if(features.find(featureIndex) == features.end())
                        {
                            features.insert(featureIndex);
                        }
                    }
                }
            }
        }
        
        return features;
    }
};

std::unordered_set<int> expandFeatures(std::unordered_set<int>& features, int size)
{
    std::unordered_set<int> expandedFeatures = features;
    for(auto it1 = features.begin(); it1 != features.end(); it1++)
    {
        auto it2 = it1;
        it2++;
        
        for(; it2 != features.end(); it2++)
        {
            int minF = std::min(*it1, *it2);
            int maxF = std::max(*it1, *it2);
            expandedFeatures.insert(size + size*minF - minF*(minF+1)/2 + maxF - minF - 1);
        }
    }
    
    return expandedFeatures;
}
