#include <vector>
#include <cstdlib>
#include <cassert>
#include <unordered_set>
#include <cmath>

#ifndef PII
    #define PII pair<int,int>
#endif

using namespace std;

struct state {
    bool savedActive;
    bool savedDir;
    int savedCurrTime;
    int savedDiversCollected;
    PII savedAgentLocation;
    vector<pair<PII, bool> > savedDiverLocations;
    vector<pair<PII, bool> > savedFishLocations;

    public:
    state(bool active,bool dir,int currTime,
          int diversCollected,
          PII agentLocation,
          vector<pair<PII, bool> > diverLocations,
          vector<pair<PII, bool> > fishLocations) {
        savedActive = active;
        savedDir = dir;
        savedCurrTime = currTime;
        savedDiversCollected = diversCollected;
        savedAgentLocation = agentLocation;
        savedDiverLocations = diverLocations;
        savedFishLocations = fishLocations;
    }

    bool equals(state& st) {
        return this->savedActive == st.savedActive &&
               this->savedDir == st.savedDir &&
               this->savedCurrTime == st.savedCurrTime &&
               this->savedDiversCollected == st.savedDiversCollected &&
               this->savedAgentLocation == st.savedAgentLocation &&
               this->savedDiverLocations == st.savedDiverLocations &&
               this->savedFishLocations == st.savedFishLocations;
    }
};

class Seaquest
{
    static const int numActions = 5;
    enum actions 
    {
        ACTION_NULL = 0,
        ACTION_UP,
        ACTION_RIGHT,
        ACTION_DOWN,
        ACTION_LEFT
    };
    static const int maxDivers = 1;
    static const int width = 16;
    static const int height = 16;
    static const int maxEpisodeLength = 256;
    static const int diverProb = 15;
    int featureType;
    int proximity;
    bool active;
    int diversCollected;
    int currTime;
    bool dir;
    int dropPoint;
    pair<int, int> agentLocation;
    vector<pair<pair<int, int >, bool> > diverLocations;
    vector<pair<pair<int, int >, bool> > fishLocations;

    stack<state> s;

    public:
    Seaquest(int featureType, int proximity) : featureType(featureType), proximity(proximity), active(false), diversCollected(0), currTime(0), dropPoint(0), agentLocation(make_pair(0,0))
    {
    }

    bool isActive()
    {
        return active;
    }

    void reset()
    {
        agentLocation.first = 0;
        agentLocation.second = 1 + (width-2) / 3;
        diverLocations.clear();
        fishLocations.clear();
        diversCollected = 0;
        currTime = 0;
        dropPoint = height-1;
        active = true;
        dir = 1;
        generateFish();
    }

    void generateFish() {
        for(int location=1;location < height-1;location += 4) {
            int col = rand() % (width - 1);
            fishLocations.push_back(make_pair(make_pair(location, col), 1));
        }
    }

    void generateDiver(bool direction)
    {
        int location = 1 + (height-1)/3;
        diverLocations.push_back(make_pair(make_pair(location, (!direction)*(width-1)), direction));
    }
    
    vector<pair<pair<int, int>, bool> > pruneList(vector<pair<pair<int, int>, bool> >& list)
    {
        vector<pair<pair<int, int>, bool> > newList;
        
        for(uint i=0; i < list.size(); i++)
        {
            if(list[i].first.second != -1)
            {
                assert(list[i].first.first > 0);
                assert(list[i].first.first < height);
                assert(list[i].first.second >= 0);
                assert(list[i].first.second < width);
                newList.push_back(list[i]);
            }
        }
        
        return newList;
    }
    
    double processAction(int action)
    {
        if(!active)
        {
            return 0;
        }
        
        assert(agentLocation.first >= 0);
        assert(agentLocation.first < height);
        assert(agentLocation.second > 0);
        assert(agentLocation.second < width - 1);
        
        switch(action)
        {
            case ACTION_NULL:
                break;
            case ACTION_UP:
                if(agentLocation.first > 0)
                    agentLocation.first--;
                break;
            case ACTION_RIGHT:
                if(agentLocation.second < width-2)
                    agentLocation.second++;
                break;
            case ACTION_DOWN:
                if(agentLocation.first < height-1)
                    agentLocation.first++;
                break;
            case ACTION_LEFT:
                if(agentLocation.second > 1)
                    agentLocation.second--;
                break;
            default:
                assert(0);
        }
        
        double reward = 0;
        
        if(agentLocation.first == dropPoint)
        {
            if(diversCollected == maxDivers)
            {
                reward = 1;
                diversCollected = 0;
            }
            else
            {
                active = false;
            }
        }

        for(uint i=0; i < diverLocations.size(); i++)
        {
            assert(diverLocations[i].first.first > 0);
            assert(diverLocations[i].first.first < height);
            assert(diverLocations[i].first.second >= 0);
            assert(diverLocations[i].first.second < width);
            
            if(diverLocations[i].second)
            {
                diverLocations[i].first.second++;
                
                if(diverLocations[i].first.second >= width)
                {
                    diverLocations[i].first.second = -1;
                }
                else
                {
                    if( (diverLocations[i].first.first == agentLocation.first && diverLocations[i].first.second == agentLocation.second) ||
                        (action == ACTION_LEFT && (diverLocations[i].first.first == agentLocation.first && diverLocations[i].first.second-1 == agentLocation.second))
                    )
                    {
                        if(diversCollected < maxDivers)
                        {
                            diversCollected++;
                            diverLocations[i].first.second = -1;
                        }
                    }
                }
            }
            else
            {
                diverLocations[i].first.second--;
                
                if(diverLocations[i].first.second < 0)
                {
                    diverLocations[i].first.second = -1;
                }
                else
                {
                    if( (diverLocations[i].first.first == agentLocation.first && diverLocations[i].first.second == agentLocation.second) ||
                        (action == ACTION_RIGHT && (diverLocations[i].first.first == agentLocation.first && diverLocations[i].first.second == agentLocation.second-1))
                    )
                    {
                        if(diversCollected < maxDivers)
                        {
                            diversCollected++;
                            diverLocations[i].first.second = -1;
                        }
                    }
                }
            }
        }
        
        for(uint i=0; i < fishLocations.size(); i++)
        {
            assert(fishLocations[i].first.first > 0);
            assert(fishLocations[i].first.first < height);
            assert(fishLocations[i].first.second >= 0);
            assert(fishLocations[i].first.second < width);
            
            if(fishLocations[i].second)
            {
                fishLocations[i].first.second++;
                
                if(fishLocations[i].first.second >= width)
                {
                    fishLocations[i].first.second = 0;
                }
                else
                {
                    if( (fishLocations[i].first.first == agentLocation.first && fishLocations[i].first.second == agentLocation.second) ||
                        (action == ACTION_LEFT && (fishLocations[i].first.first == agentLocation.first && fishLocations[i].first.second-1 == agentLocation.second))
                    )
                    {
                        active = false;
                    }
                }
            }
            else
            {
                fishLocations[i].first.second--;
                
                if(fishLocations[i].first.second < 0)
                {
                    fishLocations[i].first.second = width - 1;
                }
                else
                {
                    if( (fishLocations[i].first.first == agentLocation.first && fishLocations[i].first.second == agentLocation.second) ||
                        (action == ACTION_RIGHT && (fishLocations[i].first.first == agentLocation.first && fishLocations[i].first.second == agentLocation.second-1))
                    )
                    {
                        active = false;
                    }
                }
            }
        }
        
        diverLocations = pruneList(diverLocations);
        currTime++;
        
        if(currTime == maxEpisodeLength)
        {
            active = false;
        }
        
        if(sz(diverLocations) == 0) {
            generateDiver(dir);
            dir = !dir;
        }

        return reward;
    }
    
    int getNumActions()
    {
        return numActions;
    }

    void saveState() {
        state st(active,dir,currTime,
                 diversCollected,agentLocation,
                 diverLocations,fishLocations);
        s.push(st);
    }

    void restoreState() {
        assert(s.size() > 0);
        state st = s.top();
        loadState(st);
        s.pop();
    }

    state getState() {  
        state st(active,dir,currTime,
                 diversCollected,agentLocation,
                 diverLocations,fishLocations);
        return st;
    }

    void loadState(state& st) {
        active = st.savedActive;
        dir = st.savedDir;
        currTime = st.savedCurrTime;
        diversCollected = st.savedDiversCollected;
        agentLocation = st.savedAgentLocation;
        diverLocations = st.savedDiverLocations;
        fishLocations = st.savedFishLocations;    
    }

    void printState() {
        cout<<"active = "<<active<<endl;
        cout<<"currTime = "<<currTime<<endl;
        cout<<"agent Loc = "<<agentLocation.first<<" "<<agentLocation.second<<endl;
        cout<<"#divers = "<<diverLocations.size()<<endl;
        cout<<"#divers collected = "<<diversCollected<<endl;
    }
    
};
