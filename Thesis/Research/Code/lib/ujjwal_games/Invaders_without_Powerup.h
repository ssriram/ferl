#include <vector>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <unordered_set>

#define PII pair<int,int>
#define VI vector<int>

using namespace std;

struct state {
    bool savedActive;
    bool savedDir;
    bool savedAgentBulletFired;
    int savedCurrTime;
    int savedAgentLocation;
    vector<PII > savedMonsterLocations;
    vector<PII > savedAgentBulletLocations;
    vector<PII > savedEnemyBulletLocations;
    public:
    state(bool active,bool dir,bool agentBulletFired,
          int currTime,int agentLocation,
          vector<PII >& monsterLocations,vector<PII >& agentBulletLocations,
          vector<PII >& enemyBulletLocations) {
        savedActive = active;
        savedDir = dir;
        savedAgentBulletFired = agentBulletFired;
        savedCurrTime = currTime;
        savedAgentLocation = agentLocation;
        savedMonsterLocations = monsterLocations;
        savedAgentBulletLocations = agentBulletLocations;
        savedEnemyBulletLocations = enemyBulletLocations;
    }

    bool equals(state& st) {
        return  this->savedActive == st.savedActive &&
                this->savedDir == st.savedDir &&
                this->savedAgentBulletFired == st.savedAgentBulletFired &&
                this->savedCurrTime == st.savedCurrTime &&
                this->savedAgentLocation == st.savedAgentLocation &&
                this->savedMonsterLocations == st.savedMonsterLocations &&
                this->savedAgentBulletLocations == st.savedAgentBulletLocations &&
                this->savedEnemyBulletLocations == st.savedEnemyBulletLocations;
    }

    void print() {
        cout<<savedActive<<" "<<savedDir<<endl;
    }
};

class Invaders
{
    static const int height = 16;
    static const int width = 16;
    int featureType;
    int proximity;
    bool active;
    bool dir;
    int currTime;
    bool agentBulletFired;
    static const int numColours = 6;
    static const int numActions = 6;
    static const int monsterGridSize = 4;
    static const int startingMonsterRow = 1;
    static const int startingMonsterColumn = 4;
    static const int fireRestTime = 8;
    static const int monsterFireTime = 4;
    static const int maxEpisodeLength = 256;
    
    int agentLocation;
    vector<pair<int, int> > monsterLocations;
    vector<pair<int, int> > agentBulletLocations;
    vector<pair<int, int> > enemyBulletLocations;

    stack<state> s;
    
    /* Possible screen values:
     * 0: Blank
     * 1: Agent
     * 2: Monster
     * 3: Bullet
     * 4: Power Up
     * 5: Powered Agent
     */
    
    enum colour
    {
        COLOUR_BLANK = 0,
        COLOUR_AGENT,
        COLOUR_MONSTER,
        COLOUR_BULLET,
        COLOUR_POWER_UP,
        COLOUR_POWERED_AGENT
    };
    
    public:
    Invaders(int featureType, int proximity) : featureType(featureType), proximity(proximity), active(false), dir(true), currTime(0), agentBulletFired(false), agentLocation(0)
    {
    }
    
    bool isActive()
    {
        return active;
    }
    
    void monsterFire()
    {
        if(monsterLocations.size() == 0)
        {
            return;
        }
        
        for(uint i=0;i<monsterLocations.size();i++) {
            enemyBulletLocations.push_back(make_pair(monsterLocations[i].first+1, monsterLocations[i].second));
        }
    }
    
    void agentFire()
    {
        agentBulletLocations.push_back(make_pair(height-2, agentLocation));
    }
    
    void reset()
    {
        monsterLocations = vector<pair<int, int> >(0);
        agentBulletLocations = vector<pair<int, int> >(0);
        enemyBulletLocations = vector<pair<int, int> >(0);
        for(int i=0; i < monsterGridSize; i++)
        {
            for(int j=0; j < monsterGridSize; j++)
            {
                pair<int, int> monsterLocation = make_pair(startingMonsterRow + i*2, startingMonsterColumn + j*2);
                assert(monsterLocation.first < height - 1);
                assert(monsterLocation.second < width);
                assert(monsterLocation.first >= 0);
                assert(monsterLocation.second >= 0);
                monsterLocations.push_back(monsterLocation);
            }
        }
        agentLocation = width / 2;
        currTime = 0;
        agentBulletFired = false;
        active = true;
        dir = 1;
    }
    
    void processWorld()
    {
        bool turn = false;
        
        for(uint i=0; i < monsterLocations.size(); i++)
        {
            if( (dir && (monsterLocations[i].second == height-1)) || (!dir && (monsterLocations[i].second == 0)) )
            {
                turn = true;
            }
        }
        
        if(turn)
        {
            dir = !dir;
        }
        
        for(uint i=0; i < monsterLocations.size(); i++)
        {
            if(dir)
            {
                monsterLocations[i].second++;
            }
            else
            {
                monsterLocations[i].second--;
            }
            
            assert(monsterLocations[i].second < width);
            assert(monsterLocations[i].second >= 0);
        }
        
        vector<pair<int, int> > newEnemyBulletLocations;
        
        for(uint i=0; i < enemyBulletLocations.size(); i++)
        {
            if(enemyBulletLocations[i].first < height-1)
            {
                newEnemyBulletLocations.push_back(make_pair(enemyBulletLocations[i].first+1, enemyBulletLocations[i].second));
            }
        }
        
        enemyBulletLocations = newEnemyBulletLocations;
        
        vector<pair<int, int> > newAgentBulletLocations;
        
        for(uint i=0; i < agentBulletLocations.size(); i++)
        {
            if(agentBulletLocations[i].first > 1)
            {
                newAgentBulletLocations.push_back(make_pair(agentBulletLocations[i].first-1, agentBulletLocations[i].second));
            }
        }
        
        agentBulletLocations = newAgentBulletLocations;
        
        if(currTime % monsterFireTime == 0)
        {
            monsterFire();
        }
    }
    
    float processAction(int action)
    {
        /* Possible actions:
         * 0: Do nothing
         * 1: Fire
         * 2: Left
         * 3: Right
         * 4: Left Fire
         * 5: Right Fire
         */
        
        if(!active)
            return 0;
        
        processWorld();
        
        switch(action)
        {
            case 0:
                break;
                
            case 1:
                if(!agentBulletFired)
                {
                    agentFire();
                    agentBulletFired = true;
                }
                
                break;
                
            case 2:
                if(agentLocation > 0)
                {
                    agentLocation--;
                }
                
                break;
                
            case 3:
                if(agentLocation < width-1)
                {
                    agentLocation++;
                }
                
                break;
                
            case 4:
                if(!agentBulletFired)
                {
                    agentFire();
                    agentBulletFired = true;
                }
                
                if(agentLocation > 0)
                {
                    agentLocation--;
                }
                
                break;
                
            case 5:
                if(!agentBulletFired)
                {
                    agentFire();
                    agentBulletFired = true;
                }
                
                if(agentLocation < width-1)
                {
                    agentLocation++;
                }
                
                break;
                
            default:
                assert(0);
        }
        
        int reward = 0;
        
        /* Check if enemies are hit */
        for(uint i=0; i < agentBulletLocations.size(); i++)
        {
            for(uint j=0; j < monsterLocations.size(); j++)
            {
                if(agentBulletLocations[i] == monsterLocations[j])
                {
                    agentBulletLocations[i].first = -1;
                    agentBulletLocations[i].second = -1;
                    monsterLocations[j].first = -1;
                    monsterLocations[j].second = -1;
                    reward++;
                }
            }
        }
        
        vector<pair<int, int> > newAgentBulletLocations;
        vector<pair<int, int> > newMonsterLocations;
        
        for(uint i=0; i < agentBulletLocations.size(); i++)
        {
            if( !(agentBulletLocations[i].first == -1 && agentBulletLocations[i].second == -1) )
            {
                newAgentBulletLocations.push_back(agentBulletLocations[i]);
            }
        }
        
        for(uint i=0; i < monsterLocations.size(); i++)
        {
            if( !(monsterLocations[i].first == -1 && monsterLocations[i].second == -1) )
            {
                newMonsterLocations.push_back(monsterLocations[i]);
            }
        }
        
        monsterLocations = newMonsterLocations;
        agentBulletLocations = newAgentBulletLocations;
        
        /* Check if agent is hit */
        bool hit = false;
        
        for(uint i=0; i < enemyBulletLocations.size(); i++)
        {
            if(enemyBulletLocations[i] == make_pair(height-1, agentLocation))
            {
                hit = true;
                break;
            }
        }
        
        if(hit)
        {
            active = false;
        }
        
        /* If bullet is gone */
        if(agentBulletLocations.size() == 0)
        {
            agentBulletFired = false;
        }
        
        currTime++;
        
        if(currTime >= maxEpisodeLength)
        {
            active = false;
        }
        
        if(active && monsterLocations.size() == 0) {
            int saveTime = currTime;
            reset();
            currTime = saveTime;
        }
        
        return (float)reward;
    }
    
    int getNumActions()
    {
        return numActions;
    }

    void saveState() {
        state st(active,dir,agentBulletFired,
                 currTime,agentLocation,
                 monsterLocations,agentBulletLocations,
                 enemyBulletLocations);
        s.push(st);
    }

    void restoreState() {
        assert(s.size() > 0);
        state st = s.top();
        loadState(st);
        s.pop();
    }

    state getState() {
        state st(active,dir,agentBulletFired,
                 currTime,agentLocation,
                 monsterLocations,agentBulletLocations,
                 enemyBulletLocations);
        return st;
    }

    void loadState(state st) {
        active = st.savedActive;
        currTime = st.savedCurrTime;
        dir = st.savedDir;
        agentBulletFired = st.savedAgentBulletFired;
        agentLocation = st.savedAgentLocation;
        monsterLocations = st.savedMonsterLocations;
        agentBulletLocations = st.savedAgentBulletLocations;
        enemyBulletLocations = st.savedEnemyBulletLocations;
    }

    void printState() {
        cout<<"active = "<<active<<endl;
        cout<<"currTime = "<<currTime<<endl;
        cout<<"agent Loc = "<<agentLocation<<endl;
        cout<<"# of monsters = "<<monsterLocations.size()<<endl;
    }
    
};
