#include <vector>
#include <cstdlib>
#include <cassert>
#include <cmath>

#ifndef PII
    #define PII pair<int,int>
#endif

using namespace std;

struct state {
    bool savedActive;
    PII savedP;
    public:
    state(bool active, PII p) {
        savedActive = active;
        savedP = p;
    }

    state(const state& st) {
        this->savedActive = st.savedActive;
        this->savedP = st.savedP;
    }
    
    bool equals(state& st) {
        return this->savedActive == st.savedActive && 
               this->savedP == st.savedP;
    }

    float computeDistance(state& st) {
        PII p = this->savedP, q = st.savedP;
        return sqrt((p.first - q.first) * (p.first - q.first) +
               (p.second - q.second) * (p.second - q.second));
    }
};

class GridWorld {
    static const int numActions = 4;
    enum actions {UP = 0, DOWN, LEFT, RIGHT};

    static const int n = 40;
    bool active;
    PII p, start;
    PII goal;
    PII wall;
    int wall_length;

    stack<state> s;

    public:
    GridWorld(PII point, PII g, PII w, int wl = 0) {
        this->p = point;
        this->start = point;
        this->goal = g;
        wall = w;
        wall_length = wl;
    }

    void reset() {
        active = true;
        p = start; 
    }

    bool isActive() {
        return active;
    }

    float processAction(int action) {
        assert(p.first >= 0);
        assert(p.second < n);

        
        PII currP = p;
        switch(action) {
            case UP:
                if(p.first > 0) p.first--;
                break;
            case DOWN:
                if(p.first < n-1) p.first++;
                break;
            case LEFT:
                if(p.second > 0) p.second--;
                break;
            case RIGHT:
                if(p.second < n-1) p.second++;
                break;
            default:
                assert(0);
        }

        if(p.first == wall.first &&
           p.second >= wall.second &&
           p.second <= wall.second + wall_length) {
            p = currP;
        }

        float reward = 0;
        
        reward = p.first == goal.first && p.second == goal.second;

        if(!active) return 0;

        if(reward) {
            active = false;
        }
        return reward;
    }

    int getNumActions() {
        return numActions;    
    }

    void saveState() {
        state st(active,p);
        s.push(st);
    }

    void loadState(state& st) {
        active = st.savedActive;
        p = st.savedP;
    }

    void restoreState() {
        assert(s.size() > 0);
        state st = s.top();
        loadState(st);    
        s.pop();
    }

    state getState() {
        state st(active,p);
        return st;
    }

    void printState() {
        cout<<"point = "<<p.first<<" "<<p.second<<endl;
    }
    
};
