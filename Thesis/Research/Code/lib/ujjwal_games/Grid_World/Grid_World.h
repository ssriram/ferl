#include <vector>
#include <cstdlib>
#include <cassert>
#include <cmath>

#ifndef PII
    #define PII pair<int,int>
#endif

using namespace std;

struct state {
    bool savedActive;
    int savedCurrTime;
    PII savedP;
    public:
    state(bool active, int currTime, PII p) {
        savedActive = active;
        savedCurrTime = currTime;
        savedP = p;
    }
    
    bool equals(state& st) {
        return this->savedActive == st.savedActive &&
               this->savedCurrTime == st.savedCurrTime && 
               this->savedP == st.savedP;
    }
};

class GridWorld {
    static const int numActions = 4;
    enum actions {UP = 0, DOWN, LEFT, RIGHT};

    static const int n = 40;
    static const int maxEpisodeLength = 160;
    bool active;
    int currTime;
    PII p, start;
    PII goal;
    PII wall;
    int wall_length;

    stack<state> s;

    public:
    GridWorld(PII point, PII g, PII w, int wl = 0) : active(true), currTime(0) {
        this->p = point;
        this->start = point;
        this->goal = g;
        wall = w;
        wall_length = wl;
    }

    void reset() {
        active = true;
        p = start; 
        currTime = 0;
    }

    bool isActive() {
        return active;
    }

    float processAction(int action) {
        assert(p.first >= 0);
        assert(p.second < n);
        
        PII currP = p;
        switch(action) {
            case UP:
                if(p.first > 0) p.first--;
                break;
            case DOWN:
                if(p.first < n-1) p.first++;
                break;
            case LEFT:
                if(p.second > 0) p.second--;
                break;
            case RIGHT:
                if(p.second < n-1) p.second++;
                break;
            default:
                assert(0);
        }

        if(p.first == wall.first &&
           p.second >= wall.second &&
           p.second <= wall.second + wall_length) {
            p = currP;
        }

        float reward = 0;
        
        reward = p.first == goal.first && p.second == goal.second;

        currTime++;
        
        if(currTime >= maxEpisodeLength) active = false;

        if(reward) {
            int saveTime = currTime;
            reset();
            currTime = saveTime;
        }
        return reward;
    }

    int getNumActions() {
        return numActions;    
    }

    void saveState() {
        state st(active,currTime,p);
        s.push(st);
    }

    void loadState(state& st) {
        active = st.savedActive;
        currTime = st.savedCurrTime;
        p = st.savedP;
    }

    void restoreState() {
        assert(s.size() > 0);
        state st = s.top();
        loadState(st);    
        s.pop();
    }

    state getState() {
        state st(active,currTime,p);
        return st;
    }

    void printState() {
        cout<<"currTime = "<<currTime<<endl;
        cout<<"point = "<<p.first<<" "<<p.second<<endl;
    }
    
};
