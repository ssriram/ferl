#include <vector>
#include <cstdlib>
#include <cassert>
#include <unordered_set>
#include <cmath>

using namespace std;

class Asterix
{
    vector<vector<int> > screen;
    static const int numColours = 6;
    enum colour
    {
        COLOUR_BLANK = 0,
        COLOUR_AGENT,
        COLOUR_POWERED_AGENT,
        COLOUR_POWER_UP,
        COLOUR_FRIENDLY,
        COLOUR_MONSTER
    };
    static const int numActions = 5;
    enum actions 
    {
        ACTION_NULL = 0,
        ACTION_UP,
        ACTION_RIGHT,
        ACTION_DOWN,
        ACTION_LEFT        
    };    
    static const int width = 16;
    static const int height = 16;
    static const int powerUpProbabilityPercentage = 50;
    static const int powerUpDuration = 32; 
    static const int restTime = 0;
    static const int maxEpisodeLength = 256;
    int currTime;
    int featureType;
    int proximity;
    bool active;
    bool powerUpOnScreen;
    int powerUpTimeRemaining;
    pair<int, int> agentLocation;
    pair<int, int> powerUpLocation;
    vector<int> friendlyLocations;
    vector<bool> friendlyDirections;
    vector<int> monsterLocations;
    vector<bool> monsterDirections;

    public:
    Asterix(int featureType, int proximity) : featureType(featureType), proximity(proximity), active(false), powerUpOnScreen(false), powerUpTimeRemaining(0), currTime(0), agentLocation(make_pair(0,0)), powerUpLocation(make_pair(0,0)), screen(height, vector<int>(width, 0))
    {
    }

    bool isActive()
    {
        return active;
    }
        
    vector<vector<int> > getScreen()
    {
        return screen;
    }
    
    void reset()
    {
        screen = vector<vector<int> >(height, vector<int>(width, 0));
        powerUpOnScreen = false;
        powerUpTimeRemaining = 0;
        agentLocation.first = rand() % height;
        agentLocation.second = rand() % width;
        powerUpLocation.first = -1;
        powerUpLocation.first = -1;
        friendlyLocations = vector<int>(height, -1);
        friendlyDirections = vector<bool>(height, false);
        monsterLocations = vector<int>(height, -1);
        monsterDirections = vector<bool>(height, false);
        generateScreen();
        currTime = 0;
        active = true;
    }

    void generateScreen()
    {
        screen = vector<vector<int> >(height, vector<int>(width, 0));
        
        if(powerUpOnScreen)
        {
            assert(powerUpLocation.first < height);
            assert(powerUpLocation.first >= 0);
            assert(powerUpLocation.second < width);
            assert(powerUpLocation.second >= 0);            
            screen[powerUpLocation.first][powerUpLocation.second] = COLOUR_POWER_UP;
        }
        
        for(int i=0; i < height; i++)
        {
            if(friendlyLocations[i] != -1)
            {
                assert(friendlyLocations[i] >= 0);
                assert(friendlyLocations[i] < width);
                screen[i][friendlyLocations[i]] = COLOUR_FRIENDLY;
            }
            
            if(monsterLocations[i] != -1)
            {
                assert(monsterLocations[i] >= 0);
                assert(monsterLocations[i] < width);
                screen[i][monsterLocations[i]] = COLOUR_MONSTER;
            }
        }
        
        assert(agentLocation.first < height);
        assert(agentLocation.first >= 0);
        assert(agentLocation.second < width);
        assert(agentLocation.second >= 0);
        
        if(powerUpTimeRemaining > 0)
        {
            screen[agentLocation.first][agentLocation.second] = COLOUR_POWERED_AGENT;
        }
        else
        {
            screen[agentLocation.first][agentLocation.second] = COLOUR_AGENT;
        }
    }
    
    void processWorld()
    {
        vector<vector<int> > newScreen(height, vector<int>(width, 0));
        
        for(int i=0; i < height; i++)
        {
            
        }
        
        screen = newScreen;
        
        if((currTime % (width + restTime)) == 0)
        {
            generateWave();
        }
        
        currTime++;
    }
    
    void generateWave()
    {
        bool type = rand()%2;
        bool dir = rand()%2;
        int offset = rand()%2;
        
        for(int i=offset; i < height; i += 2)
        {
            if(type)
            {
                monsterLocations[i] = (!dir) * (width-1);
                monsterDirections[i] = dir;
            }
            else
            {
                friendlyLocations[i] = (!dir) * (width-1);
                friendlyDirections[i] = dir;
            }
        }
        
        int p = rand()%100;
        
        if(!powerUpOnScreen && p < powerUpProbabilityPercentage)
        {
            powerUpLocation.first = rand() % height;
            powerUpLocation.second = rand() % width;
            powerUpOnScreen = true;
        }
    }
    
    double processAction(int action)
    {
        if(!active)
        {
            return 0;
        }
        
        assert(agentLocation.first >= 0);
        assert(agentLocation.first < height);
        assert(agentLocation.second >= 0);
        assert(agentLocation.second < width);
        
        switch(action)
        {
            case ACTION_NULL:
                break;
            case ACTION_UP:
                if(agentLocation.first > 0)
                    agentLocation.first--;
                break;
            case ACTION_RIGHT:
                if(agentLocation.second < width - 1)
                    agentLocation.second++;
                break;
            case ACTION_DOWN:
                if(agentLocation.first < height - 1)
                    agentLocation.first++;
                break;
            case ACTION_LEFT:
                if(agentLocation.second > 0)
                    agentLocation.second--;
                break;
            default:
                assert(0);
        }
        
        if(powerUpOnScreen && agentLocation == powerUpLocation)
        {
            powerUpOnScreen = false;
            powerUpLocation.first = -1;
            powerUpLocation.second = -1;
            powerUpTimeRemaining = powerUpDuration;
        }
        else if(powerUpTimeRemaining > 0)
        {
            powerUpTimeRemaining--;
        }
        
        bool collide = false;
        int reward = 0;
        
        for(int i=0; i < height; i++)
        {
            if(friendlyLocations[i] != -1)
            {
                assert(friendlyLocations[i] >= 0);
                assert(friendlyLocations[i] < width);
                
                if(friendlyDirections[i])
                {
                    friendlyLocations[i]++;
                    if(friendlyLocations[i] == width)
                    {
                        friendlyLocations[i] = -1;
                    }
                    
                    if( (agentLocation.first == i && agentLocation.second == friendlyLocations[i]) ||
                        (agentLocation.first == i && agentLocation.second == friendlyLocations[i]-1 && action == ACTION_LEFT) )
                    {
                        reward++;
                        friendlyLocations[i] = -1;
                    }
                }
                else
                {
                    friendlyLocations[i]--;
                    
                    if( (agentLocation.first == i && agentLocation.second == friendlyLocations[i]) ||
                        (agentLocation.first == i && agentLocation.second == friendlyLocations[i]+1 && action == ACTION_RIGHT) )
                    {
                        reward++;
                        friendlyLocations[i] = -1;
                    }
                }
            }
            
            if(monsterLocations[i] != -1)
            {
                assert(monsterLocations[i] >= 0);
                assert(monsterLocations[i] < width);
                
                if(monsterDirections[i])
                {
                    monsterLocations[i]++;
                    if(monsterLocations[i] == width)
                    {
                        monsterLocations[i] = -1;
                    }
                    
                    if( (agentLocation.first == i && agentLocation.second == monsterLocations[i]) ||
                        (agentLocation.first == i && agentLocation.second == monsterLocations[i]-1 && action == ACTION_LEFT) )
                    {
                        collide = true;
                        monsterLocations[i] = -1;
                    }
                }
                else
                {
                    monsterLocations[i]--;
                    
                    if( (agentLocation.first == i && agentLocation.second == monsterLocations[i]) ||
                        (agentLocation.first == i && agentLocation.second == monsterLocations[i]+1 && action == ACTION_RIGHT) )
                    {
                        collide = true;
                        monsterLocations[i] = -1;
                    }
                }
            }
        }
        
        if(collide)
        {
            if(powerUpTimeRemaining > 0)
            {
                reward++;
            }
            else
            {
                active = false;
            }
        }
        
        if((currTime % (width + restTime)) == 0)
        {
            generateWave();
        }
        
        currTime++;
        
        if(currTime == maxEpisodeLength)
        {
            active = false;
        }
        
        generateScreen();
        return reward;
    }
    
    int getNumActions()
    {
        return numActions;
    }
    
    unordered_set<int> getFeatures()
    {
        if(featureType == 4)
        {
            return getLogarithmicRelativeFeatures();
        }
        else if(featureType == 3)
        {
            return getPairwiseLogarithmicRelativeFeatures();
        }
        else if(featureType == 2)
        {
            return getPairwiseRelativeFeatures();
        }
        else if(featureType == 1)
        {
            return getRelativeFeatures();
        }
        else if(featureType == 0)
        {
            return getAbsoluteFeatures();
        }
        else
        {
            assert(0);
        }
    }
    
    int getNumFeatures()
    {
        if(featureType == 4)
        {
            return 1 + (numColours-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2);
        }
        else if(featureType == 3)
        {
            return height*width*(numColours-1) + 4*(floor(log2(height-1))+2)*(floor(log2(width-1))+2)*(numColours-1)*(numColours)/2;
        }
        else if(featureType == 2)
        {
            return (2*proximity-1)*(2*proximity-1)*(numColours-1)*(numColours)/2;
        }
        else if(featureType == 1)
        {
            return (2*proximity-1)*(2*proximity-1)*(numColours-1);
        }
        else if(featureType == 0)
        {
            return height*width*(numColours-1);
        }
        else
        {
            assert(0);
        }
    }
    
    unordered_set<int> getAbsoluteFeatures()
    {
        unordered_set<int> features;
        
        for(int i=0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                if(screen[i][j] > 0)
                {
                    features.insert((screen[i][j]-1)*height*width + i*width + j);
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getRelativeFeatures()
    {
        unordered_set<int> features;
        
        for(int i = max(0, agentLocation.first-proximity+1); i < min(int(height), agentLocation.first+proximity); i++)
        {            
            for(int j = max(0, agentLocation.second-proximity+1); j < min(int(width), agentLocation.second+proximity); j++)
            {
                if(screen[i][j] > 0)
                {
                    features.insert((screen[i][j]-1)*(2*proximity-1)*(2*proximity-1) + (i-agentLocation.first+proximity-1)*(2*proximity-1) + (j-agentLocation.second+proximity-1));
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getPairwiseRelativeFeatures()
    {
        unordered_set<int> features;
        
        for(int i = 0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                for(int k = max(0, i-proximity+1); k < min(int(height), i+proximity); k++)
                {              
                    for(int l = max(0, j-proximity+1); l < min(int(width), j+proximity); l++)
                    {
                        if(screen[i][j] == 0 || screen[i][j] > screen[k][l])
                        {
                            continue;
                        }
                        
                        int featureIndex = ((screen[i][j]-1)*(numColours-1)-((screen[i][j]-1)*(screen[i][j]-2))/2+screen[k][l]-screen[i][j])*(2*proximity-1)*(2*proximity-1) + (k-i+proximity-1)*(2*proximity-1) + (l-j+proximity-1);
                        
                        if(features.find(featureIndex) == features.end())
                        {
                            features.insert(featureIndex);
                        }
                    }
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getPairwiseLogarithmicRelativeFeatures()
    {
        unordered_set<int> features;
        
        
        for(int i = 0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                for(int k = 0; k < height; k++)
                {              
                    for(int l = 0; l < width; l++)
                    {
                        if(screen[i][j] == 0 || screen[i][j] > screen[k][l])
                        {
                            continue;
                        }
                        
                        int quadrant = (k>i)*2 + (l>j);
                        int logX;
                        int logY;
                        
                        if(k==i)
                        {
                            logX = 0;
                        }
                        else
                        {
                            logX = floor(log2(abs(k-i)))+1;
                        }
                        
                        if(l==j)
                        {
                            logY = 0;
                        }
                        else
                        {
                            logY = floor(log2(abs(l-j)))+1;
                        }
                        
                        int featureIndex = height*width*(numColours-1) + ((screen[i][j]-1)*(numColours-1)-((screen[i][j]-1)*(screen[i][j]-2))/2+screen[k][l]-screen[i][j])*4*(floor(log2(height-1))+2)*(floor(log2(width-1))+2) + quadrant*(floor(log2(height-1))+2)*(floor(log2(width-1))+2) + logX*(floor(log2(width-1))+2) + logY;
                        
                        if(features.find(featureIndex) == features.end())
                        {
                            features.insert(featureIndex);
                        }
                    }
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getLogarithmicRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = max(0, agentLocation.first-proximity+1); i < min(int(height), agentLocation.first+proximity); i++)
        {            
            for(int j = max(0, agentLocation.second-proximity+1); j < min(int(width), agentLocation.second+proximity); j++)
            {
                if(screen[i][j] > 0)
                {
                    int quadrant = (i>agentLocation.first)*2 + (j>agentLocation.second);
                    int relI = abs(i-agentLocation.first);
                    int relJ = abs(j-agentLocation.second);
                    int logI;
                    int logJ;
                    
                    if(relI == 0)
                    {
                        logI = 0;
                    }
                    else
                    {
                        logI = floor(log2(relI));
                    }
                    
                    if(relJ == 0)
                    {
                        logJ = 0;
                    }
                    else
                    {
                        logJ = floor(log2(relJ));
                    }
                    
                    int featureIndex = ( (screen[i][j]-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) +
                                         quadrant * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) +
                                         logI * (floor(log2(width-1))+2) +
                                         logJ +
                                         1
                                       );
                    
                    assert(featureIndex >= 0);
                    assert(featureIndex < (numColours-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) + 1);
                    
                    if(features.find(featureIndex) == features.end())
                    {
                        features.insert(featureIndex);
                    }
                }
            }
        }
        
        return features;
    }
};

unordered_set<int> expandFeatures(unordered_set<int>& features, int size)
{
    unordered_set<int> expandedFeatures = features;
    for(auto it1 = features.begin(); it1 != features.end(); it1++)
    {
        auto it2 = it1;
        it2++;
        
        for(; it2 != features.end(); it2++)
        {
            int minF = min(*it1, *it2);
            int maxF = max(*it1, *it2);
            expandedFeatures.insert(size + size*minF - minF*(minF+1)/2 + maxF - minF - 1);
        }
    }
    
    return expandedFeatures;
}