\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {chapter}{\numberline {2}Background}{4}
\contentsline {section}{\numberline {2.1}Markov Decision Processes (MDPs)}{4}
\contentsline {section}{\numberline {2.2}Value Function}{5}
\contentsline {section}{\numberline {2.3}Monte-Carlo Tree Search}{6}
\contentsline {section}{\numberline {2.4}UCT - UCB on Trees}{8}
\contentsline {subsection}{\numberline {2.4.1}Convergence of UCT}{10}
\contentsline {section}{\numberline {2.5}Enhancements to UCT}{10}
\contentsline {chapter}{\numberline {3}State Generalization in UCT}{13}
\contentsline {section}{\numberline {3.1}NN-UCT}{14}
\contentsline {subsection}{\numberline {3.1.1}Decay Scheme}{15}
\contentsline {section}{\numberline {3.2}Theoretical Analysis}{16}
\contentsline {section}{\numberline {3.3}Related Work}{18}
\contentsline {chapter}{\numberline {4}State Generalization on Complex Domains}{20}
\contentsline {section}{\numberline {4.1}Manifolds}{21}
\contentsline {section}{\numberline {4.2}Manifold Learning}{21}
\contentsline {subsection}{\numberline {4.2.1}Isomap}{22}
\contentsline {subsection}{\numberline {4.2.2}Local Manifolds}{23}
\contentsline {section}{\numberline {4.3}mNN-UCT}{24}
\contentsline {subsection}{\numberline {4.3.1}Discussion}{24}
\contentsline {section}{\numberline {4.4}Related Work}{25}
\contentsline {chapter}{\numberline {5}Experiments}{27}
\contentsline {section}{\numberline {5.1}Experiment Setup}{27}
\contentsline {section}{\numberline {5.2}Grid World Domains}{27}
\contentsline {section}{\numberline {5.3}Game Domains}{29}
\contentsline {subsection}{\numberline {5.3.1}Performance Comparison.}{30}
\contentsline {subsection}{\numberline {5.3.2}Parameter Sensitivity.}{33}
\contentsline {section}{\numberline {5.4}Summary}{33}
\contentsline {chapter}{\numberline {6}Conclusion}{35}
\contentsline {chapter}{Bibliography}{37}
