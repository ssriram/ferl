\chapter{State Generalization on Complex Domains}
\label{chap:mNN-UCT}
\begin{figure*}[h]
	\centering
	\begin{subfigure}{0.36\hsize}
  		\centering
  		\includegraphics[width=0.8\hsize]{Grid_wall.eps}
  		\caption{Grid World with Wall}
  		\label{fig:grid_wall}
	\end{subfigure}
	\begin{subfigure}{0.59\hsize}
		\centering
		\includegraphics[width=\hsize]{manifold.eps}
		\caption{Manifold Embedding of 20 $\times$ 20 grid with wall}
		\label{fig:mani}
	\end{subfigure}
\caption{Grid World with Wall}
\label{fig:grid_world_wall}
\end{figure*}
In the grid world example shown in Figure \ref{fig:grid_world}, the Euclidean distance between two points on the grid was a good choice for a distance metric. However, in general, the grid coordinates may not be the best representation. For example, consider a grid world with a wall as shown in Figure \ref{fig:grid_wall}. In this case, the distance between grid coordinates is not a good distance metric, as states on either side of the wall, are farther apart than those on the same side. Furthermore, not all state representations have a natural metric space for defining similarity. High dimensional state spaces pose an additional problem of tractably computing a distance metric. Hence, we need a state representation that captures the underlying topology of the environment, that is low dimensional, allowing fast computation of distances between states. Low dimensional manifolds are a natural choice that satisfy all the requirements.

\section{Manifolds}
\label{sec:mani}
A manifold $\mathcal{M} \subset \mathbb{R}^m$ is a set of points such that the Euclidean distance between neighboring points on the manifold reflects the true distances between those points. Figure \ref{fig:twoD} shows two dimensional data from four gaussian distributions. The data was mapped to three dimensions using the mapping $\langle x,y \rangle \to \langle x * \cos(x),y,x * \sin(x) \rangle$ to result in a Swiss roll shape shown in Figure \ref{fig:swiss_roll}. The 3D data on a Swiss roll is said to lie on a 2D manifold. Note that the euclidean distance between nearby points in the Swiss roll data is not exactly the same as the Euclidean distance between nearby points in the two dimensional data, but approximately equal.
\begin{figure*}
	\centering
	\begin{subfigure}{0.42\hsize}
  		\centering
  		\includegraphics[width=0.9\hsize]{twoD_data.eps}
  		\caption{2D Data}
  		\label{fig:twoD}
	\end{subfigure}%
	\begin{subfigure}{0.58\hsize}
  		\centering
  		\includegraphics[width=\hsize]{swiss_roll.eps}
  		\caption{Swiss Roll}
  		\label{fig:swiss_roll}
	\end{subfigure}
	\caption{2D data and its 3D mapping}
\end{figure*}

A manifold embedding refers to the representation of the points in the data in Euclidean space. Figure \ref{fig:mani} shows an example of a 2D manifold embedding of a $20 \times 20$ grid, with a wall. The intersection of edges represents points on the grid, with the length of the edges representing the approximate distance between the points on the grid. Notice that embedding has made the points on either side of the wall to be farther apart than other points. The absence of a direct edge between these points captures the presence of the wall.

\section{Manifold Learning}
Manifold learning typically involves learning a low-dimensional representation, subject to distance preserving constraints. Manifold learning algorithms \citep{ma2012manifold} rely on the true distances between neighboring states, while approximating other distances. Euclidean distance between points on the manifold can then serve as a distance metric for measuring similarity of states. Thus, the similarity measure needed in our NN-UCT algorithm, can be obtained by embedding the states on a manifold.

\subsection{Isomap}
\label{subsec:isomap}
Isomap \citep{tenenbaum2000global} is a popular manifold learning algorithm that is typically used on high dimensional data. The input is a distance matrix. The distance matrix is constructed in the following way: The distance between a point and any of its $k$ nearest neighbors is equal to the Euclidean distance or the actual distance between them. The other pairwise distances are computed using the shortest paths between them. The rest of the steps is the same as Metric Dimensional Scaling (MDS), which is to compute an embedding that preserves the pairwise distances. Algorithm \ref{alg:isomap} shows the steps involved in learning a manifold from a transition graph.

\begin{algorithm}
\caption{Isomap algorithm}\label{alg:isomap}
\begin{algorithmic}[1]
\Function{Isomap}{$G$} \Comment $G = \langle V,E \rangle$ is the state transition graph
\State Construct distance matrix $D$ from G such that 
\For{$u,v \in V$}
\If{$\langle u,v \rangle \in E$}
	\State $D(u,v) = 1$
\Else
	\State $D(u,v)$ = length of shortest path from $u \to v$
\EndIf
\EndFor
\State $n = |V|$
\State $H = I_n - \frac{1}{n} 1 1^{T}$ \Comment Construct Centering matrix
\State $Dsq = \frac{1}{2} * H * D^2 * H^T$ \Comment Center the squared Distance Matrix
\State Compute top $k$ eigen-vectors of $Dsq$ . Choose $k$ by heuristic
\State \Return{Embedding as the top $k$ eigen-vectors of $Dsq$}
\EndFunction
\end{algorithmic}
\end{algorithm}

The computational complexity of most manifold learning algorithms, including Isomap, is $\BigO{m^3}$, where $m$ is the number of points needed to be embedded. This cost is due to the distance matrix construction and subsequent eigen-decomposition. 

It is important to note that MDS (and hence Isomap) assumes that the distance matrix is symmetric. Such an assumption may not be true in certain domains (e.g when time elapsed is part of the state). Other manifold learning algorithms such as Graph Laplacian can be applied on directed graphs that do not impose this constraint. In this thesis, I used Isomap for illustrative purpose to show that manifolds learnt from state transition graph of MDPs can be useful for planning.

It is also noteworthy to point out the criteria used for choosing $k$- the top eigenvectors for the embedding. One such approach is to threshold the eigenvalues corresponding to the eigenvectors. Another approach is to measure the reconstruction error. In this case, the reconstruction error corresponds to the error between reconstructing the pairwise distances on the manifold and the real distances in high dimensional space. However, this involves additional computation cost of $\BigO{m^2}$ for each dimension to threshold. In this thesis, I used the former approach of picking the eigenvectors by thresholding based on eigenvalues.

\subsection{Local Manifolds}
In my problem setting, each state is a point to be embedded, making cubic complexity infeasible for large problems. One way to deal with the high computational cost of embedding states on a manifold is to embed only those states that may be encountered in the near future. As we are operating in the planning setting, with access to a model, we can sample the states reachable in the near future and construct a manifold that is local to the current state.

In our work, at every decision step, we learn a local manifold of the deterministic MDP from the sample transitions encountered during a short breadth-first walk of the state space, starting from the current state. The state transition graph from the BFS walk is the input to the Isomap algorithm outlined in Algorithm \ref{alg:isomap}. The output of Isomap is an embedding of the states in Euclidean space.

During UCT planning, we may encounter states that lie outside the local manifold. This situation is referred to as the out-of-sample problem and is more likely to occur as the UCT tree grows larger. If we were to embed these states on a manifold, the BFS walk needed from the start state would get deeper, resulting in a possibly exponential increase in the number of states to be embedded.

We address the out-of-sample problem in the following way. We generate the approximate embeddings of these states by applying a translation operator learnt for each action in the action set. An operator is a function that takes as input the manifold embedding of the current state and action, and outputs the manifold embedding (approximate) of the next state. For example, if $x$ denotes the manifold embedding of a state $s$, and action $k$ was taken from $s$ to obtain state $s'$ which was not encountered during the BFS walk, the operator for action $k$ is given by: $f(x,k) = x + b_k$, where $b_k$ is a vector that denotes the average translation offset for action $k$. We learn this offset from the manifold embeddings of the states encountered during the BFS walk for the local manifold. Let $T$ denote the set of tuples $\langle x, k, y \rangle$, such that $y$ is the manifold embedding of the state obtained by taking action $k \in A$ from the state with manifold embedding $x$. We learn a simple translation offset ($b_k$) for this action by :
\begin{align*}
b_k = \frac{1}{|T|} \sum_{t \in T, t = \langle x, k, y \rangle} (y - x).
\end{align*}
We found the simple translation operator to be useful in all the domains tested in Chapter \ref{chap:expt}. Applying more sophisticated solutions to solve the out-of-sample problem is a direction for future work.

Our use of local manifolds ensures good local coverage of the state space using the BFS walk and along with operators limits complexity by not embedding the entire space. In chapter \ref{chap:expt}, we evaluate the choice of using local manifolds over global manifolds on grid world domains and find them to offer similar benefits.

\section{mNN-UCT}
We combine all the steps to obtain NN-UCT with manifolds, referred to as mNN-UCT. This algorithm uses the manifold embedding as the state representation to compute the kernel function, needed in NN-UCT. During planning, for nodes whose states have already been encountered during the BFS walk, their manifold embedding is computed from the Isomap algorithm described above. If a state outside of the BFS walk is encountered, its manifold embedding is computed by applying the translation operator to the state from which the most recent action was taken. This enables the UCT tree to grow deeper than the BFS walk.

\subsection{Discussion}
While we show empirically (Chapter \ref{chap:expt}) that state generalization helps UCT, the improvement comes at an additional computational cost. In our algorithm, the cost is due to manifold learning and computing the nearest neighbor estimates. The cost from manifold learning is controlled by size of the BFS walk. While it would be useful to have a large size of the BFS walk to ensure a high coverage of the state space, the number of states to be embedded will increase exponentially in the depth of the BFS tree. Recall from section \ref{subsec:isomap} that the computational cost of most manifold learning algorithms is $\BigO{m^3}$, where $m$ is the number of states to be embedded. Thus, increasing the size of the BFS walk results in a dramatic increase in the cost for manifold learning. Our choice of using local manifolds constructed from a small BFS tree, along with operators, does not limit UCT from growing deeper. Although operators only approximate the embeddings of states outside of the BFS walk, the error is likely to be small within the finite horizon of UCT planning. In all our experiments, the number of calls to the operator during planning totaled less than $20\%$ of the planning steps. Thus, the approximation error due to the operator does not increase significantly.

The cost of computing the nearest neighbor estimate is controlled by the number of rollouts because each rollout adds one node. Experimental results (Chapter \ref{chap:expt}) show that mNN-UCT may need fewer rollouts than UCT to achieve good performance. This limits the cost of computing nearest neighbor estimates and may in itself justify the additional overhead.

\section{Related Work}
Learning manifolds in MDPs was previously done in proto-value functions (PVFs) \citep{mahadevan2007proto}. Proto-value functions are used to learn a low dimensional representation from a transition graph generated by following a fixed policy which is subsequently improved using policy iteration. PVFs assume that a random walk of the state space is sufficient to construct basis functions that reflect the underlying topology. However, in many domains with large state space, such as video games, a policy consisting of random actions does not provide a sufficient view of the state space. For example, in the grid world with a wall, such a policy is unlikely to make the agent move close to the wall. Additionally, it may be computationally infeasible to embed all of the states encountered by following the policy. The high cost of manifold learning limits the number of states that can be embedded.

The idea of using local homomorphisms \citep{jiang2014improving} as an approximate abstraction was an inspiration for the development of using local manifolds. In their work, they construct a homomorphism of the MDP \textit{locally}. They do so by finding a homomorphism from sample UCT trajectories. Since UCT trajectories are limited up to a finite depth in practice, the homomorphism obtained is local to the current state. Nevertheless, they show empirically that UCT can benefit from these local homomorphisms. The homomorphism construction procedure relies on the policy followed (UCB in this case). But as we have shown earlier, UCT's policy is poor when rewards are sparse. Hence, as mentioned in section \ref{sec:rel_work}, homomorphisms offer no benefit over transposition tables in sparse reward domains.