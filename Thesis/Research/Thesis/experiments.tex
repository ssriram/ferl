\chapter{Experiments}
\label{chap:expt}
Now that we have described the algorithms, it is time to evaluate them in practice. Video games are a natural choice which can be modelled as sequential decision problems. We can evaluate the algorithms using the score obtained in these games. Additionally, the sparse reward scenario occurs naturally in some games. In the next section, we describe the setup and evaluation methodology.

\section{Experiment Setup}
As the focus of this thesis is on using UCT for planning, details of its implementation are described. In my UCT implementation, whenever a new node is created, the default policy (random actions) was run for a constant length of length 50, rather than out to a fixed horizon. This helped prevent finite horizon effects from affecting generalization. The optimal branch of the UCT tree and the BFS tree was retained across planning steps. In all of the experiments, a wide range $\{10^{-5},10^{-4},..,10^3,10^4\}$ was tested for the UCB parameter with 500 trials performed for each parameter setting.
\section{Grid World Domains}
We first validate our intuitions about the impact of state generalization in UCT by evaluating the algorithms on the two grid world domains shown in Figures \ref{fig:grid_world} and \ref{fig:grid_world_wall}. An episode terminates when the agent reaches the goal state, or 1000 time-steps have elapsed, whichever is shorter. We report the number of steps taken to reach the goal state, given a fixed number of samples for planning. For the UCT algorithms with generalization, the Gaussian kernel function was used. The initial Gaussian width $\sigma$, was chosen amongst $\{10,100,1000,10000\}$ and the decay rate $\beta$ amongst $\{0.1,0.5,0.9,0.99\}$, and the best result is reported. The Euclidean distance was used for the distance metric in the kernel function. For the mNN-UCT algorithm, the BFS walk included at most 400 states for each planning step. The mNN-UCT algorithm with $\sigma = 10^{-6}$, referred to as mNN-UCT(0), effectively only shares statistics between nodes with the same state. This provides a difference over sharing values between nodes representing identical states. mNN-UCT-g is the mNN-UCT algorithm using a global instead of a local manifold, where a full BFS walk is used to construct the manifold. This allows us to evaluate the effectiveness of local manifolds.

\begin{figure*}
\centering
\begin{subfigure}{.49\hsize}
  \centering
  \includegraphics[width=\hsize]{grid_bar.eps}
  \caption{Grid}
  \label{fig:grid_bar}
\end{subfigure}%
\begin{subfigure}{.49\hsize}
  \centering
  \includegraphics[width=\hsize]{grid_wall_bar.eps}
  \caption{Grid with Wall}
  \label{fig:grid_wall_bar}
\end{subfigure}
\caption{Performance on Grid World Domains}
\label{fig:grid_bar_charts}
\end{figure*}

Figure \ref{fig:grid_bar_charts} summarizes the results of our grid world experiments. The mean number of steps (fewer steps being better), with standard error bars is shown. Sharing statistics between nodes with exactly the same states (mNN-UCT(0)) improves UCT slightly. Adding more widespread generalization to UCT improves the performance dramatically in both the domains, with the NN-UCT algorithm reaching the goal state in less than half the number of steps taken by UCT in the first domain.  The best performing $\sigma$ for the mNN-UCT and mNN-UCT-g algorithms was substantially greater than 0 (100), suggesting that generalizing over nearby states in the neighborhood of the manifold is useful. The NN-UCT algorithm, which uses the Euclidean distance metric between grid coordinates, does not perform as well in the second domain. The distance metric is not as useful since states on either side of the wall are farther apart than the Euclidean distance indicates. The mNN-UCT algorithm does not suffer from this problem as much, as it better captures the topology of the state space. In both the domains, using local manifolds rather than global manifolds does not substantially affect planning performance.

\section{Game Domains}
\begin{figure*}
        \centering
        \begin{subfigure}{0.3\hsize}
        		   \centering
                \includegraphics[width=0.9\hsize]{freeway_game.eps}
                \caption{Freeway}
        \end{subfigure}
        \begin{subfigure}{0.3\hsize}
        		   \centering
                \includegraphics[width=0.9\hsize]{invaders_game.eps}
                \caption{Space Invaders}
        \end{subfigure}
        \begin{subfigure}{0.3\hsize}
        		   \centering
                \includegraphics[width=0.9\hsize]{seaquest_game.eps}
                \caption{Seaquest}
        \end{subfigure}
        \caption{Games Domains}\label{fig:games}
\end{figure*}

We also test our algorithm on 3 domains inspired by video games. They are single player games played on a $16 \times 16$ pixel screen. The initial configuration of the games is shown in Figure \ref{fig:games}. All the games last for 256 time-steps or until the agent loses all of its lives, whichever is shorter. In each domain the agent is depicted by the blue square.
\begin{enumerate}
\item{Freeway : Cars are depicted by red chevrons. Their positions on each row and direction of movement is set randomly at the start. On reaching the end of the board, they reappear back on the opposite side. The agent has 3 lives. On collision, the agent loses a life and its position is reset to its start position. The agent has 3 actions: NO-OP (do nothing), UP and DOWN. The agent receives a reward of $+1$ on reaching the topmost row after which the agent's position is reset to the start state. The maximum possible score is 16 in the absence of cars, although with cars, the optimal score is likely lower. The complete state of the game is specified by the time elapsed, number of lives left, location of the agent, cars and their direction of movement}
\item{Space Invaders : The aliens are depicted by red triangles. These aliens fire bullets vertically downward, at fixed intervals of 4 time-steps. The aliens move from left to right. They change directions if the rightmost (or leftmost) alien reaches the end of the screen. The agent loses its only life if it gets hit by a bullet from the alien. The following actions are available to the agent : NO-OP, FIRE, LEFT, RIGHT, LEFT-FIRE (fire bullet and move left) and similarly RIGHT-FIRE (fire bullet and move right). The maximum possible score is 32 in the absence of the alien's bullets, with a realizable score likely lower. The state of the game is specified by the time elapsed, location of the agent and its bullet, locations of the aliens and their bullets.}
\item{Seaquest : Fish depicted by red chevrons, move from left to right and reappear back on reaching the end of the screen. The diver, indicated by a black triangle, appears at either end, alternately. A diver appears when the current diver is collected by the agent or if the current diver reaches the end of the screen. The agent receives a reward of +1 if it collects the diver and drops it at the drop point (top row, indicated in green). The agent loses its only life on collision with a fish, or on reaching the drop point without the diver. The actions available to the agent are NO-OP, UP, DOWN, LEFT and RIGHT. The maximum possible score is 8, as it is impossible to capture any 2 successive divers and drop them at the drop point. The state of the game is completely specified by the time elapsed, location of the agent, fish and divers.}
\end{enumerate}

\subsection{Performance Comparison.}
\begin{figure}[p]
        \centering
        \begin{subfigure}{0.9\hsize}
        		\centering
             \includegraphics[width=\hsize]{freeway.eps}
             \caption{Freeway}
        \end{subfigure}\\
        \begin{subfigure}{0.9\hsize}
        		\centering
             \includegraphics[width=\hsize]{invaders.eps}
             \caption{Space Invaders}
        \end{subfigure}\\
        \begin{subfigure}{0.9\hsize}
        		\centering
             \includegraphics[width=\hsize]{seaquest.eps}
             \caption{Seaquest}
        \end{subfigure}
        \caption{Performance comparison between mNN-UCT and UCT on Game Domains}\label{fig:perfcomp}
\end{figure}
We compare mNN-UCT with UCT on the domains described above. We again evaluate the effect of generalization by running our algorithm with $\sigma = 10^{-6}$, referred to as mNN-UCT(0). We ensure that all of the algorithms are given the same number of samples per planning step. We vary the number of rollouts from $\{20,40,80,160,320\}$. The discount factor was set at 0.99. We fix the other parameters for the mNN-UCT algorithm with $\sigma = 100$, and $\beta = 0.9$. We learn a local manifold for each planning step from a BFS tree comprising of at most 400 states. For each of the domains, we measure the mean score obtained per episode. The standard errors are indicated as error bars. The optimistic maximum score possible is indicated by a blue line in all the plots. Note that it may not be possible to achieve this score in all domains.

Figure \ref{fig:perfcomp} shows the performance of the 3 algorithms against the number of rollouts. The results are reported for the parameters which produced the best mean score obtained by performing 500 trials. mNN-UCT outperforms UCT, especially in Freeway and Seaquest. Both these games require the agent to perform a specific sequence of actions in order to obtain reward. UCT's rollout policy is sufficient to achieve a high score in Space Invaders as the game does not have sparse rewards to the same degree as the other domains. Generalization using manifolds provides only a marginal improvement in the Space Invaders game.

\begin{figure}[p]
        \centering
        \begin{subfigure}{0.9\hsize}
        			\centering
                \includegraphics[width=\hsize]{freeway_scatter.eps}
                \caption{Freeway}
        \end{subfigure}\\
        \begin{subfigure}{0.9\hsize}
        			\centering
                \includegraphics[width=\hsize]{invaders_scatter.eps}
                \caption{Space Invaders}
        \end{subfigure}\\
        \begin{subfigure}{0.9\hsize}
        			\centering
                \includegraphics[width=\hsize]{seaquest_scatter.eps}
                \caption{Seaquest}
        \end{subfigure}
        \caption{Parameter sensitivity of mNN-UCT algorithm on Game Domains}\label{fig:paramsens}
\end{figure}
\subsection{Parameter Sensitivity.}
Compared to UCT, the mNN-UCT algorithm has additional parameters such as the depth of the BFS walk,  and Isomap parameter (choosing the top $k$ eigenvalues) and the gaussian kernel width ($\sigma$) and the rate of decay ($\beta$). Of these, $\sigma$ and $\beta$ together control the degree of generalization in the UCT algorithm. While an extensive sweep of all the parameters is desirable, the main objective of this thesis is to demonstrate the effectiveness of state generalization in UCT. Hence, we evaluate the sensitivity of the algorithm with respect to only these parameters ($\sigma$ and $\beta$), keeping the rest fixed. Our choice for the rest of the parameters were set keeping in mind that they do not affect the objective. For example, the depth of the BFS walk was set so that it does not provide a full view of the state space, allowing the test of generalization possible along with the use of the algorithm in large state spaces. In this experiment, we fix the number of rollouts at 100. We vary $\sigma$ from $\{10,100,1000,10000\}$ and $\beta$ in $\{0.1,0.5.0.9,0.99\}$. For each of the parameter settings, we measure the performance of the mNN-UCT algorithm and compare it to UCT using various settings of the UCB parameter. 

Scatter plots are shown in Figure \ref{fig:paramsens} where each blue x and red square represents the mean score per episode of the mNN-UCT and UCT algorithms respectively for a specific parameter setting. We see that the mNN-UCT algorithm performs better than UCT for a wide range of parameter settings in all the domains. Using widespread generalization achieves significantly higher score on sparse reward games such as Freeway and Seaquest, while the improvement is modest when rewards are easy to obtain as in the game Space Invaders.

\section{Summary}
State generalization helps when rewards are hard to obtain. As we have seen through the heat maps and experimental results, generalization helps UCT to spend its rollouts more efficiently by visiting states different from those it has already visited. On domains whose state space does not have a natural distance metric, we showed a method to learn a local manifold of the state transition graph to use Euclidean distances on the manifold as a distance metric. In addition, the manifolds learnt were found to be low dimensional ($< 10$ dimensions). This not only helps computing the kernel function faster but provides a simple feature space to distinguish states.

The experimental results demonstrate that manifolds act as a useful distance metric for measuring similarity between states. In addition, the algorithm is not very sensitive to the parameters controlling the degree of generalization in these domains.