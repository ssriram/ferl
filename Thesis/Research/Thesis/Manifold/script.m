
data2 = importdata('preswissroll.dat');
ppm = [400 400 400 400];        % points per mixture
X = data2(:,1);
Y = data2(:,2);
data3 = [X.*cos(X) Y X.*sin(X)];
plotcol (data3,ppm,'rgbk');
