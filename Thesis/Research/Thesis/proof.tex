\documentclass{article}

\usepackage{palatino}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{natbib}
\usepackage{thmtools}
\usepackage{thm-restate}

\usepackage{algorithm}
\usepackage{algpseudocode}

%\usepackage{thesis}

% User defined
\newcommand{\norm}[1]{\| #1 \|}
\newcommand{\argmin}{\operatornamewithlimits{argmin}}
\newcommand{\BigO}[1]{\ensuremath{\operatorname{O}\bigl(#1\bigr)}}
\newtheorem{lem}{Lemma}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\DeclareMathOperator*{\argmax}{arg\,max}

\begin{document}

\section{NN-UCT}
Consider a deterministic MDP $\mathcal{M} = \langle S,A,T,\rho,\gamma \rangle$, where $S$ is the state space, $A$ is the action space. $T : S \times A \mapsto S$, is the transition function, $\rho : S \times A \mapsto \mathbb{R}$ is the reward function, where $\rho(s,a)$ is the immediate reward obtained by taking action $a$ from state $s$, and $\gamma$ is the discount factor that acts as a tradeoff between short-term and long-term rewards. We aim to learn a policy $\pi : S \times A \mapsto [0,1]$, using a state-action value function defined as $Q^{\pi}(s,a) = \mathbb{E}_{\pi}[\sum_{t=1}^{\infty} \gamma^{t-1} \rho(s_{t-1},a_{t-1}) | s_0 = s, a_0 = a]$. The optimal policy, $\pi^*$, has action-value function $Q^* = \max_{\pi} Q^{\pi}$.

Let $D$ denote the set of nodes in the UCT tree. Note that two or more nodes can share the same state $s \in S$. Let $S(d)$ denote the state corresponding to node $d \in D$. Let $A(d)$ denote the action leading to the state corresponding to node $d \in D$. Each node maintains the returns obtained by rollouts starting from that node, $R(d)$, as well as the visit count, $n(d)$. The value of the node is estimated as $V(d) = \frac{R(d)}{n(d)}$. The action value estimate is $Q(d,a) = \rho(S(d),a) + \gamma * V(d')$, where $d'$ is the child node reached by taking action $a$.

The NN-UCT algorithm computes a nearest neighbor estimate of returns and visit counts when the tree policy is executed at node $d$ as follows:
\begin{align*}
R_{nn}(d) = \sum_{d' \in D} K(S(d),S(d')) *  R(d')\ \\
n_{nn}(d) = \sum_{d' \in D} K(S(d),S(d')) * n(d') \\
V_{nn}(d) = \frac{R_nn(d)}{n_{nn}(d)}
\end{align*}
where $K(s,s')$ is a kernel function that measures similarity between states $s$ and $s'$. A popular choice for the kernel function is the Gaussian kernel function, $K(s,s') = \exp\left(\frac{\norm{s - s'}_2^2}{\sigma^2}\right)$, where $\sigma$ is the Gaussian width parameter which controls the degree of generalization. As $\sigma \to 0$, the nearest neighbor estimate approaches the Monte-Carlo (MC) estimate of all the transpositions \cite{childs2008transpositions}. Note that the nearest neighbor estimate adds bias, which could prevent the guarantee of continual exploration of all actions. Hence, asymptotically, we want the returns and visit count estimates to be closer to the unbiased MC estimates. One resolution is to employ a decay scheme, that shrinks the Gaussian widths of the nodes as planning continues. In section \ref{sec:decay}, we discuss one such scheme for reducing $\sigma$ smoothly over time. The tree policy uses a modified UCB score:
\begin{align*}
Q_{ucb}(d,a) = Q_{nn}(d,a) + C * \sqrt{\frac{2 \ln{\sum_{a' \in A} n_{nn}(d,a')}}{n_{nn}(d,a)}}
\end{align*}
where $Q_{nn}(d,a) = \rho(S(d),a) + \gamma * V_{nn}(d')$, where $d'$ is the child node of node $d$ when action $a$ is taken from state $S(d)$. The quantity $n_{nn}(d,a) \equiv n_{nn}(d')$ is taken from the child node $d'$. Thus, the nearest neighbor estimate of the number of times action $a$ was taken from state $S(d)$ is obtained from the number of state visits to the after-state $S(d')$. In our algorithm, unlike UCT, $\sum_{a \in A} n_{nn}(d,a) \neq n_{nn}(d)$. Thus, neither the actual visit count ($n(d)$), nor the nearest neighbor estimate ($n_{nn}(d)$) of the parent node is used in the exploration term. Hence, only the estimates of the child nodes (after-state) are used for action selection and actions leading to after-states similar to the ones already explored are not preferred.

\subsection{Decay Scheme}
\label{sec:decay}
Given the initial Gaussian kernel width $\sigma$, we want to develop a scheme that shrinks the Gaussian widths of nodes such that the value estimates and visit counts are closer to the MC estimate, asymptotically. We use a simple decay rate, $\beta$, where $0 < \beta < 1$. In this scheme, we shrink the Gaussian width of the child nodes whenever the tree policy is executed at the parent node. Thus, the Gaussian width of a node $d'$ is given by $\sigma(d') = \sigma * \beta^{n(d)}$, where $n(d)$ is the count of the visits to the parent node $d$. The Gaussian width of the root node is shrunk after each rollout.

\section{Theoretical Analysis}
We assume that the deterministic MDP is episodic and the maximum episode length is finite. We also assume for the moment that the underlying state transition graph is a DAG. We consider transpositions and use the values and visit counts of the after-states. One way of handling this in UCT is to create only one node for each transposition as shown in Figure \ref{fig:dag_uct}, making the UCT tree also a DAG. The root of a DAG is the node which has a path to every other node in the DAG. Leaf nodes in the DAG correspond to terminal states, and do not have a path to any other node in the DAG. Another way of handling transpositions while retaining the tree structure of UCT (Figure \ref{fig:plain_uct}) is to combine the statistics of all the transpositions. The NN-UCT algorithm achieves this while computing the nearest neighbor estimates for any value of $\sigma \geq 0$. We want to show that the NN-UCT algorithm chooses the optimal action at the root considering all rewards until termination, as the number of iterations of the tree policy goes to infinity.

Let $U$ denote the UCT DAG. We define $\tau$ as a directed acyclic subgraph if it satisfies all of the following:
\begin{enumerate}
\item $\tau$ is a subgraph of $U$.
\item The root and at least one leaf node of $U$ is in $\tau$.
\item For every non-leaf node in $\tau$, there is a path in $\tau$ to a leaf node in $\tau$. 
\end{enumerate}
For a directed acyclic subgraph $\tau$, define
\begin{align*}
V_{\tau}(d) = \max_{d' \in child_{\tau}(d)} \rho(S(d),A(d')) + \gamma * V_{\tau}(d')
\end{align*}
\begin{lem}
\label{lem:tau}
Let $\tau$ denote the set of nodes in the UCT DAG that are visited infinitely many times. Then $\tau$ is a directed acyclic subgraph, and for all nodes in $\tau$, the nearest neighbor estimates converge to the $\tau$-value of the node.
\begin{proof}
Firstly, the root is visited infinitely often and hence is in $\tau$. The set $\tau$ also includes at least one leaf node since each rollout ends at a leaf node. The value of the leaf node(s) in $\tau$ converges trivially to its $\tau$-value as the default policy is executed at the leaf node(s). 

The set $\tau$ is a subgraph of $U$ since the nodes and edges in $\tau$ are also in $U$. It also holds that every non-leaf node in $\tau$ has a path in $\tau$ to a leaf node in $\tau$, since there are finitely many nodes in $\tau$. Hence $\tau$ is a directed acyclic subgraph.

Now that the nearest neighbor estimates at the leaf node(s) in $\tau$ converge to their $\tau$-values, by induction, all other nodes in $\tau$ also converge to their $\tau$-values.
\end{proof}
\end{lem}

\begin{lem}
\label{lem:full_dag}
$\tau$ is the whole DAG $U$.
\begin{proof}
We prove by contradiction. Let $d \in \tau$ be a node such that $d'$ is a child of $d$ in $U$, with $S(d') = T(S(d),a')$ but $d'$ is not in $\tau$. Let $t_{d'}$ denote the last time $d'$ was visited. Consider any child $d"$ of $d$ with $S(d") = T(S(d),a")$ such that $d" \in \tau$. Since $d"$ is visited infinitely often, $Q_{ucb}(d,a")$ converges to $\rho(S(d),a") + \gamma * V_{\tau}(d")$ since $\lim_{t \to \infty} \frac{\ln\left(\sum_{\tilde{a} \in A} n_{nn}(d,\tilde{a})\right)}{n_{nn}(d,a")} = 0$. Hence for all children of $d$ in $\tau$, UCB estimates converge to a finite value. On the other hand, since $d'$ is visited only finitely many times but $d$ is visited infinitely often, $Q_{ucb}(d,a')$ grows unbounded. This is because the denominator in the bonus term of $Q_{ucb}(d,a')$ converges to a finite visit count $n(d')$, while the numerator grows unbounded. Hence, there exists a time after $t_{d'}$ such that $Q_{ucb}(d,a') > \underset{d' \in child_{\tau}(d), A(d') = a} \max Q_{ucb}(d,a)$, contradicting that $t_{d'}$ was the last time node $d'$ was visited.
\end{proof}
\end{lem}

We now have the main result.
\begin{theorem}
For large enough iterations, the action whose value estimate is the largest at the root is the optimal action.
\begin{proof}
By Lemma \ref{lem:full_dag}, the NN-UCT algorithm visits all the nodes in the DAG, infinitely often. By Lemma \ref{lem:tau}, the values of all nodes converge to their true values. Since there are finitely many actions, the result follows.
\end{proof}
\end{theorem}

\bibliographystyle{plain}
\bibliography{references}
\end{document}