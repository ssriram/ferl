\chapter{Background}
\label{chap:background}
Sequential decision problems involve an agent making a sequence of decisions at discrete time steps in a known or unknown environment. The goal of the agent is to maximize its future discounted sum of rewards. The rewards may be obtained from the environment \citep{sutton1998introduction} or intrinsically generated \citep{oudeyer2007intrinsic} or simply the reward of gathering knowledge about the environment \citep{sutton2011horde}. Sequential decision problems can be described using the Markov Decision Process (MDP) setting \citep{puterman2009markov}. The MDP framework makes the assumption that there is a \textit{state} variable that completely describes the environment, which is formally called the Markovian assumption. Knowledge of the state and the dynamics of the environment are sufficient to predict future rewards.

\section{Markov Decision Processes (MDPs)}
In this thesis, I focus on deterministic MDPs. Such a MDP has no randomness involved in its state transitions as well as reward function. A deterministic MDP $\mathcal{M}$ is a tuple $\langle S,A,T,\rho,\gamma \rangle$, where each one of the components are:
\begin{itemize}
\item $S$: The set of all states, with $s_0$ being the initial state.
\item $A$: The set of actions available to the agent in the environment.
\item $T : S \times A \to S$: The transition function returns the next state, given a state-action pair.
\item $\rho : S \times A \to \mathbb{R}$: The reward function returns a scalar as reward obtained for taking an action from a state.
\item $\gamma \in [0,1]$: The discount factor acts as a tradeoff between short-term and long-term rewards.
\end{itemize}
The dynamics of the environment can be described as follows: at timestep $t$, the agent observes state $s_t$ and selects an action $a_t \in A$ and receives reward $r_t = \rho(s_t,a_t)$; the next state $s_{t+1}$ is given by the transition function $T(s_t,a_t)$. The Markov assumption of the environment can be formally described as:
\begin{align*}
\Pr\left(s_{t+1},r_t | s_0,a_0,...,s_{t-1},a_{t-1},s_t,a_t\right) = \Pr\left(s_{t+1},r_t|s_t,a_t\right) 
\end{align*}
In other words, the next state and immediate reward is dependent only on the current state and action. The Markov assumption is also captured in the parameterization of the transition and reward function, where the arguments include only a state-action pair.

The behavior of an agent can be described by a \textit{policy} $\pi : S \times A \to [0,1]$, where $\pi(a|s)$ denotes the probability of taking action $a$ in state $s$. We can \textit{evaluate} a policy using its \textit{$\gamma$-discounted expected return} defined as:
\begin{align*}
\mathbb{E}_{\pi}\left[\sum_{t=0}^{\infty} \gamma^{t} \rho\left(s_{t},a_{t}\right)\right].
\end{align*}
Now that we can evaluate a policy, a natural question is to find an \textit{optimal} policy, $\pi^* = \underset{\pi}\argmax \; \mathbb{E}_{\pi}\left[\sum_{t=0}^{\infty} \gamma^{t} \rho\left(s_{t},a_{t}\right)\right] \forall s$.

\section{Value Function}
For a given deterministic MDP $\mathcal{M}$ and policy $\pi$, we can define a \textit{state-value} function $V_{\pi}(s) : S \to \mathbb{R}$, as the expected return obtained when starting in state $s$ and following policy $\pi$, i.e.,
\begin{align*}
V_{\pi}(s) = \mathbb{E}_{\pi}\left[\sum_{t=0}^{\infty} \gamma^{t} \rho(s_{t},a_{t}) \mid s_0 = s\right]
\end{align*}
We can also define an \textit{state-action value function} $Q_{\pi}(s,a) : S \times A \to \mathbb{R}$, which is a mapping from state-action pairs to the expected return achieved by starting in state $s$, taking action $a$ and following policy $\pi$ thereafter, i.e., 
\begin{align}
\label{eqn:qValue}
Q_{\pi}(s,a)= \mathbb{E}_{\pi}\left[\sum_{t=0}^{\infty} \gamma^{t} \rho(s_{t},a_{t}) | s_0 = s, a_0 = a\right]
\end{align}
The Markov property of MDPs allows us to write the state-action value function recursively, known as the \textit{Bellman} equation :
\begin{align}
\label{eqn:bellman}
Q_{\pi}(s,a) = \rho(s,a) + \gamma \sum_{a' \in A} \pi(a' \mid T(s,a)) Q_{\pi}(T(s,a),a').
\end{align}
Thus, the state-action value function and value function are related as, 
\begin{align*}
Q_{\pi}(s,a) = \rho(s,a) + \gamma V_{\pi}(T(s,a)).
\end{align*}
The optimal policy $\pi^*$ has a value function
\begin{align*}
Q^*(s,a) = \rho(s,a) + \gamma \max_{a' \in A} Q^{*}(T(s,a),a').
\end{align*}

\section{Monte-Carlo Tree Search}
\label{sec:MCTS}
Now that we have described the sequential decision problem in the form of MDPs, we can now focus on planning algorithms to determine actions in the optimal policy. Recall from Chapter \ref{chap:intro} that the search process should balance the two objectives of looking ahead into the future and exploring other actions. One of the ways to achieve this balance is by building a search tree using samples from the model. The tree consists of nodes corresponding to states and edges corresponding to actions taken from a state. These methods are called Monte-Carlo Tree Search \citep{browne2012survey} algorithms.

A general Monte-Carlo Tree Search (MCTS) algorithm is shown in Algorithm \ref{alg:mcts}. The algorithm builds a search tree iteratively, until there is no computational budget available. This budget can be defined in terms of time or memory or a constraint on the number of iterations. When there is no more budget available, the function returns the best action from the root node. The search procedure is called from a start state ($s_0$). It then builds the tree, adding one node at a time. MCTS algorithms compute value functions to determine action selection at each level in the tree, eventually leading to the creation of a leaf node. Once a leaf node is created, the algorithm performs a \textit{rollout}, which is a sequence of actions dictated by a pre-specified \emph{default policy}. The algorithm uses the returns (discounted sum of rewards) collected from these rollouts to determine the value of the node. A formal description follows.

Let $D$ denote the set of nodes in the tree. Note that two or more nodes can share the same state $s \in S$. Let $S(d)$ denote the state corresponding to node $d \in D$ and $A(d)$ denote the action from the parent node, leading to node $d \in D$. Each node maintains the sum of returns obtained by rollouts starting from that node, $R(d)$, as well as the number of times it has been visited, $n(d)$. The value of the node is, $V(d) = \frac{R(d)}{n(d)}$. The action value estimate is given by $Q(d,a) = \rho(S(d),a) + \gamma * V(d')$, where $d'$ is the child node reached by taking action $a$. 

\begin{algorithm}
\caption{MCTS algorithm}\label{alg:mcts}
\begin{algorithmic}[1]
\Function{MCTS}{$s_0,n,depth$} \Comment $n$ = number of rollouts, $depth$ = length of rollout
\State create root node $d_0$ with state $s_0$
\While{$n > 0$}
	\State $d_l \gets$ \Call{TreePolicy}{$d_0$} \Comment Tree Policy algorithm dependent
	\State z $\gets$ \Call{DefaultPolicy}{S($d_l$),$depth$,$\gamma$}
	\State \Call{BackUp}{$d_l$,$z$}
	\State $n \gets n - 1$
\EndWhile
\State \Return{$\underset{a \in A} \argmax \; Q(d_0,a)$}
\EndFunction
\Statex
\Function{DefaultPolicy}{$s,depth,\gamma$}
\If{s is terminal or $depth == 0$}
	\State \Return{0}
\Else
	\State Choose $a \in A$ uniformly at random
	\State $r \gets \rho(s,a)$
	\State $s$ $\gets$ T($s,a$)
	\State $depth \gets depth - 1$
	\State $r$ $\gets$ $r$ + $\gamma$ * \Call{DefaultPolicy}{$s,depth,\gamma$}
	\State \Return{r}
\EndIf
\EndFunction
\Statex
\Function{BackUp}{$d,z$}
\While{$d$ is not null}
	\State $n(d) \gets n(d) + 1$
	\State $R(d) \gets R(d) + z$
	\State $a \gets A(d)$
	\State $d$ $\gets$ parent of $d$
	\If{$d$ is not null}
		\State $z$ $\gets$ $\rho$(S($d$),$a$) + $\gamma * z$
	\EndIf
\EndWhile
\EndFunction
\end{algorithmic}
\end{algorithm}

An MCTS algorithm consists of the following components :
\begin{enumerate}
\item{Tree Policy - The tree policy is used to perform action selection at each level in the tree, eventually leading to the creation of a leaf node. Thus, the tree policy dictates the way the tree is grown during planning.}
\item{Default Policy - This is used to run a simulation, starting from the state corresponding to the leaf node, until a certain length or termination. The default policy is usually uniformly random.}
\item{Backup - The backup procedure updates the nodes of the tree, starting from the leaf, up to the root, along the branch chosen, with the return accumulated from the default policy. The backup procedure updates the value estimates of the nodes, which would affect the tree policy in the next iteration.}
\end{enumerate}
Finally, when no more simulations are run the greedy action from the root is chosen and returned. Ties are either broken based on visit counts, or randomly.

\section{UCT - UCB on Trees}
\label{sec:UCT}
UCT \citep{kocsis2006bandit} is an MCTS algorithm that uses the UCB1 algorithm \citep{auer2002finite} for its tree policy. UCT treats the action selection problem at each sub-tree as a multi-armed bandit problem. For the multi-armed bandit problem, UCB1 tries to solve the \textit{exploration-exploitation} dilemma by computing a score that combines the two quantities. Assuming that the payoffs of the arms are i.i.d. (identically and independently distributed), the UCB1 algorithm has a regret that grows logarithmically in the number of arm pulls. The regret is the loss incurred by not pulling the best arm in hindsight. The UCB1 aims to balance the \textit{exploration-exploitation} dilemma by keeping track of the average rewards of the arms and picking the one with the best upper-confidence bound. The average reward of arm $i$ is given by $\bar{X_{i,T_i(t-1)}} = \frac{1}{T_i(t-1)} \sum_{j=0}^{t-1} X_{i,T_i(j)}$, where $X_{i,j} \in [0,1]$ denotes the rewards and $T_i(t) = \sum_{j=0}^{t}\mathbbm{1}{ (I_j = i) }$ is the number of times arm $i$ has been pulled from time 0 to t (included). The UCB1 algorithm picks the arm in the following way :
\begin{equation}
\begin{aligned}
I_t = \underset{i \in \{1,2,..,K\}} {\argmax} {\bar{X_{i,T_i(t-1)}} + c_{t-1,T_i(t-1)}}, \\
c_{t,s} = \sqrt{\frac{2 \ln{t}}{s}}. 
\end{aligned}
\end{equation}
The UCB1 algorithm uses the optimism under uncertainty principle. The measure of uncertainty is expressed by the exploration bonus term. Thus, if a certain arm hasn't been pulled a sufficient number of times, its exploration bonus grows with time, forcing the arm to be pulled. This helps in improving the reliability of the estimates of less promising arms, as well as avoiding selecting a sub-optimal arm.

\begin{algorithm}[ht]
\caption{UCT Tree Policy}\label{alg:UCT}
\begin{algorithmic}[1]
\Function{TreePolicy}{$d$}
\While{d is non-terminal}
	\If{d has unexplored actions}
		\State \Return{\Call{Expand}{$d$}}	
	\Else
		\State $d$ $\gets$ \Call{BestChild}{$d,C$} 
	\EndIf
\EndWhile
\State \Return{d}
\EndFunction
\Statex
\Function{BestChild}{$d,C$}
\State $a \gets \underset{a \in A} {\argmax} Q(d,a) + C * \sqrt{\frac{\ln{\sum_{a' \in A} n(d,a')}}{n(d,a)}}$
\State \Return{f(d,a)}
\EndFunction
\Statex
\Function{Expand}{$d$}
\State Pick $a$ $\in$ untried actions from $A$
\State add a new child $d'$ to $d$ with
\State S($d'$) $\gets$ T(S($d$),$a$)
\State $f(d,a) \gets d'$
\State $A(d') \gets a$
\State $n(d') \gets 0$
\State $R(d') \gets 0$ 
\State \Return{$d'$}
\EndFunction
\end{algorithmic}
\end{algorithm}
Algorithm \ref{alg:UCT} shows the tree policy of UCT. The UCT algorithm treats the return obtained by the Monte-Carlo simulations as i.i.d random variables. By Hoeffding's inequality, these estimates concentrate around the mean quickly at the leaf nodes. However, since the sampling probability of actions changes in the subtree, the payoffs may change over time. But, with an appropriate exploration bonus, the change is compensated for. 

In the UCT algorithm, the UCB1 algorithm is used to select actions at each level in the tree. Hence, the UCB score at node $d$ and action $a$ is given by:
\begin{equation}
\label{eqn:UCB}
Q_{ucb}(d,a) = Q(d,a) + C * \sqrt{\frac{2 \ln{\sum_{a' \in A} n(d,a')}}{n(d,a)}}.
\end{equation}
The second term is an exploratory bonus, which encourages actions taken less frequently from a node, where $C$ is a positive parameter, $n(d,a)$ is the number of times, action $a$ was taken from node $d$ and hence $n(d,a) = n(d')$, where $d'$ is the child node reached by taking action $a$. After the computational budget is exhausted in building the tree, the greedy action at the root, $\underset{a \in A} \argmax \; Q(d_0,a)$ is picked.

\subsection{Convergence of UCT}
Given a finite horizon MDP, under the assumption that the returns from the default policy are i.i.d samples, UCT converges to the optimal policy, asymptotically. The probability of choosing a suboptimal action at the root converges to 0 at a polynomial rate in the number of rollouts. In practice, UCT has been applied successfully in many games such as computer \textit{Go}. The UCB constant ($C$) is the only parameter that needs to be tuned to the specific domain.

\section{Enhancements to UCT}
\label{sec:uct_enhance}
A few popular enhancements to UCT are described below. These enhancements are used to make UCT sample efficient in practice and have no affect on the asymptotic convergence to optimality.
\begin{enumerate}
\item{Retaining the optimal subtree between planning steps: Once the planning budget is exhausted, UCT returns the greedy action at the root. A useful optimization is to retain the branch of the tree corresponding to the chosen action for the next planning step. This helps UCT grow its tree deeper in the next planning step, as the UCT tree for the next planning would consist of nodes in the optimal branch.}
\item{Transposition tables: Transposition tables \citep{childs2008transpositions} are lookup tables to use values of state-action pairs that have already been encountered in the tree. This allows statistics to be shared across nodes with the same state. When memory is at a premium, care must be taken in ensuring that all the child nodes are considered for action selection when only some of them are stored in the table.}
\item{After-state trick \citep{mehat2010combining}: Instead of storing statistics (returns, visit counts) as state-action pairs in the edges, we can store the statistics in the nodes corresponding to the after-state. This helps in the exploration being guided towards promising after-states, instead of relying on the counts of the actions being taken from a state. 

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,
    		                thick,main node/.style={circle,draw,font=\sffamily\Large\bfseries}]
  
	  \node[main node] (1) {$s_1$};
	  \node[main node] (3) [below left of=1] {$s_3$};
	  \node[main node] (2) [below right of=1] {$s_2$};
	  \node[main node] (4) [below of=2] {$s_4$};
  
	  \path[every node/.style={font=\sffamily\small}]
    		(1) edge node [left] {0.6} (3)
        		edge node [right] {0.4} (2)
	    (2) edge node [left] {0.3} (3)
     	   edge node [right] {0.1} (4);

	\end{tikzpicture}
	\caption{DAG MDP State Diagram}
	\label{fig:dag_mdp}
\end{figure}
\begin{figure*}[h]
	\begin{subfigure}{0.42\hsize}
		\centering
		\includegraphics[width=0.9\hsize]{dag_uct.eps}
		\caption{UCT with Transpositions}
		\label{fig:dag_uct}
	\end{subfigure}
	\begin{subfigure}{0.56\hsize}
		\centering
		\includegraphics[width=0.9\hsize]{plain_uct.eps}
		\caption{UCT without Transpositions}
		\label{fig:plain_uct}
	\end{subfigure}
	\caption{UCT Implementations}
	\label{fig:uct_tree}
\end{figure*}
Figure \ref{fig:dag_mdp} shows the state diagram of an MDP whose state transitions form a Directed Acyclic Graph (DAG) with states $s_3$ and $s_4$ being terminal states. There is a transposition at state $s_3$ as it is reachable from both states $s_1$ and $s_3$. Figure \ref{fig:uct_tree} shows the UCT tree with and without transpositions, after 10 iterations. The after-state trick along with transpositions turns the UCT tree in to a DAG with returns and visit counts stored in the nodes, rather than the edges. Notice that the visit count of state $s_3$ is higher than one of its parents $s_2$ as it is visited from both the root ($s_1$) as well as state $s_2$. Without transpositions, two different nodes for the same state $s_3$ are created. The sum of the returns and visit counts of the two nodes sum up to the total visit count of the state.
}
\end{enumerate}