
\chapter{State Generalization in UCT}
\label{chap:NN-UCT}
\begin{figure*}[h]
	\centering
	\begin{subfigure}{0.32\hsize}
		\centering
		\includegraphics[width=\hsize]{Grid-eps-converted-to.pdf}
  		\caption{Grid World}
		\label{fig:grid_world}
	\end{subfigure}
	\begin{subfigure}{0.32\hsize}
		\centering
		\includegraphics[width=0.9\hsize]{uct_heatmap.png}
		\caption{Heat map of UCT after 100 rollouts}
		\label{fig:uct_heatmap}
	\end{subfigure}
	\begin{subfigure}{0.32\hsize}
		\centering
		\includegraphics[width=0.9\hsize]{nnuct_heatmap.png}
		\caption{Heat map of NN-UCT after 100 rollouts}
		\label{fig:nnuct_heatmap}
	\end{subfigure}
\end{figure*}

UCT has been successfully used in many sequential decision problems and was able to achieve \textit{master} level in computer \textit{Go}. Its balance of exploration-exploitation using confidence bounds is built on the sound theory of regret minimization.

However, UCT performs poorly when the rewards are sparse. Consider the grid world domain shown in Figure \ref{fig:grid_world}. The agent starts at the solid square in blue. The goal state is marked by a red X and the agent receives a reward of $+1$ on reaching the goal state, leading to episode termination. The agent has 4 actions available - move UP, DOWN, LEFT and RIGHT. In the absence of reward from rollouts, UCT essentially follows random behavior. For a reasonably distant goal state, a uniform random policy is unlikely to observe any reward. Figure \ref{fig:uct_heatmap} shows the heat map of states visited by UCT after 100 rollouts from the start state. Due to its random rollout policy, UCT keeps visiting the same or nearby states. As a result, UCT needs a large number of rollouts or very long rollouts to see reward, and therefore to make a good decision.

An alternative approach would be to systematically explore the state space, collecting returns from states that are different from those already visited. However, performing an exhaustive search of the state space would be impractical. We want to retain the strengths of Monte-Carlo planning in using samples, while still achieving efficient exploration. I aim to exploit similarity between states and augment UCT with this information. Using this information, we can \textit{generalize} to new states. The state generalization can be incorporated in the tree policy of UCT, specifically in the UCB score computed for action selection in the tree policy. By doing so, UCT can now grow its tree towards new and dissimilar states, thereby helping UCT see possible reward in distant future states. This would help UCT choose the optimal action from the start state, that helps it obtain reward(s) in future.

\section{NN-UCT}
\label{sec:nnUCT}
Even if we haven't visited a state before, a good estimate of the node's statistics can be obtained from the statistics of other \textit{similar} nodes in the tree, thus making better use of samples. A natural way to incorporate this form of generalization is to combine the statistics of similar nodes and use it to compute the UCB score. One way to achieve this is to compute a nearest neighbor estimate of the return and visit counts for each node $d$, as given by :
\begin{align*}
R_{nn}(d) = \sum_{d' \in D} K(S(d),S(d')) *  R(d')\ \\
n_{nn}(d) = \sum_{d' \in D} K(S(d),S(d')) * n(d') \\
V_{nn}(d) = \frac{R_{nn}(d)}{n_{nn}(d)}
\end{align*}
where $K(s,s')$ is a kernel function that measures similarity between states $s$ and $s'$. A popular choice for the kernel function is the Gaussian kernel function, $K(s,s') = \exp\left(\frac{\norm{s - s'}_2^2}{\sigma^2}\right)$, where $s,s' \in \mathbb{R}^d$ , $\sigma$ is the Gaussian width parameter which controls the degree of generalization. As $\sigma \to 0$, the nearest neighbor estimate approaches the Monte-Carlo (MC) estimate of all of the transpositions. If generalization is turned off, we can still reap the benefit of using transposition tables, described in section \ref{sec:uct_enhance}, without additional memory but with additional computational cost for every run of the tree policy. The computational cost is linear in the number of nodes in the UCT tree for every level at which the tree policy is executed. Although transposition tables cost less in combining statistics of nodes with transpositions, our approach helps us to introduce higher degrees of generalization.

Note that the nearest neighbor estimate adds bias, which could prevent the guarantee of continual exploration of all actions. Hence, asymptotically, we want the returns and visit count estimates to be closer to the unbiased MC estimates. One solution is to employ a decay scheme, that shrinks the Gaussian widths of the nodes as planning continues. In section \ref{sec:decay}, we discuss one such scheme for reducing $\sigma$ smoothly over time. The new UCB score is computed as :
\begin{align*}
Q_{ucb}(d,a) = Q_{nn}(d,a) + C * \sqrt{\frac{2 \ln{\sum_{a' \in A} n_{nn}(d,a')}}{n_{nn}(d,a)}}
\end{align*}
where $Q_{nn}(d,a) = \rho(S(d),a) + \gamma * V_{nn}(d')$, where $d'$ is the child node of node $d$ when action $a$ is taken from state $S(d)$. The quantity $n_{nn}(d,a) \equiv n_{nn}(d')$ is taken from the child node $d'$. Thus, the nearest neighbor estimate of the number of times action $a$ was taken from state $S(d)$ is obtained from the number of state visits to the after-state $S(d')$. In our algorithm, unlike UCT, $\sum_{a \in A} n_{nn}(d,a) \neq n_{nn}(d)$. Thus, neither the actual visit count ($n(d)$), nor the nearest neighbor estimate ($n_{nn}(d)$) of the parent node is used in the exploration term. Hence, only the estimates of the child nodes (after-states, as described in section \ref{sec:uct_enhance}) are used for action selection and actions leading to after-states similar to the ones already explored are not preferred. Figure \ref{fig:nnuct_heatmap} shows the heatmap of states visited by NN-UCT on the grid world domain after 100 rollouts from the start state where each state is given by a pair $(i,j), 0 \leq i,j < 40$, and a Gaussian kernel with $\sigma = 100$ is used with a decay scheme (explained in the next section) that uses $\beta = 0.9$. NN-UCT directs tree to be grown in areas farther from states that have been encountered already. In the absence of reward, with ties bro- ken using visit counts, the action picked at the root would be one that leads to a state where rollouts could see non-zero reward.

\subsection{Decay Scheme}
\label{sec:decay}
Given the initial Gaussian kernel width $\sigma$, we want to develop a scheme that shrinks the Gaussian widths of nodes such that the value estimates and visit counts are closer to the MC estimate, asymptotically. We use a simple decay rate, $\beta$, where $0 < \beta < 1$. In this scheme, we shrink the Gaussian width of the child nodes whenever the tree policy is executed at the parent node. Thus, the Gaussian width of a node $d'$ is given by \[\sigma(d') = \sigma * \beta^{n(d)},\] where $n(d)$ is the count of the visits to the parent node $d$. The Gaussian width of the root node is shrunk after each rollout. This scheme ensures that if erroneous generalization has starved visits to $d'$, it will eventually be explored once its parent has been visited sufficiently often. The next section formalizes this intuition.

\section{Theoretical Analysis}
For our theoretical analysis, we assume that the deterministic MDP is episodic and the maximum episode length is finite. We also assume for the moment that the underlying state transition graph is a DAG. We consider transpositions and use the values and visit counts of the after-states. One way of handling this in UCT is to create only one node for each transposition as shown in Figure \ref{fig:dag_uct}, making the UCT tree also a DAG. The root of a DAG is the node which has a path to every other node in the DAG. Leaf nodes in the DAG correspond to terminal states, and do not have a path to any other node in the DAG. Another way of handling transpositions while retaining the tree structure of UCT (Figure \ref{fig:plain_uct}) is to combine the statistics of all the transpositions. The NN-UCT algorithm achieves this while computing the nearest neighbor estimates using the kernel function. We also assume that the kernel (similarity) function converges to the Kronecker delta after a (possibly) large but finite number of rollouts, i.e.
\begin{displaymath}
   K(S(d),S(d')) = \left\{
     \begin{array}{lr}
       1 : S(d) == S(d') \\
       0 : otherwise
     \end{array}
   \right.
\end{displaymath}
Note that even after the kernel function evaluates to the Kronecker delta function, NN-UCT still accounts for transpositions in the search tree. We want to show that the NN-UCT algorithm chooses the optimal action at the root considering all rewards until termination, as the number of rollouts goes to infinity.

Let $U$ denote the UCT DAG. A set of nodes $\tau$ of $U$ spans a \textit{proper} subgraph of $U$ if $\tau$ satisfies all of the following:
\begin{enumerate}
\item The root and at least one leaf node of $U$ is in $\tau$.
\item For every non-leaf node in $\tau$, there is a path of $U$ through nodes only in $\tau$ to reach some leaf node in $\tau$. 
\end{enumerate}
For a node $d \in \tau$, we define $child_{\tau}(d)$ as the set of nodes such that $d' \in child_{\tau}(d)$ if node $d'$ is a child of node $d$ in $U$. 

For a proper subgraph $\tau$ and a node $d \in \tau$, define
\begin{align*}
V_{\tau}(d) = \max_{d' \in child_{\tau}(d)} \rho(S(d),A(d')) + \gamma * V_{\tau}(d')
\end{align*}
\begin{lem}
\label{lem:tau}
Let $\tau$ denote the set of nodes in the UCT DAG that are visited infinitely many times. Then $\tau$ is a directed acyclic subgraph, and for all nodes in $\tau$, the nearest neighbor estimates converge to the $\tau$-value of the node.
\begin{proof}
Firstly, the root is visited infinitely often and hence is in $\tau$. The set $\tau$ also includes at least one leaf node since each rollout ends at a leaf node and there are finitely many leaf nodes in $U$. The value of the leaf node(s) in $\tau$ converges trivially to its $\tau$-value, which is also its value.

It also holds that every non-leaf node in $\tau$ has a path in $\tau$ to a leaf node in $\tau$, since there are finitely many nodes in $\tau$. Hence $\tau$ is a proper subgraph of $U$.

Now that the nearest neighbor estimates at the leaf node(s) in $\tau$ converge to their $\tau$-values, by induction, all other nodes in $\tau$ also converge to their $\tau$-values.
\end{proof}
\end{lem}

\begin{lem}
\label{lem:full_dag}
$\tau$ is the whole DAG $U$.
\begin{proof}
We prove by contradiction. Let $d \in \tau$ be a node such that $d'$ is a child of $d$ in $U$, with $S(d') = T(S(d),a')$ but $d'$ is not in $\tau$. Let $t_{d'}$ denote the last time (index of a rollout) $d'$ was visited. Consider any child $d"$ of $d$ with $S(d") = T(S(d),a")$ such that $d" \in \tau$. Since $d"$ is visited infinitely often, $Q_{ucb}(d,a")$ converges to $\rho(S(d),a") + \gamma * V_{\tau}(d")$ since $\lim_{t \to \infty} \frac{\ln\left(\sum_{\tilde{a} \in A} n_{nn}(d,\tilde{a})\right)}{n_{nn}(d,a")} = 0$. Hence for all children of $d$ in $\tau$, UCB estimates converge to a finite value. On the other hand, since $d'$ is visited only finitely many times but $d$ is visited infinitely often, $Q_{ucb}(d,a')$ grows unbounded. This is because the denominator in the bonus term of $Q_{ucb}(d,a')$ converges to a finite visit count $n(d')$, while the numerator grows unbounded. Hence, there exists a time after $t_{d'}$ such that $Q_{ucb}(d,a') > \underset{d' \in child_{\tau}(d), A(d') = a} \max Q_{ucb}(d,a)$, contradicting that $t_{d'}$ was the last time node $d'$ was visited.
\end{proof}
\end{lem}

We now have the main result.
\begin{theorem}
For sufficiently many rollouts, the action whose value estimate is the largest at the root is the optimal action.
\begin{proof}
By Lemma \ref{lem:full_dag}, the NN-UCT algorithm visits all the nodes in the DAG, infinitely often. By Lemma \ref{lem:tau}, the values of all nodes converge to their true values. Since there are finitely many actions, the result follows.
\end{proof}
\end{theorem}

A couple of important points regarding the proof are noted here: \\
Firstly, in the above proof, we assume a general kernel function that converges to the Kronecker delta after a finite number of rollouts. Our choice of the gaussian kernel function with an exponential decay seems to work in practice while performing a finite number of rollouts, but it may be interesting to see if an appropriate fast decay scheme for the gaussian kernel function is also sufficient to retain optimality in the limit when the number of rollouts goes to infinity.

Secondly, the above proof of asymptotic convergence of NN-UCT assumes that the transition graph of the deterministic MDP is a DAG, but it can be extended to deterministic MDPs in general if we ignore transpositions. For example, when the deterministic MDP includes loops, the NN-UCT algorithm can still be made to converge to optimality in the limit by a slight modification of the kernel function. Forcing $K\left(S(d),S(d')\right) \to 0$ for $d \neq d'$ after sufficiently many but finite number of rollouts, ignores transpositions, but ensures that the value estimate of node $d$ converges to its true value estimate in the limit.

\section{Related Work}
\label{sec:rel_work}
We review transposition tables, described in section \ref{sec:uct_enhance}. Transposition tables offer a basic form of generalization by sharing statistics of nodes in the UCT tree having the same state. Our algorithm with $\sigma \approx 0$ closely resembles UCT with transposition tables. Although transposition tables help speed up search, they provide limited exploration benefit as they can only recognize state equality. In large state spaces, the likelihood of encountering transpositions is low, and hence transposition tables may be less useful. In Chapter \ref{chap:expt}, this will be seen in even our relatively small experimental domains, with more aggressive generalization improving on basic transposition tables.

State abstraction using homomorphisms in UCT \citep{jiang2014improving} adds another layer of generalization, by grouping states with the same transition and reward distributions. The homomorphism groups states together but like transposition tables, they do not have a degree of similarity, only a hard notion of equivalence. In the case of the grid world domain in Figure \ref{fig:grid_world}, the only homomorphism is the grid itself. Thus, homomorphisms offer no benefit over transposition tables in this case.

Another approach to generalization in UCT is Rapid Action Value Estimation (RAVE) \citep{gelly2011monte}. RAVE uses the \textit{All-Moves-As-First} (AMAF) heuristic which states that the value of an action is independent of when it is taken, i.e. if an action is profitable at a subtree $\tau(s)$, it is profitable immediately at root $s$ as well. RAVE combines the Monte-Carlo estimate with the AMAF estimate, $Q_{rave}(s,a) = (1-\beta) * Q(s,a) + \beta * Q_{AMAF}(s,a)$, where $0 < \beta < 1$ is a weight parameter that decreases with increasing number of rollouts. The RAVE estimate is used in place of $Q(s,a)$ for the tree policy. RAVE has been shown to be successful in Computer Go and has also been applied in continuous action spaces \citep{couetoux2011continuous}. In that case, the RAVE estimate for action $a$ is obtained by computing a nearest-neighbor estimate of the returns in the subtree where action $a$ was taken. However, the AMAF heuristic does not modify the exploration bonus. Thus, when no rewards have been encountered, its exploration strategy suffers from the same problems as UCT.