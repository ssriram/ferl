# README #

The following software package/tools are necessary for setup :
* gcc 4.2 and higher
* LAPACK with BLAS support

### What is this repository for? ###

* Running basic RL algorithms (Sarsa, GQ, TD Search) and UCT on mini-Atari and Atari games
* Learning Manifolds on the games.
* Using manifolds in RL and UCT
* Version - 1.0

### How do I get set up? ###

* Ensure gcc 4.2 and higher is available
* Install CLAPACK, CBLAS
* Dependencies - CLAPACK, CBLAS

### Contribution guidelines ###

* Talk to me before contributing.

### Who do I talk to? ###

* Owner - Srinivasan Sriram <ssriram@ualberta.ca>